#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_router::use_location;

use model::Order;
use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UserInfo {
    pub sub: String,
    pub email: String,
    #[serde(rename = "email_verified")]
    pub email_verified: bool,
    pub name: String,
    pub groups: Vec<String>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ProfileBoard {
    Overview,
    Orders,
    OrderDetail,
    Invoices,
    Retoure,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ProfileContext {
    auth_audience: String,
    menu_collapse: RwSignal<bool>,
    board: RwSignal<ProfileBoard>,
    focused_order: RwSignal<Option<Order>>,
}

impl ProfileContext {
    pub fn init(auth_audience: String) {
        let profile_signal = Self {
            auth_audience,
            menu_collapse: create_rw_signal(true),
            board: create_rw_signal(ProfileBoard::Overview),
            focused_order: create_rw_signal(None),
        };

        let location = use_location();
        create_effect(move |_| {
            location.pathname.track();
            profile_signal.board.set(ProfileBoard::Overview);
        });

        provide_context(profile_signal)
    }

    pub fn set_menu_collapse(&self, collapse: bool) {
        self.menu_collapse.set(collapse);
    }

    pub fn set_current_board(&self, board: ProfileBoard) {
        self.board.set(board);
    }

    pub fn set_order_board(&self, order: Order) {
        self.focused_order.set(Some(order));
        self.set_current_board(ProfileBoard::OrderDetail);
    }

    // #[must_use]
    // pub fn get_user_info(&self) -> Option<TokenData<UserInfo>> {
    //     let auth = expect_context::<Auth>();
    //     if let Some(user_info) = auth.decoded_access_token_unverified::<UserInfo>(
    //         jsonwebtoken::Algorithm::RS256,
    //         &[&self.auth_audience],
    //     ) {
    //         user_info.ok()
    //     } else {
    //         None
    //     }
    // }

    #[must_use]
    pub fn get_current_board(&self) -> ProfileBoard {
        self.board.get()
    }

    #[must_use]
    pub fn get_focused_order(&self) -> Option<Order> {
        self.focused_order.get()
    }

    #[must_use]
    pub fn is_menu_collapsed(&self) -> bool {
        self.menu_collapse.get()
    }

    pub fn collapse_menu(&self) {
        self.menu_collapse.update(|value| *value = !*value);
    }
}
