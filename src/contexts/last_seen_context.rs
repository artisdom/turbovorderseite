use std::collections::VecDeque;

use leptos::{Signal, SignalGet, SignalUpdate, WriteSignal};
use leptos_use::{storage::use_local_storage, utils::JsonCodec};

#[derive(Clone)]
pub struct LastSeenContext {
    products: (Signal<VecDeque<String>>, WriteSignal<VecDeque<String>>),
}

impl Default for LastSeenContext {
    fn default() -> Self {
        Self::new()
    }
}

impl LastSeenContext {
    #[must_use]
    pub fn new() -> Self {
        let (signal, write, _) = use_local_storage::<VecDeque<String>, JsonCodec>("last_seen");
        Self {
            products: (signal, write),
        }
    }

    pub fn push(&self, sku: String) {
        self.products.1.update(|upd| {
            let sku = if let Some(idx) = upd.iter_mut().position(|model_sku| *model_sku == sku) {
                upd.remove(idx).unwrap_or(sku.clone())
            } else {
                sku
            };
            upd.push_front(sku);
            upd.truncate(5);
        });
    }

    #[must_use]
    pub fn get_last_sku_set(&self) -> VecDeque<String> {
        self.products.0.get()
    }
}
