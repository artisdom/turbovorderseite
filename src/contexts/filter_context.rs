use std::collections::{BTreeMap, BTreeSet};

use leptos::{
    create_effect, create_rw_signal, RwSignal, SignalGet, SignalGetUntracked, SignalUpdate,
    SignalUpdateUntracked, SignalWith,
};
use leptos_router::use_location;
use wasm_bindgen::UnwrapThrowExt;

#[derive(Clone)]
pub struct FilterContext {
    pub filters_signal: RwSignal<BTreeMap<String, BTreeSet<String>>>,
}

impl Default for FilterContext {
    fn default() -> Self {
        Self::new()
    }
}

impl FilterContext {
    #[must_use]
    pub fn new() -> Self {
        let filters_signal = create_rw_signal(BTreeMap::new());
        let location = use_location();
        create_effect(move |_| {
            location.pathname.track();
            filters_signal.update(BTreeMap::clear);
        });
        Self { filters_signal }
    }

    pub fn toggle_filter(&self, filter_name: String, filter_value: String) {
        self.filters_signal.update(|upd| {
            if let Some(filter) = upd.get_mut(&filter_name) {
                if filter.contains(&filter_value) {
                    let _ = filter.remove(&filter_value);
                    if filter.is_empty() {
                        upd.remove(&filter_name);
                    }
                } else {
                    filter.insert(filter_value);
                }
            } else {
                let mut set = BTreeSet::new();
                set.insert(filter_value);
                upd.insert(filter_name, set);
            }
        });
    }

    #[must_use]
    pub fn build_signature_once(&self, once_attribute: Option<(String, String)>) -> String {
        if let Some((attribute, name)) = once_attribute {
            if !self.get_filters().contains_key(&attribute) {
                self.filters_signal.update_untracked(|upd| {
                    let mut once_value = BTreeSet::new();
                    once_value.insert(name);
                    upd.insert(attribute, once_value);
                });
            }
        }
        self.build_signature(&self.get_filters())
    }

    #[must_use]
    pub fn build_signature(&self, targets: &BTreeMap<String, BTreeSet<String>>) -> String {
        format!(
            "({})",
            targets
                .iter()
                .map(|(name, values)| {
                    format!(
                        "({})",
                        values
                            .iter()
                            .cloned()
                            .map(|value| format!("'{name}'='{value}'"))
                            .collect::<Vec<_>>()
                            .join(" OR ")
                    )
                })
                .collect::<Vec<_>>()
                .join(" AND ")
        )
    }

    #[must_use]
    pub fn build_inversion_filter_set(&self, facet: &str) -> BTreeMap<String, BTreeSet<String>> {
        self.get_filters()
            .into_iter()
            .filter(|(key, _): &(String, BTreeSet<String>)| {
                let (key_head, _) = key.split_once('.').unwrap_throw();
                !&facet.starts_with(key_head)
            })
            .collect()
    }

    #[must_use]
    pub fn get_mapped_facets(&self) -> BTreeMap<String, BTreeSet<String>> {
        self.filters_signal
            .get()
            .iter()
            .map(|(facet, filters)| {
                (
                    if let Some(trimmed) = facet.find('.') {
                        &facet[..trimmed]
                    } else {
                        facet
                    }
                    .to_string(),
                    filters.clone(),
                )
            })
            .collect::<BTreeMap<_, _>>()
    }

    #[must_use]
    pub fn get_filters(&self) -> BTreeMap<String, BTreeSet<String>> {
        self.filters_signal.get()
    }

    #[must_use]
    pub fn is_filter_key_exist(&self, key: &str) -> bool {
        self.filters_signal.get_untracked().contains_key(key)
    }

    #[must_use]
    pub fn is_filter_value_exist(&self, value: &str) -> bool {
        self.filters_signal
            .get()
            .iter()
            .any(|(_, filter_values)| filter_values.contains(value))
    }

    pub fn clear_filters(&self) {
        self.filters_signal.update(BTreeMap::clear);
    }
}
