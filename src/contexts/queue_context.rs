use leptos::SignalGetUntracked;
use leptos::{create_rw_signal, RwSignal, SignalSet, SignalUpdate};
use std::fmt::Debug;

#[derive(Clone, Debug)]
pub enum QueuePosition<T>
where
    T: Clone + Debug + 'static,
{
    Start(T),
    Inside(T),
    End(T),
}

#[derive(Clone, Debug)]
pub struct QueueContext<T>
where
    T: Clone + Debug + 'static,
{
    pub index: RwSignal<usize>,
    pub current: RwSignal<QueuePosition<T>>,
    pub items: Vec<T>,
}

impl<T> QueueContext<T>
where
    T: Clone + Debug + 'static,
{
    #[must_use]
    pub fn new(items: Vec<T>) -> Self {
        Self {
            current: create_rw_signal(QueuePosition::Start(items[0].clone())),
            index: create_rw_signal(0),
            items,
        }
    }

    pub fn next(&mut self) {
        let current = self.current.get_untracked();
        match current {
            QueuePosition::Start(_) | QueuePosition::Inside(_) => {
                self.index.update(|upd| *upd += 1);
                let next = self.items[self.index.get_untracked()].clone();
                if self.index.get_untracked() == self.items.len() - 1 {
                    self.current.set(QueuePosition::End(next));
                } else {
                    self.current.set(QueuePosition::Inside(next));
                }
            }
            QueuePosition::End(_) => (),
        }
    }

    pub fn previous(&mut self) {
        match self.current.get_untracked() {
            QueuePosition::End(_) | QueuePosition::Inside(_) => {
                self.index.update(|upd| *upd = upd.saturating_sub(1));
                let index = self.index.get_untracked();
                let previous = self.items[index].clone();
                if index == 0 {
                    self.current.set(QueuePosition::Start(previous));
                } else {
                    self.current.set(QueuePosition::Inside(previous));
                }
            }
            QueuePosition::Start(_) => (),
        }
    }

    pub fn set_index(&mut self, index: usize) {
        let item_count = self.items.len() - 1;
        self.index.set(std::cmp::min(index, item_count));
        let index = self.index.get_untracked();
        let item = self.items[index].clone();
        let queue_item = if index == item_count {
            QueuePosition::End(item)
        } else if index == 0 {
            QueuePosition::Start(item)
        } else {
            QueuePosition::Inside(item)
        };

        self.current.set(queue_item);
    }
}
