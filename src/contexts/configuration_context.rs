#[allow(clippy::wildcard_imports)]
use leptos::*;

use crate::environment::AppEnvironment;

#[derive(Debug, Copy, Clone)]
pub struct ConfigurationContext {
    app_environment: RwSignal<AppEnvironment>,
}

impl ConfigurationContext {
    pub fn init(app_environment: AppEnvironment) {
        provide_context(Self {
            app_environment: create_rw_signal(app_environment),
        })
    }

    #[must_use]
    pub fn get_app_environment(self) -> AppEnvironment {
        self.app_environment.get_untracked()
    }
}
