#![allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_oidc::Auth;
use model::{Order, OrderStatus, UpdateOrder};

use crate::fetch::api_put_call;

use super::configuration_context::ConfigurationContext;

#[derive(Debug, Clone, PartialEq)]
pub enum AdminBoard {
    Dashboard,
    Orders,
    Products,
    Invoices,
    Inventory,
}

#[derive(Debug, Copy, Clone)]
pub struct AdminContext {
    board: RwSignal<AdminBoard>,
}

impl Default for AdminContext {
    fn default() -> Self {
        Self::new()
    }
}

impl AdminContext {
    #[must_use]
    pub fn new() -> Self {
        Self {
            board: create_rw_signal(AdminBoard::Dashboard),
        }
    }

    pub async fn update_status(&self, order_id: String, status: OrderStatus) -> Option<Order> {
        let configuration_context = expect_context::<ConfigurationContext>();
        let auth_context = expect_context::<Auth>();

        api_put_call::<UpdateOrder, Order>(
            &format!(
                "{}/orders/{order_id}",
                configuration_context.get_app_environment().backend_endoint
            ),
            UpdateOrder { status },
            auth_context.access_token(),
        )
        .await
        .ok()
    }

    pub fn set_current_board(&self, board: AdminBoard) {
        self.board.set(board);
    }

    #[must_use]
    pub fn get_current_board(&self) -> AdminBoard {
        self.board.get()
    }
}
