#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_router::use_location;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FaqBoard {
    Shipping,
    Retoure,
    Support,
}

#[derive(Debug, Copy, Clone)]
pub struct FaqContext {
    board: RwSignal<FaqBoard>,
}

impl Default for FaqContext {
    fn default() -> Self {
        Self::new()
    }
}

impl FaqContext {
    #[must_use]
    pub fn new() -> Self {
        let faq_context = Self {
            board: create_rw_signal(FaqBoard::Support),
        };

        let location = use_location();
        create_effect(move |_| {
            location.pathname.track();
            match location.pathname.get_untracked().as_str() {
                "/shipping" => faq_context.board.set(FaqBoard::Shipping),
                "/retoure" => faq_context.board.set(FaqBoard::Retoure),
                _ => faq_context.board.set(FaqBoard::Support),
            }
        });

        faq_context
    }

    pub fn set_current_board(&self, board: FaqBoard) {
        self.board.set(board);
    }

    #[must_use]
    pub fn get_current_board(&self) -> FaqBoard {
        self.board.get()
    }
}
