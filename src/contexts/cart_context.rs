use super::configuration_context::ConfigurationContext;
use crate::fetch::api_get_call;

#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_router::use_location;
use leptos_use::{storage::use_local_storage, utils::JsonCodec};

use model::Product;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy)]
pub enum CartSortOrdering {
    Price,
    Quantity,
    Subtotal,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct CartItem {
    pub product_id: String,
    pub quantity: i32,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct CartContext {
    collapsed: RwSignal<bool>,
    items: (Signal<Vec<CartItem>>, WriteSignal<Vec<CartItem>>),
    items_in_state: RwSignal<Vec<Product>>,
    locked: RwSignal<bool>,
}

impl Default for CartContext {
    fn default() -> Self {
        Self::new()
    }
}

impl CartContext {
    #[must_use]
    pub fn new() -> Self {
        let (signal, write, _) = use_local_storage::<Vec<CartItem>, JsonCodec>("items");
        let cart_signal = Self {
            collapsed: create_rw_signal(false),
            items: (signal, write),
            locked: create_rw_signal(false),
            items_in_state: create_rw_signal(Vec::new()),
        };

        let location = use_location();
        create_effect(move |_| {
            location.pathname.track();
            cart_signal.collapsed.set(false);
        });

        cart_signal
    }

    pub fn set_collapse(&self) {
        self.collapsed.update(|open| *open = !*open);
    }

    pub fn set_quantity(&self, count: i32, variant_id: &str) {
        self.items.1.update(|items| {
            if let Some(item) = items.iter_mut().find(|item| item.product_id == variant_id) {
                item.quantity = count;
            }
        });
    }

    pub fn add_to_cart(&self, cart_item: CartItem) {
        self.items.1.update(|items| {
            if let Some(item) = items
                .iter_mut()
                .find(|item| item.product_id == cart_item.product_id)
            {
                item.quantity += cart_item.quantity;
            } else {
                items.push(cart_item);
            }
        });
    }

    pub fn saturating_from_cart(&self, product_id: &str, addition: bool) {
        self.items.1.update(|items| {
            if let Some(item) = items.iter_mut().find(|item| item.product_id == product_id) {
                item.quantity = if addition {
                    item.quantity.saturating_add(1)
                } else if item.quantity > 1 {
                    item.quantity.saturating_sub(1)
                } else {
                    1
                }
            }
        });
    }

    pub fn remove_from_cart(&self, product_id: &str) {
        self.items.1.update(|items| {
            *items = items
                .iter()
                .filter(|&item| (item.product_id != product_id))
                .map(std::borrow::ToOwned::to_owned)
                .collect::<Vec<_>>();
        });
    }

    pub fn remove_all_from_cart(&self) {
        self.items.1.update(Vec::clear);
    }

    pub fn transfer_to_cart(&self, items: Vec<CartItem>) {
        for item in items {
            self.add_to_cart(item);
        }
    }

    #[must_use]
    pub fn get_item_count(&self) -> i32 {
        self.items
            .0
            .get()
            .iter()
            .fold(0, |count, item| count + item.quantity)
    }

    #[must_use]
    pub async fn get_item_sum(&self) -> i32 {
        self.get_items()
            .await
            .iter()
            .fold(0, |sum, (item, product)| {
                sum + item.quantity * product.price.unwrap_or_default()
            })
    }

    #[must_use]
    pub fn get_raw_items(&self) -> Vec<CartItem> {
        self.items.0.get()
    }

    #[must_use]
    pub async fn get_items(&self) -> Vec<(CartItem, Product)> {
        let configuration_context = expect_context::<ConfigurationContext>();
        let mut cart_products: Vec<(CartItem, Product)> = Vec::new();
        for item in self.items.0.get_untracked() {
            let items_in_state = self.items_in_state.get_untracked();
            let product_state = if let Some(item_in_state) = items_in_state
                .iter()
                .find(|item_in_state| item_in_state.id == item.product_id)
            {
                Some(item_in_state.clone())
            } else {
                match api_get_call::<Product>(
                    &format!(
                        "{}/products/{}",
                        configuration_context.get_app_environment().backend_endoint,
                        item.product_id
                    ),
                    None,
                )
                .await
                {
                    Ok(product) => {
                        self.items_in_state.update(|upd| upd.push(product.clone()));
                        Some(product)
                    }
                    Err(_) => None,
                }
            };
            if let Some(product) = product_state {
                cart_products.push((item.clone(), product.clone()));
            }
        }
        cart_products
    }

    #[must_use]
    pub fn check_shipping_costs(&self, shipping_cost: i32) -> i32 {
        let configuration_context = expect_context::<ConfigurationContext>();
        let app_configuration = configuration_context.get_app_environment();

        if shipping_cost > app_configuration.free_shipping {
            0
        } else {
            app_configuration.shipping_fee
        }
    }

    #[must_use]
    pub fn is_collapsed(&self) -> bool {
        self.collapsed.get()
    }

    #[must_use]
    pub async fn is_locked(&self) -> bool {
        let mut locked = false;
        for item in &self.get_items().await {
            if !item.1.is_orderable {
                locked = true;
                break;
            }
        }
        locked
    }
}
