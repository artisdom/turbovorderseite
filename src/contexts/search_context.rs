#[allow(clippy::wildcard_imports)]
use leptos::*;

#[derive(Debug, Copy, Clone)]
pub struct SearchContext {
    category_signal: RwSignal<Option<String>>,
}

impl Default for SearchContext {
    fn default() -> Self {
        Self::new()
    }
}

impl SearchContext {
    #[must_use]
    pub fn new() -> Self {
        Self {
            category_signal: create_rw_signal(None),
        }
    }

    pub fn set_category(&self, category: String) {
        self.category_signal.set(Some(category));
    }

    #[must_use]
    pub fn get_category(&self) -> Option<String> {
        self.category_signal.get()
    }
}
