use std::collections::HashSet;

use crate::fetch::{api_get_call, api_post_call};

#[allow(clippy::wildcard_imports)]
use leptos::*;

use leptos_oidc::Auth;
use model::{Checkout, CheckoutGuest, NewCheckout, NewOrderItem, Order, Product, ValidateCheckout};

use super::cart_context::CartItem;

const DEFAULT_ORDER_PAGE_BEGIN: usize = 1;

pub enum OrderResponse {
    Success,
    ValidationError,
    ResponseError,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OrderContext {
    backend_url: String,
    pub current_orders_page: RwSignal<usize>,
    total_pages_in_state: RwSignal<usize>,
}

impl OrderContext {
    pub fn init(backend_url: String) {
        provide_context(Self {
            backend_url,
            current_orders_page: create_rw_signal(DEFAULT_ORDER_PAGE_BEGIN),
            total_pages_in_state: create_rw_signal(DEFAULT_ORDER_PAGE_BEGIN),
        });
    }

    pub async fn send_checkout(&self, items: Vec<(CartItem, Product)>) -> Option<String> {
        let auth_context = expect_context::<Auth>();

        if items.is_empty() {
            return None;
        }

        if let Some(access_token) = auth_context.access_token() {
            match api_post_call::<NewCheckout, Checkout>(
                &format!("{}/checkout/self", self.backend_url),
                Some(NewCheckout {
                    items: items
                        .iter()
                        .map(|(item, _)| NewOrderItem {
                            product: item.product_id.clone(),
                            quantity: item.quantity,
                        })
                        .collect::<HashSet<_>>(),
                }),
                Some(access_token),
            )
            .await
            {
                Ok(response) => response.checkout_url,
                Err(_) => None,
            }
        } else {
            match api_post_call::<NewCheckout, CheckoutGuest>(
                &format!("{}/checkout/guest", self.backend_url),
                Some(NewCheckout {
                    items: items
                        .iter()
                        .map(|(item, _)| NewOrderItem {
                            product: item.product_id.clone(),
                            quantity: item.quantity,
                        })
                        .collect::<HashSet<_>>(),
                }),
                None,
            )
            .await
            {
                Ok(response) => Some(response.checkout_url),
                Err(_) => None,
            }
        }
    }

    pub async fn validate_checkout(&self, session_id: String) -> Option<Order> {
        api_post_call::<ValidateCheckout, Order>(
            &format!("{}/checkout/validate", self.backend_url),
            Some(ValidateCheckout {
                validation_key: session_id,
            }),
            None,
        )
        .await
        .ok()
    }

    pub async fn get_orders(&self, page_size: usize, order_dir: String, all: bool) -> Vec<Order> {
        let auth_context = expect_context::<Auth>();
        match api_get_call::<pagination::models::Pagination<Order>>(
            &format!(
                "{}/orders{}?size={page_size}&order_dir={order_dir}&page={}",
                self.backend_url,
                if all { "" } else { "/self" },
                self.get_page()
            ),
            auth_context.access_token(),
        )
        .await
        .ok()
        {
            Some(response) => {
                self.set_total_pages(response.total_pages);
                response.data
            }
            None => Vec::new(),
        }
    }

    pub async fn get_order(&self, order_id: String) -> Option<Order> {
        let auth_context = expect_context::<Auth>();
        api_get_call::<Order>(
            &format!("{}/orders/{order_id}", self.backend_url),
            auth_context.access_token(),
        )
        .await
        .ok()
    }

    #[must_use]
    pub fn get_page(&self) -> usize {
        self.current_orders_page.get()
    }

    pub fn increment_page(&self) {
        self.current_orders_page.update(|upd| *upd += 1);
    }

    pub fn decrement_page(&self) {
        if self.get_page() > 1 {
            self.current_orders_page.update(|upd| *upd -= 1);
        }
    }

    #[must_use]
    pub fn get_total_pages(&self) -> usize {
        self.total_pages_in_state.get()
    }

    pub fn set_total_pages(&self, amount: usize) {
        self.total_pages_in_state.set(amount);
    }
}
