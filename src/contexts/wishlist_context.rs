use leptos::{Signal, SignalGet, SignalUpdate, WriteSignal};
use leptos_use::{storage::use_local_storage, utils::JsonCodec};

#[derive(Clone)]
pub struct WishlistContext {
    pub products: (Signal<Vec<String>>, WriteSignal<Vec<String>>),
}

impl Default for WishlistContext {
    fn default() -> Self {
        Self::new()
    }
}

impl WishlistContext {
    #[must_use]
    pub fn new() -> Self {
        let (signal, write, _) = use_local_storage::<Vec<String>, JsonCodec>("wishlist");
        Self {
            products: (signal, write),
        }
    }

    pub fn toggle(&self, sku: String) {
        self.products.1.update(|upd| {
            if !upd.contains(&sku) {
                upd.push(sku);
            } else if let Some(idx) = upd.iter_mut().position(|model_sku| *model_sku == sku) {
                upd.remove(idx);
            }
        });
    }

    #[must_use]
    pub fn items_count(&self) -> usize {
        self.products.0.get().len()
    }
}
