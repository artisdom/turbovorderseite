use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use url::Url;

use crate::cdn::{CdnNetwork, CdnPullType};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct AppEnvironment {
    pub auth_authorization_endpoint: String,
    pub auth_client_id: String,
    pub auth_scope: String,
    pub auth_redirect_uri: String,
    pub auth_audience: String,
    pub app_deployment: String,
    pub backend_endoint: String,
    pub cdn_host: String,
    pub login_redirect_endpoint: String,
    pub logout_redirect_endpint: String,
    pub meilisearch_endpoint: String,
    pub meilisearch_api_key: String,
    pub free_shipping: i32,
    pub shipping_fee: i32,
    pub company_phone: String,
    pub company_email: String,
    pub company_registration: String,
    pub company_name: String,
    pub company_street: String,
    pub company_postalcode: String,
    pub company_city: String,
    pub company_country: String,
    pub content_networks: HashMap<String, CdnNetwork>,
    pub pull_defaults: HashMap<CdnPullType, Url>,
}

impl AppEnvironment {
    /// # Panics
    ///
    /// Will panic if fallback url cannot parsed
    #[must_use]
    pub fn init() -> Self {
        let placeholder_image = std::env!("CDN_FALLBACK");

        let mut content_networks: HashMap<String, CdnNetwork> = HashMap::new();
        let host = std::env!("CDN_HOST").replace("https://", "");

        content_networks.insert(
            String::from(std::env!("CDN_EXTERNAL")),
            CdnNetwork {
                untracked_segments: vec![],
                fallback_resource: Url::parse(placeholder_image).ok(),
                host: host.clone(),
            },
        );
        content_networks.insert(
            String::from("moturbo.com"),
            CdnNetwork {
                host: host.clone(),
                untracked_segments: vec![],
                fallback_resource: Url::parse(placeholder_image).ok(),
            },
        );
        content_networks.insert(
            String::from("staging.moturbo.com"),
            CdnNetwork {
                host,
                untracked_segments: vec![],
                fallback_resource: Url::parse(placeholder_image).ok(),
            },
        );

        let mut pull_defaults: HashMap<CdnPullType, Url> = HashMap::new();
        pull_defaults.insert(CdnPullType::Image, Url::parse(placeholder_image).unwrap());

        Self {
            auth_authorization_endpoint: std::env!("AUTH_AUTHORIZATION_ENDPOINT").to_owned(),
            auth_client_id: std::env!("AUTH_CLIENT_ID").to_owned(),
            auth_scope: std::env!("AUTH_SCOPE").to_owned(),
            auth_redirect_uri: std::env!("AUTH_REDIRECT_URI").to_owned(),
            auth_audience: std::env!("AUTH_AUDIENCE").to_owned(),
            app_deployment: std::env!("APP_DEPLOYMENT").to_owned(),
            backend_endoint: std::env!("BACKEND_ENDPOINT").to_owned(),
            cdn_host: std::env!("CDN_HOST").to_owned(),
            login_redirect_endpoint: std::env!("LOGIN_REDIRECT_ENDPOINT").to_owned(),
            logout_redirect_endpint: std::env!("LOGOUT_REDIRECT_ENDPOINT").to_owned(),
            meilisearch_endpoint: std::env!("MEILISEARCH_ENDPOINT").to_owned(),
            meilisearch_api_key: std::env!("MEILISEARCH_API_KEY").to_owned(),
            free_shipping: std::env!("FREE_SHIPPING")
                .to_owned()
                .parse::<i32>()
                .unwrap(),
            shipping_fee: std::env!("SHIPPING_FEE").to_owned().parse::<i32>().unwrap(),
            company_phone: std::env!("COMPANY_PHONE").to_owned(),
            company_email: std::env!("COMPANY_EMAIL").to_owned(),
            company_registration: std::env!("COMPANY_REGISTRATION").to_owned(),
            company_name: std::env!("COMPANY_NAME").to_owned(),
            company_street: std::env!("COMPANY_STREET").to_owned(),
            company_postalcode: std::env!("COMPANY_POSTAL_CODE").to_owned(),
            company_city: std::env!("COMPANY_CITY").to_owned(),
            company_country: std::env!("COMPANY_COUNTRY").to_owned(),
            content_networks,
            pull_defaults,
        }
    }
}
