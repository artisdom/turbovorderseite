use http::StatusCode;
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub mod entry;
pub mod template;

pub type AppResult<R> = Result<R, AppError>;

#[derive(Clone, Error, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[allow(clippy::module_name_repetitions)]
pub enum AppError {
    #[error("Forwarding out of isomorphic state")]
    IsomorphicForward,
    #[error("Not found")]
    NotFound,
    #[error("Forbidden")]
    Forbidden,
    #[error("Product not available")]
    InvalidVariant,
    #[error("Request failed, invalid response")]
    InvalidResponse,
    #[error("Mutex error")]
    MutexError,
    #[error("API error: Not reachable")]
    ApiError(String),
    #[error("Parsing error")]
    ParsingError(String),
    #[error("Storage invalid")]
    StorageInvalid,
    #[error("serde_json error: {0}")]
    SerdeError(String),
    #[error("jsonwebtoken error: {0}")]
    JWTError(String),
    #[error("reqwest error: {0}")]
    ReqwestError(String),
}

impl From<reqwest::Error> for AppError {
    fn from(value: reqwest::Error) -> Self {
        Self::ReqwestError(value.to_string())
    }
}

impl From<serde_json::Error> for AppError {
    fn from(value: serde_json::Error) -> Self {
        Self::SerdeError(value.to_string())
    }
}

impl From<jsonwebtoken::errors::Error> for AppError {
    fn from(value: jsonwebtoken::errors::Error) -> Self {
        Self::JWTError(value.to_string())
    }
}

impl AppError {
    #[must_use]
    pub fn status_code(&self) -> StatusCode {
        match self {
            AppError::NotFound => StatusCode::NOT_FOUND,
            AppError::InvalidVariant | AppError::IsomorphicForward => StatusCode::NO_CONTENT,
            AppError::Forbidden => StatusCode::FORBIDDEN,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
