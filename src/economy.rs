use chrono::{DateTime, Datelike, Days, FixedOffset, TimeZone, Utc};
use null_kane::{Currency, CurrencyLocale};

pub type ShopCurrency = Currency<Localization>;

#[derive(Default, Clone, Copy)]
pub enum Localization {
    #[default]
    De,
}

impl CurrencyLocale for Localization {
    fn separator(&self) -> char {
        ','
    }

    fn thousand_separator(&self) -> char {
        '.'
    }

    fn currency_symbol(&self) -> &'static str {
        "€"
    }
}

#[must_use]
pub fn format_rfc3339_to_date(timestamp: &str) -> String {
    if let Ok(parsed) = DateTime::parse_from_rfc3339(timestamp) {
        format!(
            "{:02}.{:02}.{}",
            parsed.day(),
            parsed.month(),
            parsed.year()
        )
    } else {
        String::new()
    }
}

#[must_use]
pub fn format_timestamp_to_date(timestamp: i64) -> String {
    if let Some(parsed) = Utc.timestamp_opt(timestamp, 0).single() {
        format!(
            "{:02}.{:02}.{}",
            parsed.day(),
            parsed.month(),
            parsed.year()
        )
    } else {
        String::new()
    }
}

pub fn get_delivery_time() -> (DateTime<FixedOffset>, DateTime<FixedOffset>) {
    let now: DateTime<Utc> = Utc::now();
    let now: DateTime<FixedOffset> = now.with_timezone(&TimeZone::from_offset(
        &FixedOffset::east_opt(2 * 3600).unwrap(),
    ));
    let weekend_addition = match now.weekday() {
        chrono::Weekday::Fri => 3,
        chrono::Weekday::Sat => 2,
        chrono::Weekday::Sun => 1,
        _ => 0,
    };

    (
        now.checked_add_days(Days::new(2 + weekend_addition))
            .unwrap(),
        now.checked_add_days(Days::new(4 + weekend_addition))
            .unwrap(),
    )
}
