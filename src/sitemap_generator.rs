use crate::fetch::api_get_call;

use model::Product;
use pagination::models::Pagination;
use std::{
    fs::{self, File},
    io::Write,
    time::{Duration, SystemTime},
};

const BASE_API_PATH: &str = "https://backend.moturbo.com/api";
const BASE_SITE_URL: &str = "https://moturbo.com";
const SITEMAP_PATH: &str = "public/sitemap.xml";
const PUBLIC_ROUTING_LIST: [&str; 10] = [
    "company",
    "source",
    "support",
    "shipping",
    "retoure",
    "privacy",
    "revocation",
    "battery-oil-electro-notes",
    "terms",
    "imprint",
];

pub async fn create_sitemap() {
    if !check_file() {
        return;
    }

    let mut sitemap: Vec<String> = Vec::new();

    // XML definition
    sitemap.push("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".to_owned());

    // Start sitemap URL set
    sitemap.push("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">".to_owned());

    // Append root site
    append_location("", &mut sitemap);

    // General public routing
    for path in PUBLIC_ROUTING_LIST {
        append_location(path, &mut sitemap);
    }

    // Products sitemap
    let product_endpoints = get_product_endpoints().await;
    for id in product_endpoints {
        append_location(&format!("product/{id}"), &mut sitemap);
    }

    // End of URL sitemap set
    sitemap.push("</urlset>".to_owned());

    // Create or overwrite a final sitemap
    create_file(&sitemap);
}

fn append_location(location: &str, sitemap: &mut Vec<String>) {
    sitemap.push("<url>".to_owned());
    sitemap.push(format!("<loc>{BASE_SITE_URL}/{location}</loc>"));
    sitemap.push("</url>".to_owned());
}

fn check_file() -> bool {
    if let Ok(file_metadata) = fs::metadata(SITEMAP_PATH) {
        let modified_time = file_metadata
            .modified()
            .expect("Rerun sitemap generation cause modified date error");
        let current_time = SystemTime::now();
        return current_time
            .duration_since(modified_time)
            .expect("Time went backwards")
            > Duration::from_secs(24 * 60 * 60);
    }
    true
}

fn create_file(sitemap: &[String]) {
    let mut file = File::create(SITEMAP_PATH).expect("Cannot create sitemap file");
    for line in sitemap {
        file.write_all(format!("{line}\n").as_bytes()).unwrap();
    }
}

async fn get_product_endpoints() -> Vec<String> {
    let mut product_id_set: Vec<String> = Vec::new();
    let mut current_page = 0;
    loop {
        let path = format!(
            "{BASE_API_PATH}/products?size=100&page={}",
            current_page + 1
        );
        let products_callback = api_get_call::<Pagination<Product>>(&path, None)
            .await
            .expect("Endpoint response error on fetch");
        let product_id_slice = products_callback
            .data
            .iter()
            .map(|product| product.id.clone())
            .collect::<Vec<String>>();
        product_id_set.extend(product_id_slice);
        println!("Generating sitemap... Fetching page[{current_page}]");
        if products_callback.current_page >= products_callback.total_pages {
            break;
        }
        current_page = products_callback.current_page;
    }
    product_id_set
}
