#![allow(clippy::module_name_repetitions)]

use leptos::provide_context;
use reqwest::Url;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Deserialize, Serialize)]
pub enum CdnPullType {
    Image,
    Video,
    Sourcecode,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct CdnNetwork {
    pub host: String,
    pub untracked_segments: Vec<String>,
    pub fallback_resource: Option<Url>,
}

#[derive(Clone, Copy)]
pub struct CdnOptions {
    pub quality: Option<isize>,
    pub size: Option<usize>,
    pub crop_width: Option<usize>,
    pub crop_height: Option<usize>,
    pub crop_x: Option<usize>,
    pub crop_y: Option<usize>,
}

#[derive(Clone)]
#[allow(clippy::needless_pass_by_value)]
pub struct ContentProvider {
    networks: HashMap<String, CdnNetwork>,
    pull_defaults: HashMap<CdnPullType, Url>,
}

impl ContentProvider {
    pub fn init(networks: HashMap<String, CdnNetwork>, pull_defaults: HashMap<CdnPullType, Url>) {
        provide_context(Self {
            networks,
            pull_defaults,
        });
    }

    /// # Panics
    ///
    /// Will panic if the host cannot be parsed from the url parameter
    #[must_use]
    pub fn provide_content_network(
        &self,
        provider_url: Result<Url, url::ParseError>,
        options: &CdnOptions,
        pull_type: CdnPullType,
    ) -> Url {
        if let Ok(mut url) = provider_url.clone() {
            if let Some(network) = self
                .networks
                .get(url.host_str().expect("Cannot get host from url"))
            {
                if url.set_host(Some(&network.host)).is_ok() {
                    parse_options(&mut url, options);
                    url.set_path(&format!(
                        "{}/{}",
                        provider_url
                            .unwrap()
                            .host_str()
                            .expect("Cannot get host from url"),
                        url.path_segments()
                            .expect("No path segments specified")
                            .filter(|segment| !network
                                .untracked_segments
                                .contains(&segment.to_owned().to_owned()))
                            .collect::<Vec<&str>>()
                            .join("/")
                    ));
                    return url;
                }
                return network.fallback_resource.clone().unwrap_or(
                    self.pull_defaults
                        .get(&pull_type)
                        .expect("Cannot fallback! Pull Type Default is not present!")
                        .clone(),
                );
            }
        }
        self.pull_defaults
            .get(&pull_type)
            .expect("Cannot fallback! Pull Type Default is not present!")
            .clone()
    }

    /// # Panics
    ///
    /// Will panic when `url_str` cannot parsed to url type
    #[must_use]
    pub fn overwrite_raw_query_set(&self, url_str: &str, options: &CdnOptions) -> Url {
        let mut url = Url::parse(url_str).expect("Incorrect raw url");
        parse_options(&mut url, options);
        url
    }
}

fn parse_options(url: &mut Url, options: &CdnOptions) {
    let queries = vec![
        options.quality.map(|q| format!("quality={q}")),
        options.size.map(|s| format!("size={s}")),
        options.crop_width.map(|w| format!("width={w}")),
        options.crop_height.map(|h| format!("height={h}")),
        options.crop_x.map(|x| format!("x={x}")),
        options.crop_y.map(|y| format!("y={y}")),
    ]
    .into_iter()
    .flatten()
    .collect::<Vec<_>>();
    url.set_query(Some(
        queries
            .iter()
            .enumerate()
            .fold(String::new(), |acc, (idx, param)| {
                acc + param + if idx == queries.len() - 1 { "" } else { "&" }
            })
            .as_str(),
    ));
}
