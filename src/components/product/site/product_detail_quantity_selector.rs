use leptos::{html::Input, *};

#[must_use]
#[component]
pub fn ProductDetailQuantitySelector(quantity_signal: RwSignal<i32>) -> impl IntoView {
    let input_el = create_node_ref::<Input>();

    // Effect to keep value consistent
    // Quantity signal is subscribed to this effect
    create_effect(move |_| {
        input_el
            .get()
            .unwrap()
            .set_value(&quantity_signal.get().to_string());
    });

    view! {
        <div class="flex items-center space-x-2 md:mr-3">
            <button
                on:click=move |_| {
                    if quantity_signal.get() > 1 {
                        quantity_signal.update(|upd| *upd -= 1);
                    }
                }

                text
                class="text-lg font-bold text-white md:text-black md:dark:text-white mr-2 hover:text-[var(--primary-color)]"
            >
                "-"
            </button>
            <input
                node_ref=input_el
                class="appearance-none w-10 md:dark:text-white p-0.5 border text-white md:text-black bg-[var(--dark-background-color)] md:bg-white dark:bg-[var(--secondary-color)]"
                type="number"
                value=move || quantity_signal.get()
                min="1"
                on:change=move |ev| {
                    quantity_signal.set(event_target_value(&ev).parse::<i32>().unwrap_or(1));
                }
            />

            <button
                on:click=move |_| {
                    quantity_signal.update(|upd| *upd += 1);
                }

                class="text-lg font-bold text-white md:text-black md:dark:text-white hover:text-[var(--primary-color)]"
            >
                "+"
            </button>
        </div>
    }
}
