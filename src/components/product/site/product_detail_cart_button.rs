use crate::{
    components::orderflow::cart::minicart::added_modal::AddedModal,
    contexts::cart_context::{CartContext, CartItem},
};

use leptos::{html::Div, *};
use model::Product;
use wasm_bindgen::UnwrapThrowExt;

#[must_use]
#[component]
pub fn ProductDetailCartButton(product: Product, quantity_signal: RwSignal<i32>) -> impl IntoView {
    let cart_context = expect_context::<CartContext>();
    let area_el = create_node_ref::<Div>();

    let product_id = product.id.clone();
    let add_cart_action = create_action(move |()| {
        let product_id = product_id.to_owned();
        async move {
            cart_context.add_to_cart(CartItem {
                product_id,
                quantity: quantity_signal.get_untracked(),
            });
            let _ = area_el
                .get_untracked()
                .unwrap_throw()
                .style("display", "block");
        }
    });

    let added_product = product.clone();

    view! {
        <button
            on:click=move |_| add_cart_action.dispatch(())
            type="button"
            class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
            "In den Einkaufswagen"
        </button>
        {move || {
            let quantity = quantity_signal.get();
            view! {
                <AddedModal
                    product=added_product.clone()
                    quantity=quantity
                    area_el=area_el
                />
            }
        }}
    }
}
