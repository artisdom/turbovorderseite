use crate::components::product::site::product_detail_gallery_zoom::ProductDetailGalleryZoom;
use crate::contexts::queue_context::QueuePosition;
use leptos::{html::Div, *};
use leptos_use::use_element_hover;
use model::Media;
use url::Url;

use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::queue_context::QueueContext,
};

const SVG_ARROW_STYLE: &str =
    "absolute transition duration-150 ease-in-out h-full max-w-[25%] text-[var(--primary-color)]";

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn PrimaryMedia(
    primary_signal: RwSignal<String>,
    queue_context_signal: RwSignal<QueueContext<Media>>,
    product_name: String,
) -> impl IntoView {
    let cdn_context = expect_context::<ContentProvider>();
    let hover_track = create_node_ref::<Div>();
    let is_hovered = use_element_hover(hover_track);
    let zoom_signal = create_rw_signal(false);

    let opt_url = move || {
        cdn_context.provide_content_network(
            Url::parse(&primary_signal.get()),
            &CdnOptions {
                quality: Some(90),
                size: Some(670),
                crop_width: None,
                crop_height: None,
                crop_x: Some(0),
                crop_y: Some(0),
            },
            CdnPullType::Image,
        )
    };

    view! {
        <div node_ref=hover_track class="relative flex justify-center md:justify-between items-center">
            <Show when=move || zoom_signal.get()>
                <ProductDetailGalleryZoom url=primary_signal.get() zoom_signal=zoom_signal/>
            </Show>
            <Show when=move || {
                !matches!(
                    queue_context_signal.get_untracked().current.get(),
                    QueuePosition::Start(_)
                )
            }>
                <svg
                    on:click=move |_| queue_context_signal.get_untracked().previous()
                    class=move || {
                        format!(
                            "select-none left-0 pr-[17.5%] {} {}",
                            SVG_ARROW_STYLE,
                            if is_hovered.get() { "opacity-100" } else { "opacity-20" },
                        )
                    }

                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="m15 19-7-7 7-7"
                    ></path>
                </svg>
            </Show>
            <div class="md:w-[640px] bg-white border-lg dark:rounded-2xl flex justify-center">
                <img
                    on:click=move |_| zoom_signal.update(|upd| *upd = !*upd)
                    class="select-none dark:rounded-2xl object-contain h-[380px] md:h-[447px]"
                    src=move || opt_url().to_string()
                    alt=format!("product primary picture of the product {}", product_name)
                />
            </div>
            <Show when=move || {
                !matches!(queue_context_signal.get_untracked().current.get(), QueuePosition::End(_))
                    && (queue_context_signal.get_untracked().items.len() > 1)
            }>
                <svg
                    on:click=move |_| queue_context_signal.get_untracked().next()
                    class=move || {
                        format!(
                            "select-none right-0 pl-[17.5%] {} {}",
                            SVG_ARROW_STYLE,
                            if is_hovered.get() { "opacity-100" } else { "opacity-20" },
                        )
                    }

                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="m9 5 7 7-7 7"
                    ></path>
                </svg>
            </Show>
        </div>
    }
}
