#![allow(clippy::needless_pass_by_value)]

use super::product_detail_available::ProductAvailable;
use crate::{
    components::product::{
        price_availability::PriceAvailability,
        site::{
            product_detail_cart_button::ProductDetailCartButton,
            product_detail_quantity_selector::ProductDetailQuantitySelector,
        },
    },
    contexts::wishlist_context::WishlistContext,
};

use leptos::*;
use model::Product;

#[must_use]
#[component]
pub fn ProductDetailProperties(product: Product) -> impl IntoView {
    let wishlist_context = create_rw_signal(expect_context::<WishlistContext>());
    let quantity_signal = create_rw_signal(1i32);

    let toggle = create_rw_signal(
        wishlist_context
            .get_untracked()
            .products
            .0
            .get_untracked()
            .contains(&product.id),
    );

    let wishlist_action = create_action({
        let product_id = product.id.clone();
        move |()| {
            let product_id = product_id.clone();
            async move {
                wishlist_context.update(|upd| upd.toggle(product_id.clone()));
                toggle.update(|upd| *upd = !*upd);
            }
        }
    });

    let is_orderable = product.is_orderable;
    let detail_cart_product = product.clone();

    view! {
        <div class="md:ml-4 w-full md:w-full">
            <p class="font-black text-2xl hidden md:block">{product.name.clone()}</p>
            <p class="text-sm mt-2">{product.id.clone()}</p>
            <div class="md:mt-4 md:border-t border-gray-300">
                <PriceAvailability
                    some_class="text-3xl mt-4 font-bold"
                    none_class="text-lg mt-4 mb-4"
                    price=product.price
                >
                    <p class="text-sm">
                        "MwSt. wird nicht ausgewiesen (Kleinunternehmer, § 19 UStG)"
                    </p>
                    <div class="mt-4 pt-4 border-t border-gray-300">
                        <ProductAvailable available=product.is_orderable/>
                    </div>
                    <div class="fixed md:relative flex justify-between md:justify-start bottom-0 left-0 w-full bg-[var(--dark-background-color)] md:opacity-100 md:bg-white md:dark:text-white md:dark:bg-[var(--secondary-color)] items-center p-4 md:p-0 md:mt-4 md:pt-4">
                        <ProductDetailQuantitySelector quantity_signal=quantity_signal/>
                        <ProductDetailCartButton
                            product=detail_cart_product.clone()
                            quantity_signal=quantity_signal
                        />
                        <Show when={
                            let is_orderable = product.is_orderable;
                            move || is_orderable
                        }>
                            <button
                                on:click=move |_| wishlist_action.dispatch(())
                                type="button"
                                class=move || {
                                    format!(
                                        "md:ml-4 hover:animate-heart {}",
                                        if toggle.get() {
                                            "fill-[var(--primary-color)]"
                                        } else {
                                            "fill-red-600"
                                        },
                                    )
                                }
                            >

                                <svg
                                    class="w-6 h-6 dark:text-white"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    // fill="var(--primary-color)"
                                    viewBox="0 0 20 18"
                                >
                                    <path d="M17.947 2.053a5.209 5.209 0 0 0-3.793-1.53A6.414 6.414 0 0 0 10 2.311 6.482 6.482 0 0 0 5.824.5a5.2 5.2 0 0 0-3.8 1.521c-1.915 1.916-2.315 5.392.625 8.333l7 7a.5.5 0 0 0 .708 0l7-7a6.6 6.6 0 0 0 2.123-4.508 5.179 5.179 0 0 0-1.533-3.793Z"></path>
                                </svg>
                            </button>
                        </Show>
                    </div>
                </PriceAvailability>
                <Show when=move || !is_orderable>
                    <button
                        on:click=move |_| wishlist_action.dispatch(())
                        type="button"
                        class="text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                    >
                        {move || {
                            if toggle.get() {
                                "Von Merkliste entfernen"
                            } else {
                                "Auf die Merkliste"
                            }
                        }}

                    </button>
                </Show>
            </div>
        </div>
    }
}
