use leptos::*;
use model::Media;
use url::Url;

use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::queue_context::QueueContext,
};

#[must_use]
#[component]
pub fn GalleryMedias(
    primary_signal: RwSignal<String>,
    queue_context_signal: RwSignal<QueueContext<Media>>,
    product_name: String,
) -> impl IntoView {
    let cdn_context = expect_context::<ContentProvider>();

    view! {
        <Show when=move || (queue_context_signal.get().items.clone().len() > 1)>
            <ul class="grid grid-cols-5 md:grid-cols-7 gap-[0.25rem] md:gap-2">
                {queue_context_signal
                    .get()
                    .items
                    .clone()
                    .into_iter()
                    .enumerate()
                    .map(|(idx, media)| {
                        let raw_url = media.url;
                        let opt_url = cdn_context
                            .provide_content_network(
                                Url::parse(&raw_url),
                                &CdnOptions {
                                    quality: None,
                                    size: Some(100),
                                    crop_width: Some(100),
                                    crop_height: Some(100),
                                    crop_x: Some(0),
                                    crop_y: Some(0),
                                },
                                CdnPullType::Image,
                            );
                        view! {
                            <li
                                on:click=move |_| {
                                    queue_context_signal.get().set_index(idx);
                                    primary_signal.set(raw_url.clone());
                                }

                                class=move || {
                                    format!(
                                        "rounded-lg dark:rounded-2xl w-[4.25rem] h-[4.25rem] md:w-[5.25rem] md:h-[5.25rem] flex items-center justify-center bg-white transition ease-in-out duration-100 border dark:border-4 {}",
                                        if queue_context_signal.get().index.get().eq(&idx) {
                                            "border-[var(--primary-color)]"
                                        } else {
                                            "hover:border-gray-300"
                                        },
                                    )
                                }
                            >

                                <div class="flex justify-center w-[4.25rem] md:w-[6.25rem] max-h-[6.25rem] dark:rounded-xl">
                                    <img
                                        class="select-none dark:rounded-2xl"
                                        src=opt_url.to_string()
                                        alt=media
                                            .alt
                                            .unwrap_or(
                                                format!("product picture of the product {product_name}"),
                                            )
                                    />

                                </div>
                            </li>
                        }
                    })
                    .collect_view()}
            </ul>
        </Show>
    }
}
