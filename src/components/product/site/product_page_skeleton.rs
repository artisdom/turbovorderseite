pub use leptos::*;
use leptos_meta::Title;

#[must_use]
#[component]
pub fn ProductPageSkeleton() -> impl IntoView {
    view! {
        <Title text="Loading..."/>
        <div class="w-full md:flex md:justify-center">
            <div class="flex flex-col md:grid gap-x-1 gap-y-0 gap-x-4 gap-y-4 w-full md:max-w-[84rem] md:my-12">
                <div class="hidden lg:flex w-full col-start-1 col-end-2 row-start-1 row-end-2 bg-white dark:bg-[var(--secondary-color)] dark:text-white shadow p-4 rounded-2xl text-xs md:text-sm">
                    <ul class="flex items-center space-x-2">
                        <li class="flex items-center">
                            <div class="animate-pulse h-4 my-0.5 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[37rem]"></div>
                        </li>
                    </ul>
                </div>
                <div class="dark:text-white row-start-2 row-end-3">
                    <div class="p-4 dark:bg-[var(--secondary-color)] bg-white md:shadow md:rounded-2xl md:flex inline-block">
                        <div class="md:hidden">
                            <div class="animate-pulse h-4 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-full"></div>
                            <div class="animate-pulse h-4 mt-2 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-2/5"></div>
                        </div>
                        <div class="flex flex-col space-y-4">
                            <div class="relative flex justify-between items-center">
                                <div class="md:w-[640px] mt-4 md:mt-0 bg-white border-lg dark:rounded-2xl flex justify-center">
                                    <svg
                                        class="animate-pulse h-[380px] md:h-[447px] w-full text-gray-200 dark:text-gray-600"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 18"
                                    >
                                        <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z"></path>
                                    </svg>
                                </div>
                            </div>
                            <ul class="grid grid-cols-5 md:grid-cols-7 gap-[0.25rem] md:gap-2">
                                <li class="rounded-lg dark:rounded-2xl w-[4.25rem] h-[4.25rem] md:w-[5.25rem] md:h-[5.25rem] flex items-center justify-center bg-white transition ease-in-out duration-100 border dark:border-4">
                                    <svg
                                        class="animate-pulse flex justify-center w-[4.25rem] md:w-[6.25rem] max-h-[6.25rem] dark:rounded-2xl text-gray-200 dark:text-gray-600"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 18"
                                    >
                                        <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z"></path>
                                    </svg>
                                </li>
                                <li class="rounded-lg dark:rounded-2xl w-[4.25rem] h-[4.25rem] md:w-[5.25rem] md:h-[5.25rem] flex items-center justify-center bg-white transition ease-in-out duration-100 border dark:border-4">
                                    <svg
                                        class="animate-pulse flex justify-center w-[4.25rem] md:w-[6.25rem] max-h-[6.25rem] dark:rounded-2xl text-gray-200 dark:text-gray-600"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 18"
                                    >
                                        <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z"></path>
                                    </svg>
                                </li>
                                <li class="rounded-lg dark:rounded-2xl w-[4.25rem] h-[4.25rem] md:w-[5.25rem] md:h-[5.25rem] flex items-center justify-center bg-white transition ease-in-out duration-100 border dark:border-4">
                                    <svg
                                        class="animate-pulse flex justify-center w-[4.25rem] md:w-[6.25rem] max-h-[6.25rem] dark:rounded-2xl text-gray-200 dark:text-gray-600"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 18"
                                    >
                                        <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z"></path>
                                    </svg>
                                </li>
                                <li class="rounded-lg dark:rounded-2xl w-[4.25rem] h-[4.25rem] md:w-[5.25rem] md:h-[5.25rem] flex items-center justify-center bg-white transition ease-in-out duration-100 border dark:border-4">
                                    <svg
                                        class="animate-pulse flex justify-center w-[4.25rem] md:w-[6.25rem] max-h-[6.25rem] dark:rounded-2xl text-gray-200 dark:text-gray-600"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        viewBox="0 0 20 18"
                                    >
                                        <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z"></path>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                        <div class="md:ml-4 flex flex-col w-full md:w-[500px] mt-2">
                            <div class="hidden md:flex flex-col">
                                <div class="animate-pulse h-4 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[15rem] md:w-[31rem]"></div>
                                <div class="animate-pulse mt-2 h-4 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[10rem]"></div>
                            </div>
                            <div class="animate-pulse mt-5 h-2 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[4rem]"></div>
                            <div class="animate-pulse mt-8 md:mt-12 h-6 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[6rem]"></div>
                        </div>
                    </div>
                </div>
                <div class="row-start-3 row-end-4 dark:text-white">
                    <div class="flex flex-col md:grid grid-cols-3 grid-rows-1 gap-x-4 gap-y-4">
                        <div class="col-start-1 p-4 col-end-3 row-start-1 row-end-1 bg-white h-min dark:bg-[var(--secondary-color)] md:shadow md:rounded-2xl">
                            <div class="animate-pulse h-4 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[12rem] mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                            <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50 mb-4"></div>
                        </div>
                        <div class="col-start-3 col-end-4 row-start-1 row-end-1 h-min bg-white md:shadow dark:bg-[var(--secondary-color)] p-4 rounded-2xl">
                            <div class="animate-pulse h-4 bg-gray-200 rounded-full dark:bg-[var(--secondary-color)]/50 w-[12rem] mb-4"></div>
                            <table class="w-full border-separate border-spacing-2 table-auto">
                                <tbody>
                                    <div class="flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                    <div class="animate-pulse flex items-center mb-4">
                                        <div class="animate-pulse h-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                        <div class="animate-pulse h-2 ml-2 bg-gray-200 w-full rounded-full dark:bg-[var(--secondary-color)]/50"></div>
                                    </div>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}
