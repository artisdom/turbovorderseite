#![allow(clippy::needless_pass_by_value)]

use leptos::*;

#[must_use]
#[component]
pub fn ProductDetailPropertiesSkeleton() -> impl IntoView {
    view! {
        <div class="md:ml-4 p-2 w-full md:w-[500px]">
            <p class="font-black text-2xl hidden md:block">"Loading..."</p>
            <div class="md:mt-6 md:border-t border-gray-300">
                <p class="md:mt-6 text-3xl font-bold">"0,00 €"</p>
                <p class="text-sm">"MwSt. wird nicht ausgewiesen (Kleinunternehmer, § 19 UStG)"</p>
                <div class="mt-4 border-t border-gray-300">
                    // <ProductAvailable status=variant_cl.stock/>
                    <div class="mt-4"></div>
                </div>
                // <button
                <div class="mt-4 border-gray-300"></div>
            </div>
        </div>
    }
}
