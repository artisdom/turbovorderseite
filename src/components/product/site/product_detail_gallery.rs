use leptos::*;
use model::Product;

use crate::{
    components::product::site::{
        product_detail_gallery_medias::GalleryMedias, product_detail_primary_media::PrimaryMedia,
    },
    contexts::queue_context::{QueueContext, QueuePosition},
};

#[must_use]
#[component]
pub fn ProductDetailGallery(product: Product) -> impl IntoView {
    let queue_context = QueueContext::new(product.medias);
    let queue_signal = create_rw_signal(queue_context);
    let primary_signal = create_rw_signal(String::new());

    create_isomorphic_effect(move |_| {
        queue_signal.get().current.track();
        primary_signal.set({
            match queue_signal.get().current.get() {
                QueuePosition::Start(curr)
                | QueuePosition::Inside(curr)
                | QueuePosition::End(curr) => curr.url,
            }
        });
    });
    create_isomorphic_effect({
        let preview_media = product.preview_media;
        move |_| {
            if let Some(preview_media) = preview_media.clone() {
                primary_signal.set(preview_media.url);
            }
        }
    });
    view! {
        <div class="flex flex-col space-y-4">
            <PrimaryMedia
                primary_signal=primary_signal
                queue_context_signal=queue_signal
                product_name=product.name.clone()
            />
            <GalleryMedias
                primary_signal=primary_signal
                queue_context_signal=queue_signal
                product_name=product.name
            />
        </div>
    }
}
