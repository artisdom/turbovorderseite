use std::collections::BTreeMap;

use leptos::*;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn ProductDetailMetaList(
    product_id: String,
    metadata: BTreeMap<String, String>,
) -> impl IntoView {
    view! {
        <div class="col-start-3 col-end-4 row-start-1 row-end-1 h-min bg-white md:shadow dark:bg-[var(--secondary-color)] p-4 rounded-2xl">
            <p class="mb-4 text-xl font-bold">"Artikel Details"</p>
            <table class="w-full border-separate border-spacing-2 table-auto">
                <tbody class="space-x-2">
                    <tr>
                        <td>
                            <b>Artikelnummer</b>
                        </td>
                        <td>{product_id}</td>
                    </tr>
                    {metadata
                        .iter()
                        .map(|(name, value)| {
                            view! {
                                <tr>
                                    <td>
                                        <b>{name}</b>
                                    </td>
                                    <td>{value}</td>
                                </tr>
                            }
                        })
                        .collect_view()}
                </tbody>
            </table>
        </div>
    }
}
