use crate::{components::standards::indicators::UnavailableIndicator, economy::get_delivery_time};

use leptos::*;

#[must_use]
#[component]
pub fn ProductAvailable(available: bool) -> impl IntoView {
    let (estimated_from, estimated_to) = get_delivery_time();

    view! {
        <div class="text-[#00A341]">
            <Show
                when=move || available
                fallback=move || {
                    view! {
                        <UnavailableIndicator>"Derzeit nicht verfügbar"</UnavailableIndicator>
                    }
                }
            >

                <div class="bg-green-100 text-green-800 dark:text-emerald-300 dark:bg-emerald-900 rounded-lg p-3">
                    <div class="flex items-center">
                        <svg
                            class="w-4 h-4 mr-1"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 16 12"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M1 5.917 5.724 10.5 15 1.5"
                            ></path>
                        </svg>
                        <b>"Verfügbar"</b>
                    </div>
                    <p class="mt-1 text-base">
                        "Voraussichtliche Versendung zwischen "
                        {format!(
                            "{}. - {}",
                            estimated_from.format("%d"),
                            estimated_to.format("%d.%m.%Y"),
                        )}

                    </p>
                </div>
            </Show>
        </div>
    }
}
