use leptos::*;
use pulldown_cmark::{html::push_html, Parser};

#[must_use]
#[component]
#[allow(clippy::needless_pass_by_value)]
pub fn ProductDetailDescription(description: String) -> impl IntoView {
    let description_resource = create_resource(
        move || description.clone(),
        move |description| async move {
            let mut output = String::new();
            push_html(&mut output, Parser::new(&description));
            output
        },
    );
    view! {
        <div class="col-start-1 p-4 col-end-3 row-start-1 row-end-1 bg-white h-min dark:bg-[var(--secondary-color)] md:shadow md:rounded-2xl">
            <p class="mb-4 text-xl font-bold">"Artikel Beschreibung"</p>
            <Suspense>
                <div
                    class="text-lg space-y-4"
                    inner_html=move || description_resource.try_get().unwrap_or_default()
                ></div>
            </Suspense>
        </div>
    }
}
