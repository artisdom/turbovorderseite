use leptos::*;
use leptos_router::A;
use model::CategoryRecursive;

#[must_use]
#[component]
pub fn ProductBreadcrumb(category: Option<CategoryRecursive>, name: String) -> impl IntoView {
    let categories_signal = create_rw_signal(Vec::<String>::new());

    if let Some(category) = category.clone() {
        categories_signal.set(extract_categories(category));
    }

    view! {
        <div class="hidden lg:flex w-full col-start-1 col-end-2 row-start-1 row-end-2 bg-white dark:bg-[var(--secondary-color)] dark:text-white shadow p-4 rounded-2xl text-xs md:text-sm">
            <ul class="flex space-x-2">
                <li>
                    <A href="/">"Start"</A>
                </li>
                {move || {
                    categories_signal
                        .get()
                        .into_iter()
                        .enumerate()
                        .map(|(idx, category)| {
                            let first = categories_signal.get_untracked()[0].clone();
                            view! {
                                <li class="flex items-center space-x-2 text-sm">
                                    <BreadIcon/>
                                    {if first.eq_ignore_ascii_case(&category) {
                                        view! {
                                            <A href=format!(
                                                "/category/lvl{idx}/{}",
                                                category.to_lowercase(),
                                            )>{category}</A>
                                        }
                                    } else {
                                        view! {
                                            <A href=format!(
                                                "/category/lvl{idx}/{} > {}",
                                                first,
                                                category.to_lowercase(),
                                            )>{category}</A>
                                        }
                                    }}

                                </li>
                            }
                        })
                        .collect_view()
                }}

                <li class="flex items-center space-x-2 text-sm">
                    <BreadIcon/>
                    <p class="font-bold">{name}</p>
                </li>
            </ul>
        </div>
    }
}

fn extract_categories(mut category: CategoryRecursive) -> Vec<String> {
    let mut categories: Vec<String> = Vec::new();
    categories.push(category.name);
    while let Some(entry) = category.parent {
        categories.push(entry.name.clone());
        category = *entry;
    }
    categories.reverse();
    categories
}

#[component]
pub fn BreadIcon() -> impl IntoView {
    view! {
        <svg
            class="w-3 h-3 text-gray-800 dark:text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            viewBox="0 0 24 24"
        >
            <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="m9 5 7 7-7 7"
            ></path>
        </svg>
    }
}
