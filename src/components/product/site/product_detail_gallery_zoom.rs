use leptos::*;
use url::Url;

use crate::cdn::{CdnOptions, CdnPullType, ContentProvider};

#[must_use]
#[component]
pub fn ProductDetailGalleryZoom(url: String, zoom_signal: RwSignal<bool>) -> impl IntoView {
    let cdn_context = expect_context::<ContentProvider>();
    let primary_url = cdn_context.provide_content_network(
        Url::parse(&url),
        &CdnOptions {
            quality: Some(100),
            size: None,
            crop_width: None,
            crop_height: None,
            crop_x: None,
            crop_y: None,
        },
        CdnPullType::Image,
    );

    view! {
        <div
            on:click=move |_| zoom_signal.update(|upd| *upd = !*upd)
            class="fixed inset-0 h-9/12 z-50 flex items-center justify-center bg-black bg-opacity-40"
        >
            <img class="w-full md:w-auto h-auto md:h-4/6" src=primary_url.to_string()/>
        </div>
    }
}
