use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    components::{
        product::price_availability::PriceAvailability, standards::indicators::AvailableIndicator,
    },
};

use leptos::*;
use leptos_router::A;

use model::MeilisearchProduct;
use reqwest::Url;

#[must_use]
#[component]
pub fn SearchCollectionCard(product: MeilisearchProduct) -> impl IntoView {
    let cdn = expect_context::<ContentProvider>();
    view! {
        <div class="m-3 w-80 h-[21.5rem] transform bg-white rounded-lg dark:bg-[var(--secondary-color)] shadow-md duration-300 hover:scale-102 hover:shadow-lg">
            <A href=format!(
                "/product/{}",
                product.id,
            )>
                {if let Some(preview_media) = product.preview_media {
                    let opt_url = cdn
                        .provide_content_network(
                            Url::parse(&preview_media.url),
                            &CdnOptions {
                                quality: Some(90),
                                size: Some(320),
                                crop_width: None,
                                crop_height: None,
                                crop_x: Some(0),
                                crop_y: Some(0),
                            },
                            CdnPullType::Image,
                        );
                    view! {
                        <div class="bg-white flex justify-center rounded-t-lg h-48 w-full">
                            <img
                                src=opt_url.to_string()
                                class="object-contain rounded-t-lg"
                                alt=format!("product preview image of the article {}", product.name)
                            />
                        </div>
                    }
                        .into_view()
                } else {
                    view! { <div class="w-full h-48 bg-gray-200"></div> }.into_view()
                }}
                <div class="p-4 flex flex-col justify-between h-[9.5rem]">
                    <p class="mb-2 text-lg font-medium dark:text-white text-gray-900 line-clamp-2">
                        {product.name}
                    </p>
                    <div>
                        <p class="text-sm dark:text-white text-gray-900">
                            {format!("Artikel-Nr.: {}", product.id)}
                        </p>
                        <p class="mb-2 text-base dark:text-gray-300 text-gray-700"></p>
                        <div class="flex justify-between">
                            <PriceAvailability
                                some_class="mr-2 text-xl font-bold text-gray-900 dark:text-white"
                                none_class="text-base"
                                price=product.price
                            >

                                <AvailableIndicator>"Verfügbar"</AvailableIndicator>
                            </PriceAvailability>
                        </div>
                    </div>
                </div>
            </A>
        </div>
    }
}
