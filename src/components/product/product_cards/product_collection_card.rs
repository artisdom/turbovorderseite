use std::collections::BTreeMap;

use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    components::{
        product::price_availability::PriceAvailability, standards::indicators::AvailableIndicator,
    },
};

use leptos::*;
use leptos_router::A;
use model::{MeilisearchProduct, Product};
use reqwest::Url;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn MeilisearchCollectionCard(product: MeilisearchProduct) -> impl IntoView {
    let product = Product {
        id: product.id,
        category: None,
        name: product.name,
        description: product.description,
        preview_media: product.preview_media,
        medias: product.medias,
        attributes: Vec::new(),
        associations: Vec::new(),
        metadata: BTreeMap::new(),
        price: product.price,
        is_orderable: product.is_orderable,
        created: product.created,
        updated: product.updated,
    };

    view! {
        <ProductCollectionCard product=product/>
    }
}

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn ProductCollectionCard(product: Product) -> impl IntoView {
    let cdn = expect_context::<ContentProvider>();
    let preview_image = {
        let product = product.clone();
        move || {
            cdn.provide_content_network(
                Url::parse(product.preview_media.unwrap().url.as_str()),
                &CdnOptions {
                    quality: Some(90),
                    size: Some(260),
                    crop_width: Some(260),
                    crop_height: Some(173),
                    crop_x: Some(0),
                    crop_y: Some(0),
                },
                CdnPullType::Image,
            )
        }
    };

    let product_id = product.id.clone();
    let product_link = format!("/product/{}", product.clone().id);
    let product_name = product.name.clone();
    let product_alt = format!("product preview of the article {product_name}");

    view! {
        <div class="flex items-center justify-center">
            <div class="flex flex-col justify-between h-[19.3rem] bg-white dark:bg-[var(--secondary-color)] w-[16rem] rounded-xl shadow-md lg:transform lg:duration-300 lg:hover:scale-105">
                <A href=product_link.clone()>
                    <div class="bg-white flex justify-center rounded-t-xl h-[10.625rem] w-[16rem]">
                        <img
                            class="object-contain rounded-t-xl"
                            loading="lazy"
                            src=preview_image().to_string()
                            alt=product_alt
                        />
                    </div>
                </A>
                <div class="dark:text-white flex flex-col justify-between h-full pt-0 p-4">
                    <div class="mb-2 flex flex-col h-full">
                        <A href=product_link>
                            <p class="mt-4 line-clamp-2">{product_name}</p>
                        </A>
                        <p class="text-sm text-gray-800 dark:text-gray-200">{format!("Art. Nr.: {product_id}")}</p>
                    </div>
                    <div class="flex justify-between">
                        <PriceAvailability
                            some_class="text-2xl font-bold"
                            none_class="text-base"
                            price=product.price
                        >
                            <AvailableIndicator>"Verfügbar"</AvailableIndicator>
                        </PriceAvailability>
                    </div>
                </div>
            </div>
        </div>
    }
}
