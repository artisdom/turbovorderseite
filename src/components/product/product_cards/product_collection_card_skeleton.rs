use leptos::*;

#[must_use]
#[component]
pub fn ProductCollectionCardSkeleton() -> impl IntoView {
    view! {
        <div class="flex items-center justify-center">
            <div class="flex flex-col justify-between border border-gray-100 w-[10rem] md:w-[16rem] h-[18.75rem] rounded-xl shadow-lg transform duration-300">
                <svg
                    class="animate-pulse w-full mt-5 h-[11.125rem] text-gray-200 dark:text-gray-600"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="currentColor"
                    viewBox="0 0 20 18"
                >
                    <path d="M18 0H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2Zm-5.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3Zm4.376 10.481A1 1 0 0 1 16 15H4a1 1 0 0 1-.895-1.447l3.5-7A1 1 0 0 1 7.468 6a.965.965 0 0 1 .9.5l2.775 4.757 1.546-1.887a1 1 0 0 1 1.618.1l2.541 4a1 1 0 0 1 .028 1.011Z"></path>
                </svg>
                <div>
                    <div class="animate-pulse h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 md:w-48 w-20 ml-4 mt-4"></div>
                    <div class="animate-pulse h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-20 ml-4 mt-2 mb-4"></div>
                    <div class="hidden md:flex justify-between px-4 pb-6 mt-6">
                        <div class="animate-pulse h-4 bg-gray-200 rounded-full dark:bg-gray-700 w-20"></div>
                    </div>
                </div>
            </div>
        </div>
    }
}
