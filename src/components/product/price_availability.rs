use leptos::*;

use crate::economy::ShopCurrency;

#[component]
pub fn PriceAvailability(
    some_class: &'static str,
    none_class: &'static str,
    price: Option<i32>,
    children: ChildrenFn,
) -> impl IntoView {
    view! {
        {match price {
            Some(price) => {
                view! {
                    <p class=some_class>{ShopCurrency::from(price).to_string()}</p>
                    {children}
                }
                    .into_view()
            }
            _ => {
                view! {
                    <p class=format!(
                        "text-red-600 font-bold {none_class}",
                    )>"Derzeit nicht lieferbar"</p>
                }
                    .into_view()
            }
        }}
    }
}
