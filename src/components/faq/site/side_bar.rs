use leptos::*;
use leptos_use::use_media_query;

use crate::{
    components::faq::site::side_bar_element::SideBarElement, contexts::faq_context::FaqBoard,
};

#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn FaqSideBar(children: ChildrenFn) -> impl IntoView {
    let collapse_menu = create_rw_signal(true);
    let medium_screen_signal = use_media_query("(min-width: 768px)");

    create_local_resource(
        move || medium_screen_signal.get(),
        move |is_medium_screen| async move {
            collapse_menu.set(is_medium_screen);
        },
    );

    view! {
        <button
            type="button"
            on:click=move |_| collapse_menu.update(|upd| *upd = !*upd)
            class=move || {
                format!(
                    "inline-flex items-center ml-4 mt-2 text-sm rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600 {}",
                    if collapse_menu.get() {
                        "text-[var(--primary-color)]"
                    } else {
                        "text-gray-700"
                    },
                )
            }
        >
            <svg
                class="w-6 h-6 mr-2"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    clip-rule="evenodd"
                    fill-rule="evenodd"
                    d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z"
                ></path>
            </svg>
            "Menü"
        </button>

        <div class="flex flex-col md:flex-row md:justify-center min-content p-4 pt-0 md:pt-4 rounded-lg">
            <Show when=move || collapse_menu.get()>
                <div class="h-full md:px-3 py-4 overflow-y-auto">
                    <div class="border-b border-black dark:border-white mb-2 pb-3">
                        <p class="text-2xl font-bold dark:text-white">"FAQ - Bereich"</p>
                    </div>
                    <ul class="space-y-1.5 font-medium">
                        <SideBarElement board=FaqBoard::Shipping>
                            <svg
                                class="flex-shrink-0 w-5 h-5 transition duration-75"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                            >
                                <path d="m17.418 3.623-.018-.008a6.713 6.713 0 0 0-2.4-.569V2h1a1 1 0 1 0 0-2h-2a1 1 0 0 0-1 1v2H9.89A6.977 6.977 0 0 1 12 8v5h-2V8A5 5 0 1 0 0 8v6a1 1 0 0 0 1 1h8v4a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1v-4h6a1 1 0 0 0 1-1V8a5 5 0 0 0-2.582-4.377ZM6 12H4a1 1 0 0 1 0-2h2a1 1 0 0 1 0 2Z"></path>
                            </svg>
                            <span class="flex-1 ms-3 whitespace-nowrap">"Lieferung"</span>
                        </SideBarElement>
                        <SideBarElement board=FaqBoard::Retoure>
                            <svg
                                class="flex-shrink-0 w-5 h-5 transition duration-75"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                viewBox="0 0 18 20"
                            >
                                <path d="M17 5.923A1 1 0 0 0 16 5h-3V4a4 4 0 1 0-8 0v1H2a1 1 0 0 0-1 .923L.086 17.846A2 2 0 0 0 2.08 20h13.84a2 2 0 0 0 1.994-2.153L17 5.923ZM7 9a1 1 0 0 1-2 0V7h2v2Zm0-5a2 2 0 1 1 4 0v1H7V4Zm6 5a1 1 0 1 1-2 0V7h2v2Z"></path>
                            </svg>
                            <span class="flex-1 ms-3 whitespace-nowrap">
                                "Reklamation & Rückgabe"
                            </span>
                        </SideBarElement>
                        <SideBarElement board=FaqBoard::Support>
                            <svg
                                class="flex-shrink-0 w-5 h-5 transition duration-75"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                viewBox="0 0 20 18"
                            >
                                <path d="M14 2a3.963 3.963 0 0 0-1.4.267 6.439 6.439 0 0 1-1.331 6.638A4 4 0 1 0 14 2Zm1 9h-1.264A6.957 6.957 0 0 1 15 15v2a2.97 2.97 0 0 1-.184 1H19a1 1 0 0 0 1-1v-1a5.006 5.006 0 0 0-5-5ZM6.5 9a4.5 4.5 0 1 0 0-9 4.5 4.5 0 0 0 0 9ZM8 10H5a5.006 5.006 0 0 0-5 5v2a1 1 0 0 0 1 1h11a1 1 0 0 0 1-1v-2a5.006 5.006 0 0 0-5-5Z"></path>
                            </svg>
                            <span class="ms-3">"Support"</span>
                        </SideBarElement>
                    </ul>
                </div>
            </Show>

            <div class="sm:ml-6 md:w-[48rem] md:max-w-[48rem] min-h-96">{children.clone()}</div>
        </div>
    }
}
