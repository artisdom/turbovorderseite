use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::configuration_context::ConfigurationContext,
};

use leptos::*;
use url::Url;

#[must_use]
#[component]
pub fn FaqSupportBoard() -> impl IntoView {
    let configuration_signal = expect_context::<ConfigurationContext>();
    let cdn = expect_context::<ContentProvider>();

    let opt_url = cdn.provide_content_network(
        Url::parse(&format!(
            "{}/static/faq_images/faq_support.jpg",
            configuration_signal.get_app_environment().app_deployment
        )),
        &CdnOptions {
            quality: None,
            size: Some(770),
            crop_width: Some(770),
            crop_height: Some(192),
            crop_x: Some(0),
            crop_y: Some(85),
        },
        CdnPullType::Image,
    );

    view! {
        <div class="max-w-screen-xl mt-4 mb-8 dark:text-gray-300">
            <h2 class="mb-6 lg:mb-8 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">
                "Support"
            </h2>
            <img
                class="rounded-lg w-full object-cover md:h-48 mb-8"
                src=opt_url.to_string()
                alt="motorcycle driver calling with intercom"
            />
            <p>
                "Wenn du Fragen zu einem Produkt hast oder technische Hilfe benötigst, steht dir unser Kundenservice jederzeit gerne zur Verfügung. Du kannst uns telefonisch oder per E-Mail erreichen - bitte beachte jedoch die unten angegebenen Supportzeiten."
            </p>
            <div class="mt-2">
                <p>"Email: " <span class="font-bold">"support@moturbo.com"</span></p>
                <p>"Telefon: " <span class="font-bold">"+49 6409 6623197"</span></p>
            </div>
            <p class="mb-2 mt-4 text-xl lg:text-2xl tracking-tight font-bold">
                "Telefon Supportzeiten"
            </p>
            <ul>
                <li>
                    <p>"Montag "</p>
                    <b>"17:30 - 21:30 Uhr"</b>
                </li>
                <li>
                    <p>"Dienstag "</p>
                    <b>"Telefonsupport geschlossen"</b>
                </li>
                <li>
                    <p>"Mittwoch "</p>
                    <b>"Telefonsupport geschlossen"</b>
                </li>
                <li>
                    <p>"Donnerstag "</p>
                    <b>"17:30 - 21:30 Uhr"</b>
                </li>
                <li>
                    <p>"Freitag "</p>
                    <b>"17:30 - 21:30 Uhr"</b>
                </li>
                <li>
                    <p>"Samstag "</p>
                    <b>"12:00 - 20:00 Uhr"</b>
                </li>
                <li>
                    <p>"Sonntag "</p>
                    <b>"Telefonsupport geschlossen"</b>
                </li>
            </ul>
        </div>
    }
}
