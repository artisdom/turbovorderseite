use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::configuration_context::ConfigurationContext,
};

use leptos::*;
use url::Url;

#[must_use]
#[component]
pub fn FaqShippingBoard() -> impl IntoView {
    let configuration_signal = expect_context::<ConfigurationContext>();
    let cdn = expect_context::<ContentProvider>();

    let opt_url = cdn.provide_content_network(
        Url::parse(&format!(
            "{}/static/faq_images/faq_shipping.jpg",
            configuration_signal.get_app_environment().app_deployment
        )),
        &CdnOptions {
            quality: None,
            size: Some(770),
            crop_width: Some(770),
            crop_height: Some(192),
            crop_x: Some(0),
            crop_y: Some(200),
        },
        CdnPullType::Image,
    );

    view! {
        <div class="max-w-screen-xl mt-4 mb-8 dark:text-gray-300">
            <h2 class="mb-6 lg:mb-8 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">
                "Lieferung"
            </h2>
            <img
                class="rounded-lg w-full object-cover md:h-48 mb-8"
                src=opt_url.to_string()
                alt="motorcycle with driver from side"
            />
            <p>
                "In unserem Shop bieten wir kostenlosen Versand für Bestellungen ab 90€ an. Das bedeutet, dass du bei einem Einkaufswert von 90€ oder mehr keine zusätzlichen Versandkosten bezahlen musst. Diese Regelung gilt für alle Bestellungen innerhalb unseres Liefergebiets. Bitte beachte, dass der kostenlose Versand automatisch angewendet wird, sobald der Mindestbestellwert von 90€ erreicht ist."
            </p>
            <p class="mb-2 mt-4 text-xl lg:text-2xl tracking-tight font-bold">
                "Prozess und Bearbeitungszeit"
            </p>
            <p>"Die Bearbeitung kann je nach Wochentag und Uhrzeit differenzieren."</p>
            <p class="mb-2">
                "Bitte beachte das wir die meisten Artikel zwischenlagern, unter der Woche ist die derzeitige Regelzeit bis zur vollständigen Bearbeitung "
                <b>"ca. 48 bis 72 Stunden"</b> "."
            </p>
            <p>
                "Wir sind stetig daran unseren Lagerbestand zu erweitern und durch bereits bestehenden Artikel in unserem Lager, die Bearbeitungszeit der Bestellungen zu verbessern."
            </p>
            <p class="mb-2 mt-4 text-xl lg:text-2xl tracking-tight font-bold">"Versand"</p>
            <p>
                "Sobald deine Bestellung erfolgreich in unserem Zwischenlager bearbeitet wurde, geht sie in den "
                <b>"DHL Versand"</b> "."
            </p>
            <p>
                "Beachte dabei das wir keine Teillieferung anbieten und Bestellungen immer komplett versendet werden."
            </p>
            <p>
                "Es kann je nach Aulastung bei DHL 1-2 Tage dauern bis eine Sendungsnummer zur Verfügung stehen."
            </p>
        </div>
    }
}
