use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::configuration_context::ConfigurationContext,
};

use leptos::*;
use url::Url;

#[must_use]
#[component]
pub fn FaqRetoureBoard() -> impl IntoView {
    let configuration_signal = expect_context::<ConfigurationContext>();
    let cdn = expect_context::<ContentProvider>();

    let app_environment = configuration_signal.get_app_environment();

    let opt_url = cdn.provide_content_network(
        Url::parse(&format!(
            "{}/static/faq_images/faq_retoure.jpg",
            configuration_signal.get_app_environment().app_deployment
        )),
        &CdnOptions {
            quality: None,
            size: Some(770),
            crop_width: Some(770),
            crop_height: Some(192),
            crop_x: Some(0),
            crop_y: Some(200),
        },
        CdnPullType::Image,
    );

    view! {
        <div class="max-w-screen-xl mt-4 mb-8 dark:text-gray-300">
            <h2 class="mb-6 lg:mb-8 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">
                "Reklamation und Rückgabe"
            </h2>
            <img
                class="rounded-lg w-full object-cover md:h-48 mb-8"
                src=opt_url.to_string()
                alt="motorcycle with driver from side"
            />
            <p class="mb-2">
                "Wenn es Grund zur Rücksendung gibt, bitten wir darum, uns telefonisch oder per E-Mail zu kontaktieren. Unser Team steht dir zur Verfügung."
            </p>
            <p class="mb-2">"Email: " <b>"support@moturbo.com"</b></p>
            <p>
                "Bitte beachte, dass Reklamationen bezüglich eines Pakets und dessen Inhalt innerhalb einer angemessenen Frist nach Erhalt der Lieferung uns mitgeteilt werden müssen."
            </p>
            <p class="mb-2">
                "Solltest du Ware zurückschicken müssen, die nicht ordnungsgemäß per E-Mail gemeldet wurde, wird sie an den Absender zurückgeschickt!"
            </p>
            <p>
                "Bitte stelle sicher, dass alle Produkte in ihrer Originalverpackung und ohne erkennbare Schäden zurückgeschickt werden. Sowohl das Produkt als auch die Verpackung sollten sich in einwandfreiem Zustand befinden."
            </p>
            <div class="my-3">
                <p>
                    "Bitte vergiss nicht, folgende Dokumente mit in das Rücksende-Paket zu legen:"
                </p>
                <ul class="list-disc list-inside">
                    <li>"Kopie der Rechnung"</li>
                    <li>"Grund für die Rücksendung"</li>
                </ul>
            </div>
            <div class="mb-3">
                <p>"ALLE RÜCKSENDUNGEN SIND AN DIE FOLGENDE ADRESSE ZU SENDEN:"</p>
                <p>{app_environment.company_name}</p>
                <p>{app_environment.company_street}</p>
                <p>{format!("{} {}", app_environment.company_postalcode, app_environment.company_city)}</p>
                <p>{app_environment.company_country}</p>
            </div>
            <p class="mb-3">
                "Weitere Anweisungen zur Rückgabe erhältst du per E-Mail, sobald du dich mit uns in Verbindung gesetzt hast. Unsere Produkte unterliegen den Herstellergarantien, die für jedes einzelne Produkt spezifiziert sind."
            </p>
            <p class="mb-3">
                "Nichtbeachtung der Rückgabebedingungen kann zusätzliche Gebühren nach sich ziehen."
            </p>
            <p>"Wir danken dir für dein Vertrauen!"</p>
            <p>"Dein " <b>"Moturbo Team"</b></p>
        </div>
    }
}
