use crate::contexts::faq_context::{FaqBoard, FaqContext};

use leptos::*;
use leptos_router::A;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn SideBarElement(children: ChildrenFn, board: FaqBoard) -> impl IntoView {
    const FOCUSED: &str = "bg-[var(--primary-color)] opacity-80 text-gray-50 dark:text-gray-200";

    let faq_context = expect_context::<FaqContext>();
    let path = match board {
        FaqBoard::Shipping => "/shipping",
        FaqBoard::Retoure => "/retoure",
        FaqBoard::Support => "/support",
    };

    view! {
        <li class=move || {
            format!(
                "hover:bg-[var(--primary-color)] hover:opacity-70 hover:text-gray-100 dark:text-white rounded-lg transition duration-100 ease-in-out {}",
                if faq_context.get_current_board().eq(&board.clone()) { FOCUSED } else { "" },
            )
        }>

            <A href=path class="flex items-center p-2 rounded-lg group">
                {children}
            </A>
        </li>
    }
}
