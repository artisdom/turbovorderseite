use leptos::*;
use leptos_router::A;

#[must_use]
#[component]
pub fn RegisterTermsCheck() -> impl IntoView {
    view! {
        <div class="flex items-start">
            <div class="flex items-center h-5">
                <input
                    id="terms"
                    aria-describedby="terms"
                    type="checkbox"
                    class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
                    required=""
                />
            </div>
            <div class="ml-3 text-sm">
                <label for="terms" class="font-light text-gray-500 dark:text-gray-300">
                    "Ich akzeptiere die "
                    <A
                        class="font-medium text-primary-600 hover:underline dark:text-primary-500"
                        href="/terms"
                    >
                        "Allgemeine Geschäftsbedingungen"
                    </A>
                    " und die "
                    <A
                        href="/privacy"
                        class="font-medium text-primary-600 hover:underline dark:text-primary-500"
                    >
                        "Datenschutzerklärung"
                    </A>
                </label>
            </div>
        </div>
    }
}
