use leptos::*;

#[must_use]
#[component]
pub fn RegisterInputFields() -> impl IntoView {
    view! {
        <div>
            <label
                for="firstname"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
                "Vorname"
            </label>
            <input
                type="text"
                name="firstname"
                autocomplete="off"
                id="firstname"
                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Dein Vorname"
                required=""
            />
        </div>
        <div>
            <label
                for="lastname"
                autocomplete="off"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
                "Nachname"
            </label>
            <input
                type="text"
                name="lastname"
                id="lastname"
                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Dein Nachname"
                required=""
            />
        </div>
        <div>
            <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                "Deine E-Mail Adresse"
            </label>
            <input
                type="email"
                name="email"
                autocomplete="off"
                id="email"
                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="name@company.com"
                required=""
            />
        </div>
        <div>
            <label
                for="password"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
                "Passwort"
            </label>
            <input
                type="password"
                name="password"
                id="password"
                autocomplete="new-password"
                placeholder="••••••••"
                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required=""
            />
        </div>
        <div>
            <label
                for="confirm-password"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
                "Passwort wiederholen"
            </label>
            <input
                type="confirm-password"
                name="confirm-password"
                id="confirm-password"
                autocomplete="new-password"
                placeholder="••••••••"
                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required=""
            />
        </div>
    }
}
