use leptos::*;
use leptos_router::A;
use model::OrderItem;

use crate::economy::ShopCurrency;

const MAX_PRODUCT_NAME_LENGTH: usize = 55;

#[must_use]
#[component]
pub fn OrderedListItem(item: OrderItem) -> impl IntoView {
    let mut product_name = item.product_name;
    let is_cutted = product_name.len() > MAX_PRODUCT_NAME_LENGTH;
    product_name.truncate(MAX_PRODUCT_NAME_LENGTH);
    if is_cutted {
        product_name.push_str("...");
    }
    view! {
        <tr class="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-[var(--secondary-color)] border-b dark:border-gray-700">
            <th
                scope="row"
                class="px-1 py-2 font-medium text-gray-900 whitespace-nowrap dark:text-white"
            >
                <A href=format!("/product/{}", item.product_id)>{product_name}</A>
            </th>
            <td class="px-1 py-2">{item.quantity}</td>
            <td class="px-1 py-2">{item.product_id}</td>
            <td class="px-1 py-2">{ShopCurrency::from(item.price).to_string()}</td>
            <td class="px-1 py-2">{ShopCurrency::from(item.amount_total).to_string()}</td>
        </tr>
    }
}
