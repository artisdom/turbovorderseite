use leptos::*;
use model::OrderItem;

use crate::components::order::ordered_list_item::OrderedListItem;

#[must_use]
#[component]
pub fn OrderedItemsList(items: Vec<OrderItem>) -> impl IntoView {
    view! {
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-1 py-2">
                            "Produktname"
                        </th>
                        <th scope="col" class="px-1 py-2">
                            "Menge"
                        </th>
                        <th scope="col" class="px-1 py-2">
                            "Artikel-Nr."
                        </th>
                        <th scope="col" class="px-1 py-2">
                            "Einzelpreis"
                        </th>
                        <th scope="col" class="px-1 py-2">
                            "Gesamtpreis"
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <For
                        each=move || items.clone()
                        key=|item| item.product_id.clone()
                        children=|item: OrderItem| view! { <OrderedListItem item=item/> }
                    />

                </tbody>
            </table>
        </div>
    }
}
