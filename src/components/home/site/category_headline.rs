use leptos::*;

#[component]
pub fn CategoryHeadline(children: ChildrenFn) -> impl IntoView {
    view! {
        <section class="ml-4 md:ml-0 mb-4">
            <h2 class="text-3xl font-bold text-[var(--primary-color)] dark:text-white">{children}</h2>
        </section>
    }
}
