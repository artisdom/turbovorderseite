use crate::{
    components::{
        collections::skeleton_collection::SkeletonCollection,
        product::product_cards::product_collection_card::ProductCollectionCard,
    },
    contexts::configuration_context::ConfigurationContext,
    error::template::ErrorTemplate,
    fetch::api_get_call,
};

use leptos::*;
use model::Product;
use pagination::models::Pagination;

#[must_use]
#[component]
pub fn BestsellerCollection() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();

    let bestseller_resource = create_resource(
        || (),
        move |()| async move {
            api_get_call::<Pagination<Product>>(
                &format!(
                    "{}/products/bestseller?size=10&order_dir=desc",
                    configuration_context.get_app_environment().backend_endoint
                ),
                None,
            )
            .await
        },
    );

    let product_cards_view = move || {
        bestseller_resource.map(|products| match products {
            Ok(products) => view! { <ProductCards products=products.data.clone()/> }.into_view(),
            Err(_) => view! { <SkeletonCollection count=5/> }.into_view(),
        })
    };

    view! {
        <ul class="flex flex-wrap justify-center max-w-[84rem]">
            <Transition fallback=move || view! { <SkeletonCollection count=5/> }>
                <ErrorBoundary fallback=|errors| {
                    view! { <ErrorTemplate errors=errors/> }
                }>{move || product_cards_view}</ErrorBoundary>
            </Transition>
        </ul>
    }
}

#[component]
fn ProductCards(products: Vec<Product>) -> impl IntoView {
    products
        .into_iter()
        .map(|product| {
            view! {
                <li class="mx-1.5 my-1.5">
                    <ProductCollectionCard product=product/>
                </li>
            }
            .into_view()
        })
        .collect_view()
}
