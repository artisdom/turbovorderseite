use leptos::*;
use leptos_use::{use_interval_fn, utils::Pausable};

#[must_use]
#[component]
pub fn WeAre() -> impl IntoView {
    let (sentence, set_sentence) =
        create_signal([String::new(), String::new(), String::new(), String::new()].to_vec());

    let index = create_rw_signal(0);
    let current_sentence_index = create_rw_signal(0);

    let part_0 = create_rw_signal(String::new());
    let part_1 = create_rw_signal(String::new());
    let part_2 = create_rw_signal(String::new());
    let part_3 = create_rw_signal(String::new());

    create_effect(move |_| {
        let sentence_parts = [
            "BETA! WELCOME".to_owned(),
            "TO".to_owned(),
            " MOTURBO".to_owned(),
            " 2-WHEEL SHOP!".to_owned(),
        ];

        let Pausable {
            pause: _,
            resume: _,
            is_active: _,
        } = use_interval_fn(
            move || {
                if let Some(current_sentence) = sentence_parts.get(current_sentence_index.get()) {
                    set_sentence.update(|upd| {
                        if let Some(appendix) = current_sentence.chars().nth(index.get()) {
                            let next_index = index.get() + 1;
                            if appendix == ' ' && next_index < current_sentence.len() {
                                if let Some(next_char) = current_sentence.chars().nth(next_index) {
                                    upd[current_sentence_index.get()] = format!(
                                        "{} {}",
                                        upd.get(current_sentence_index.get()).unwrap(),
                                        next_char
                                    );
                                    index.update(|i| *i += 1);
                                }
                            } else {
                                upd[current_sentence_index.get()] = format!(
                                    "{}{}",
                                    upd.get(current_sentence_index.get()).unwrap(),
                                    appendix
                                );
                            }
                        }
                    });

                    if current_sentence.len() <= index.get() {
                        index.set(0);
                        if current_sentence_index.get() < sentence_parts.len() {
                            current_sentence_index.update(|i| *i += 1);
                        }
                    } else {
                        index.update(|i| *i += 1);
                    }
                }
            },
            75,
        );
    });

    create_local_resource(sentence, move |local_sentence| async move {
        if let Some(inner) = local_sentence.first() {
            part_0.set(inner.to_owned());
        }
        if let Some(inner) = local_sentence.get(1) {
            part_1.set(inner.to_owned());
        }
        if let Some(inner) = local_sentence.get(2) {
            part_2.set(inner.to_owned());
        }
        if let Some(inner) = local_sentence.get(3) {
            part_3.set(inner.to_owned());
        }
    });

    view! {
        <div class="font-quicksand text-6xl font-bold text-white">
            <p class="opacity-60">{move || part_0.get()}</p>
            <p>
                {move || part_1.get()}
                <span class="text-[var(--primary-color)]">{move || part_2.get()}</span>
                {move || part_3.get()}
            </p>
        </div>
    }
}
