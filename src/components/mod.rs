#![allow(clippy::wildcard_imports)]
#![allow(clippy::module_name_repetitions)]

pub mod admin;
pub mod collections;
pub mod faq;
pub mod home;
pub mod navigation;
pub mod order;
pub mod orderflow;
pub mod product;
pub mod products;
pub mod profile;
pub mod register;
pub mod standards;
