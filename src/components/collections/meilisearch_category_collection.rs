use crate::components::{
    collections::skeleton_collection::SkeletonCollection,
    product::product_cards::product_collection_card::MeilisearchCollectionCard,
};

use leptos::*;
use leptos_meilisearch::{SearchQuery, SingleSearch};
use model::MeilisearchProduct;

#[must_use]
#[component]
pub fn MeilisearchCategoryCollection(
    category_name: String,
    #[prop(default = 5)] count: usize,
    #[prop(default = 1)] page: usize,
) -> impl IntoView {
    let meilisearch_context = expect_context::<SingleSearch<MeilisearchProduct>>();

    let query_signal = create_rw_signal(SearchQuery::default());
    let oneshot_search = meilisearch_context.create_resource(query_signal);

    let level = if category_name.contains(">") { 1 } else { 0 };

    create_effect(move |_| {
        query_signal.update(|upd| {
            upd.filter = Some(format!("categories.lvl{level}=\"{category_name}\""));
            upd.hits_per_page = Some(count);
            upd.limit = Some(count);
            upd.page = Some(page);
        });
    });

    view! {
        <ul class="flex flex-wrap justify-center max-w-[84rem]">
            {move || {
                match oneshot_search.get() {
                    Some(search_result) => {
                        search_result
                            .map(|results| {
                                let products = results
                                    .hits
                                    .iter()
                                    .map(|product_wrap| product_wrap.result.clone())
                                    .collect::<Vec<_>>();
                                view! { <ProductCards products=products/> }
                            })
                            .collect_view()
                    }
                    None => view! { <SkeletonCollection count=count/> },
                }
            }}

        </ul>
    }
}

#[component]
fn ProductCards(products: Vec<MeilisearchProduct>) -> impl IntoView {
    products
        .into_iter()
        .map(|product| {
            view! {
                <li class="mx-1.5 my-1.5">
                    <MeilisearchCollectionCard product=product/>
                </li>
            }
            .into_view()
        })
        .collect_view()
}
