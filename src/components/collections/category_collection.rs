use leptos::*;
use model::Product;
use pagination::models::Pagination;

use crate::components::collections::skeleton_collection::SkeletonCollection;
use crate::components::product::product_cards::product_collection_card::ProductCollectionCard;

use crate::contexts::configuration_context::ConfigurationContext;
use crate::error::template::ErrorTemplate;
use crate::fetch::api_get_call;

#[must_use]
#[component]
pub fn CategoryCollection(category_id: String, page: usize) -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();

    let products_resource = create_local_resource(
        || (),
        move |()| {
            let category_id = category_id.clone();
            async move {
                api_get_call::<Pagination<Product>>(
                    format!(
                        "{}/products?category_id={category_id}&size=5&page={page}",
                        configuration_context.get_app_environment().backend_endoint,
                    )
                    .as_str(),
                    None,
                )
                .await
            }
        },
    );

    let product_cards_view = move || {
        products_resource.map(|products| match products.as_ref().ok() {
            Some(products) => view! { <ProductCards products=products.data.clone()/> }.into_view(),
            None => view! { <SkeletonCollection count=5/> }.into_view(),
        })
    };

    view! {
        <ul class="flex flex-wrap justify-center max-w-[84rem]">
            <Transition fallback=move || view! { <SkeletonCollection count=5/> }>
                <ErrorBoundary fallback=|errors| {
                    view! { <ErrorTemplate errors=errors/> }
                }>{move || product_cards_view}</ErrorBoundary>
            </Transition>
        </ul>
    }
}

#[component]
fn ProductCards(products: Vec<Product>) -> impl IntoView {
    products
        .into_iter()
        .map(|product| {
            view! {
                <li class="mx-1.5 my-1.5">
                    <ProductCollectionCard product=product/>
                </li>
            }
            .into_view()
        })
        .collect_view()
}
