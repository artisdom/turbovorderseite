use crate::components::product::product_cards::product_collection_card::ProductCollectionCard;

use crate::contexts::configuration_context::ConfigurationContext;
use crate::error::template::ErrorTemplate;
use crate::error::AppResult;
use crate::fetch::api_get_call;

use leptos::*;

use futures::future::join_all;
use model::Product;

use super::skeleton_collection::SkeletonCollection;

#[must_use]
#[component]
pub fn SpecifiedCollection(product_id_set: Vec<String>) -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();

    let id_set_count = product_id_set.len();
    let products_resource = create_local_resource(
        || (),
        move |()| {
            let products = product_id_set.clone();
            async move {
                let product_result_tasks: Vec<_> = products
                    .iter()
                    .map(|product_id| async {
                        api_get_call::<Product>(
                            format!(
                                "{}/products/{}",
                                configuration_context.get_app_environment().backend_endoint,
                                product_id.to_owned()
                            )
                            .as_str(),
                            None,
                        )
                        .await
                    })
                    .collect();
                join_all(product_result_tasks)
                    .await
                    .into_iter()
                    .collect::<Vec<AppResult<Product>>>()
            }
        },
    );
    let product_cards_view = move || {
        products_resource
            .map(|products| view! { <ProductCards products=products.clone()/> }.into_view())
    };

    view! {
        <ul class="flex flex-wrap justify-center max-w-[84rem]">
            <Transition fallback=move || {
                view! { <SkeletonCollection count=id_set_count/> }
            }>
                <ErrorBoundary fallback=|errors| {
                    view! { <ErrorTemplate errors=errors/> }
                }>{move || product_cards_view}</ErrorBoundary>
            </Transition>
        </ul>
    }
}

#[component]
fn ProductCards(products: Vec<AppResult<Product>>) -> impl IntoView {
    products
        .into_iter()
        .map(|product_result| {
            match product_result {
                Ok(product) => view! {
                    <li class="mx-0.5 md:mx-1.5 my-1.5">
                        <ProductCollectionCard product=product/>
                    </li>
                }
                .into_view(),
                Err(_) => view! { <SkeletonCollection count=1/> }.into_view(),
            }
            .into_view()
        })
        .collect_view()
}
