use crate::components::product::product_cards::product_collection_card_skeleton::ProductCollectionCardSkeleton;

use leptos::*;

#[must_use]
#[component]
pub fn SkeletonCollection(count: usize) -> impl IntoView {
    (0..count)
        .map(|_| {
            view! {
                <li class="mx-0.5 md:mx-1.5 my-1.5">
                    <ProductCollectionCardSkeleton/>
                </li>
            }
        })
        .collect_view()
}
