use crate::components::home::site::category_headline::CategoryHeadline;
use crate::{
    components::collections::specified_collection::SpecifiedCollection,
    contexts::last_seen_context::LastSeenContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn LastSeenCollection() -> impl IntoView {
    let last_seen_context = expect_context::<LastSeenContext>();

    let last_seen_resource = create_local_resource(
        move || last_seen_context.get_last_sku_set(),
        move |last_seen_signal| async move { last_seen_signal },
    );

    view! {
        <Show when=move || (last_seen_resource.get().unwrap_or_default().len() > 0)>
            <CategoryHeadline>"Zuletzt angesehene Produkte"</CategoryHeadline>
        </Show>
        <Transition>
            {move || {
                last_seen_resource
                    .get()
                    .map(|sku_set| {
                        view! { <SpecifiedCollection product_id_set=sku_set.into()/> }
                    })
                    .collect_view()
            }}

        </Transition>
    }
}
