#![allow(clippy::needless_pass_by_value)]

use leptos::*;

#[component]
pub fn WarningIndicator(children: ChildrenFn) -> impl IntoView {
    view! {
        <div class="inline-flex items-center space-x-2 text-orange-700 bg-orange-100 rounded-lg p-2">
            <svg
                class="w-7 h-7"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 20 20"
            >
                <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z"></path>
            </svg>
            <p class="text-sm">{children()}</p>
        </div>
    }
}

#[component]
pub fn ErrorIndicator(children: ChildrenFn) -> impl IntoView {
    view! {
        <div class="inline-flex items-center space-x-2 text-red-700 bg-red-100 rounded-lg p-2">
            <svg
                class="w-7 h-7"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 20 20"
            >
                <path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 11.793a1 1 0 1 1-1.414 1.414L10 11.414l-2.293 2.293a1 1 0 0 1-1.414-1.414L8.586 10 6.293 7.707a1 1 0 0 1 1.414-1.414L10 8.586l2.293-2.293a1 1 0 0 1 1.414 1.414L11.414 10l2.293 2.293Z"></path>
            </svg>
            <p class="text-sm">{children()}</p>
        </div>
    }
}
