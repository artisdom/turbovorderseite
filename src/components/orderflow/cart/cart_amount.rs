use leptos::*;

#[must_use]
#[component]
pub fn CartAmount() -> impl IntoView {
    view! {
        <div class="flex items-center">
            <input type="number" value="1" min="1" class="w-10"/>
        </div>
    }
}
