use leptos::*;

#[must_use]
#[component]
pub fn CartCoupon() -> impl IntoView {
    view! {
        <div class="p-4 border border-gray-200 rounded-lg">
            <p class="text-center text-xl">"Gutscheincode einlösen"</p>
            <div class="mt-6">
                <input placeholder="Gutscheincode eingeben"/>
            </div>
        </div>
    }
}
