use crate::components::orderflow::cart::site::cart_totals::share_cart_modal::ShareCartModal;
use crate::{
    codec::encode_to_id,
    contexts::{cart_context::CartContext, configuration_context::ConfigurationContext},
};

use leptos::html::Div;
use leptos::*;
use wasm_bindgen::UnwrapThrowExt;

#[must_use]
#[component]
pub fn ShareCartButton() -> impl IntoView {
    let link_signal = create_rw_signal(None);
    let area_el = create_node_ref::<Div>();

    let share_action = create_action(move |()| async move {
        let configuration_context = expect_context::<ConfigurationContext>();
        let cart_context = expect_context::<CartContext>();
        let encoded = encode_to_id(cart_context.get_raw_items());

        link_signal.set(Some(format!(
            "{}/shared-cart/{encoded}",
            configuration_context.get_app_environment().app_deployment
        )));
        let _ = area_el
            .get_untracked()
            .unwrap_throw()
            .style("display", "block");
    });

    view! {
        {move || if let Some(link) = link_signal.get() {
            view! {
                <ShareCartModal area_el=area_el link=link/>
            }
        } else { view! {}.into_view() }}
        <button
            on:click=move |_| share_action.dispatch(())
            class="relative w-full inline-flex items-center justify-center p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-green-400 to-blue-600 group-hover:from-green-400 group-hover:to-blue-600 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-green-200 dark:focus:ring-green-800"
        >
            <span class="w-full relative px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-[var(--secondary-color)] rounded-md group-hover:bg-opacity-0">
                "Warenkorb teilen"
            </span>
        </button>
    }
}
