use crate::{
    components::orderflow::cart::cart_indicator::WarningIndicator,
    economy::ShopCurrency,
    signals::cart_signal::{CartItem, CartSignal},
};
use leptos::*;

#[must_use]
#[component]
pub fn ShippingDetails(
    item_sum_resource: Resource<Vec<CartItem>, i32>,
    cart_lock_resource: Resource<Vec<CartItem>, bool>,
) -> impl IntoView {
    let cart = expect_context::<CartSignal>();
    view! {
        <div>
            <div class="flex justify-between">
                <p>"Versandkosten"</p>
                <p>
                    {move || match item_sum_resource.get() {
                        Some(sum) => ShopCurrency::from(cart.check_shipping_costs(sum)).to_string(),
                        None => "Versandt wird berechnet".to_owned(),
                    }}

                </p>
            </div>
            {move || {
                if let (Some(locked), Some(sum)) = (
                    cart_lock_resource.get(),
                    item_sum_resource.get(),
                ) {
                    view! {
                        <Show when=move || {
                            cart.check_shipping_costs(sum) > 0 && !locked && sum > 6000
                        }>
                            <div class="w-full my-2.5">
                                <WarningIndicator>
                                    "Noch " <b>{ShopCurrency::from(9000 - sum).to_string()}</b>
                                    " um kostenlosen Versand zu erhalten."
                                </WarningIndicator>
                            </div>
                        </Show>
                    }
                        .into_view()
                } else {
                    view! {}.into_view()
                }
            }}

        </div>
    }
}
