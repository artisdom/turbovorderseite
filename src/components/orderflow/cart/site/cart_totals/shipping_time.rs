use crate::economy::get_delivery_time;

use leptos::*;

#[must_use]
#[component]
pub fn ShippingTime() -> impl IntoView {
    let (estimated_from, estimated_to) = get_delivery_time();

    view! {
        <p class="text-sm mt-2">
            "Versendung zwischen "
            <b class="text-green-600">{estimated_from.format("%d").to_string()} "."</b> " und "
            <b class="text-green-600">{estimated_to.format("%d.%m.%Y").to_string()}</b>
        </p>
    }
}
