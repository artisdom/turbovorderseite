use crate::{
    contexts::cart_context::{CartContext, CartItem},
    economy::ShopCurrency,
};

use leptos::*;

#[must_use]
#[component]
pub fn ShippingTotalSum(item_sum_resource: Resource<Vec<CartItem>, i32>) -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    view! {
        <div class="mt-2 flex justify-between text-lg">
            <b>"Gesamtsumme"</b>
            <b>
                {move || match item_sum_resource.get() {
                    Some(sum) => {
                        ShopCurrency::from(sum + cart_context.check_shipping_costs(sum)).to_string()
                    }
                    None => "Gesamtsumme wird berechnet".to_owned(),
                }}

            </b>
        </div>
    }
}
