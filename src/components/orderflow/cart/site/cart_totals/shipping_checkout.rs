use crate::{
    components::orderflow::cart::cart_indicator::ErrorIndicator,
    contexts::{
        cart_context::{CartContext, CartItem},
        order_context::OrderContext,
    },
};

use leptos::*;
use leptos_oidc::Auth;
use leptos_router::A;

#[must_use]
#[component]
pub fn ShippingCheckout(cart_lock_resource: Resource<Vec<CartItem>, bool>) -> impl IntoView {
    let auth_context = expect_context::<Auth>();

    let checkout_action = create_action(move |()| async move {
        let cart_context = expect_context::<CartContext>();
        let order_context = expect_context::<OrderContext>();
        if let Some(checkout_url) = order_context
            .send_checkout(cart_context.get_items().await)
            .await
        {
            let _ = window().location().set_href(&checkout_url);
        }
    });

    move || match cart_lock_resource.get() {
        Some(locked) => {
            if locked {
                view! {
                    <div class="w-full my-2">
                        <ErrorIndicator>
                            "Einer oder mehrere Artikel sind derzeit nicht auf Lager."
                        </ErrorIndicator>
                    </div>
                }
                .into_view()
            } else if !auth_context.authenticated() {
                view! {
                    <A
                        href="/checkout-forward"
                        class="inline-flex justify-center items-center mt-2 text-white bg-gradient-to-r from-[var(--primary-color)] via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm w-full px-8 py-3 text-center me-2 mb-2"
                    >
                        "Zur Kasse"
                        <svg
                            class="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 10"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9"
                            ></path>
                        </svg>
                    </A>
                }
                    .into_view()
            } else {
                view! {
                    <button
                        on:click=move |_| checkout_action.dispatch(())
                        class="inline-flex justify-center items-center mt-2 text-white bg-gradient-to-r from-[var(--primary-color)] via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm w-full px-8 py-3 text-center me-2 mb-2"
                    >
                        "Zur Kasse"
                        <svg
                            class="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 10"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9"
                            ></path>
                        </svg>
                    </button>
                }
                    .into_view()
            }
        }
        None => view! {}.into_view(),
    }
}
