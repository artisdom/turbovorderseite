use crate::{contexts::cart_context::CartItem, economy::ShopCurrency};

use leptos::*;

#[must_use]
#[component]
pub fn ShippingPartiallySum(item_sum_resource: Resource<Vec<CartItem>, i32>) -> impl IntoView {
    view! {
        <div class="flex justify-between mb-0.5">
            <p>"Zwischensumme"</p>
            <p>
                {move || match item_sum_resource.get() {
                    Some(sum) => ShopCurrency::from(sum).to_string(),
                    None => "Zwischenpreise werden berechnet".to_owned(),
                }}

            </p>
        </div>
    }
}
