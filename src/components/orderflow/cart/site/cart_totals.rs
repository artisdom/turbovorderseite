mod share_cart_button;
pub mod share_cart_modal;
mod shipping_checkout;
mod shipping_details;
mod shipping_partially_sum;
pub mod shipping_time;
mod shipping_total_sum;

use crate::{
    components::orderflow::cart::site::cart_totals::{
        share_cart_button::ShareCartButton, shipping_checkout::ShippingCheckout,
        shipping_details::ShippingFeeDetails, shipping_partially_sum::ShippingPartiallySum,
        shipping_time::ShippingTime, shipping_total_sum::ShippingTotalSum,
    },
    contexts::cart_context::CartContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn CartTotals() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    let item_sum_resource = create_local_resource(
        move || cart_context.get_raw_items(),
        move |_| async move { cart_context.get_item_sum().await },
    );
    let cart_lock_resource = create_local_resource(
        move || cart_context.get_raw_items(),
        move |_| async move { cart_context.is_locked().await },
    );

    view! {
        <div class="rounded-xl h-fit mt-0 m-4 md:mt-4 p-4 bg-white dark:bg-[var(--secondary-color)] md:w-80 dark:text-white shadow">
            <div class="border-b border-gray-400 pb-2">
                <ShippingPartiallySum item_sum_resource=item_sum_resource/>
                <ShippingFeeDetails
                    item_sum_resource=item_sum_resource
                    cart_lock_resource=cart_lock_resource
                />
                <div class="w-72">
                    <p class="text-sm text-gray-800 dark:text-gray-300">
                        "MwSt. wird nicht ausgewiesen (Kleinunternehmer, § 19 UStG)"
                    </p>
                </div>
            </div>
            <ShippingTime/>
            <ShippingTotalSum item_sum_resource=item_sum_resource/>
            <ShippingCheckout cart_lock_resource=cart_lock_resource/>
            <ShareCartButton/>
        </div>
    }
}
