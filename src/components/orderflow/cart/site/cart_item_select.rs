use leptos::*;

use crate::contexts::cart_context::CartItem;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn CartItemSelect(item: CartItem) -> impl IntoView {
    let options = (1..100)
        .map(|idx| {
            {
                let is_eq_quantity = idx == item.quantity;
                view! {
                    <Show
                        when=move || is_eq_quantity
                        fallback=move || { view! { <option value=idx>{idx}</option> }.into_view() }
                    >

                        <option selected value=idx>
                            {idx}
                        </option>
                    </Show>
                }
            }
            .into_view()
        })
        .collect_view();
    view! {
        <select name="item-amount" class="rounded-lg bg-blue-600 text-white p-0.5">
            {options}
        </select>
    }
}
