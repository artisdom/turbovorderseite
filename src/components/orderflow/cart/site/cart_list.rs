use super::cart_item::CartListItem;
use crate::{
    components::orderflow::cart::site::cart_totals::CartTotals, contexts::cart_context::CartItem,
};

use leptos::*;
use model::Product;

#[must_use]
#[component]
pub fn CartList(cart_items: Vec<(CartItem, Product)>) -> impl IntoView {
    view! {
        <ul class="mt-2 p-4 md:p-0">
            <For
                each=move || cart_items.clone()
                key=|item| item.0.product_id.clone()
                children=move |item| {
                    view! {
                        <CartListItem product=item.1 quantity=item.0.quantity allow_actions=true/>
                    }
                        .into_view()
                }
            />

        </ul>
        <CartTotals/>
    }
    .into_view()
}
