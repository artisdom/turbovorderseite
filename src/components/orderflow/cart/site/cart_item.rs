use leptos::*;
use leptos_router::A;
use model::Product;
use url::Url;

use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::cart_context::CartContext,
    economy::ShopCurrency,
};

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[allow(clippy::too_many_lines)]
#[component]
pub fn CartListItem(product: Product, quantity: i32, allow_actions: bool) -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    let product_link = format!("/product/{}", product.id);
    let product_preview_media = product.preview_media;

    view! {
        <li class="flex flex-col bg-white dark:bg-[var(--secondary-color)] dark:text-white rounded-xl shadow p-4 my-2">
            <A href=product_link.clone() class="font-bold text-lg mb-2 line-clamp-1">
                {product.name}
            </A>
            <div class="flex justify-between space-x-2 md:space-x-14">
                <div class="flex">
                    {match product_preview_media {
                        Some(media) => {
                            let cdn = expect_context::<ContentProvider>();
                            let opt_url = cdn
                                .provide_content_network(
                                    Url::parse(&media.url),
                                    &CdnOptions {
                                        quality: Some(100),
                                        size: Some(512),
                                        crop_width: None,
                                        crop_height: None,
                                        crop_x: None,
                                        crop_y: None,
                                    },
                                    CdnPullType::Image,
                                );
                            view! {
                                <A href=product_link>
                                    <img
                                        src=opt_url.to_string()
                                        class="w-[3.81rem] h-[2.66rem] md:w-[11.43rem] md:h-[8rem] rounded-lg object-cover object-center"
                                    />
                                </A>
                            }
                                .into_view()
                        }
                        None => view! { <div class="bg-gray-300 p-6"></div> }.into_view(),
                    }}
                    <div class="w-30 ml-2">
                        <Show
                            when=move || product.is_orderable
                            fallback=move || {
                                view! { <p class="text-red-600 text-xs">"Nicht Verfügbar"</p> }
                            }
                        >

                            <AvailableCartIndicator>"Verfügbar"</AvailableCartIndicator>
                        </Show>
                        <p class="text-xs m-1 md:mt-2 md:m-0 md:text-base">{product.id.clone()}</p>
                    </div>
                </div>
                <div class="space-y-1 md:space-y-1.5">
                    <p class="text-sm">"Stückpreis"</p>
                    <p class="font-bold flex justify-end">
                        {ShopCurrency::from(product.price.unwrap_or_default()).to_string()}
                    </p>
                </div>
                <div class="space-y-1 md:space-y-1.5">
                    <p class="text-sm">"Anzahl"</p>
                    <div class="flex items-center space-x-2">
                        {if allow_actions {
                            view! {
                                <button
                                    on:click={
                                        let product_id = product.id.clone();
                                        move |_| {
                                            cart_context
                                                .saturating_from_cart(&product_id.clone(), false);
                                        }
                                    }

                                    class="text-lg font-bold hover:text-[var(--primary-color)]"
                                >
                                    "-"
                                </button>
                            }
                                .into_view()
                        } else {
                            view! {}.into_view()
                        }}
                        <p class="font-bold">{quantity}</p>
                        {if allow_actions {
                            view! {
                                <button
                                    on:click={
                                        let product_id = product.id.clone();
                                        move |_| {
                                            cart_context
                                                .saturating_from_cart(&product_id.clone(), true);
                                        }
                                    }

                                    class="text-lg font-bold hover:text-[var(--primary-color)]"
                                >
                                    "+"
                                </button>
                            }
                                .into_view()
                        } else {
                            view! {}.into_view()
                        }}

                    </div>
                </div>
                <div class="flex flex-col justify-between">
                    <div class="hidden md:block space-y-1 md:space-y-1.5">
                        <p class="text-sm">"Zwischensumme"</p>
                        <p class="font-bold flex justify-end">
                            {ShopCurrency::from(product.price.unwrap_or_default() * quantity)
                                .to_string()}
                        </p>
                    </div>
                    {if allow_actions {
                        view! {
                            <button
                                on:click={
                                    let product_id = product.id.clone();
                                    move |_| {
                                        cart_context.remove_from_cart(&product_id);
                                    }
                                }

                                class="flex justify-end"
                            >
                                <svg
                                    class="w-6 h-6 text-red-600 dark:text-red-600"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                >
                                    <path
                                        stroke="currentColor"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M5 7h14m-9 3v8m4-8v8M10 3h4a1 1 0 0 1 1 1v3H9V4a1 1 0 0 1 1-1ZM6 7h12v13a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V7Z"
                                    ></path>
                                </svg>
                            </button>
                        }
                            .into_view()
                    } else {
                        view! {}.into_view()
                    }}

                </div>
            </div>
        </li>
    }
}

#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn AvailableCartIndicator(children: ChildrenFn) -> impl IntoView {
    view! {
        <span class="inline-flex items-center bg-green-100 text-green-800 font-medium px-1.5 py-[0.09rem] md:px-2.5 md:py-0.5 rounded-full dark:bg-green-900 dark:text-green-300">
            <span class="hidden md:block relative flex h-2 w-2 mr-1">
                <span class="absolute animate-ping w-2 h-2 me-1 bg-green-500 rounded-full"></span>
                <span class="absolute w-2 h-2 me-1 bg-green-500 rounded-full"></span>
            </span>
            <p class="text-xs md:text-sm">{children()}</p>
        </span>
    }
}
