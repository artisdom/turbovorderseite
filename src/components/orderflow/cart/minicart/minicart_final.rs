use crate::{
    components::orderflow::cart::minicart::minicart_checkout_button::MinicartCheckoutButton,
    contexts::cart_context::CartContext, economy::ShopCurrency,
};

use leptos::*;
use leptos_router::{use_navigate, NavigateOptions};

#[must_use]
#[component]
pub fn MinicartFinal() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    let navigate_action = create_action(move |()| async move {
        let navigate = use_navigate();
        navigate("/cart", NavigateOptions::default());
        cart_context.set_collapse();
    });

    let item_sum_resource = create_local_resource(
        move || cart_context.get_raw_items(),
        move |_| async move { cart_context.get_item_sum().await },
    );
    let cart_lock_resource = create_local_resource(
        move || cart_context.get_raw_items(),
        move |_| async move { !cart_context.is_locked().await },
    );

    view! {
        <div class="border-t border-gray-200 px-4 py-6 sm:px-6">
            <div class="flex justify-between text-base font-medium text-gray-300">
                <p>"Summe"</p>
                <p>
                    {move || match item_sum_resource.get() {
                        Some(sum) => ShopCurrency::from(sum).to_string(),
                        None => "Summe wird berechnet".to_owned(),
                    }}

                </p>
            </div>
            <p class="mt-0.5 text-sm text-gray-500">
                "Versand und Steuern werden bei der Kasse berechnet."
            </p>
            <div class="mt-6">
                <Show when=move || (cart_context.get_item_count() > 0 && cart_lock_resource.get().unwrap_or(false))>
                    <MinicartCheckoutButton/>
                </Show>
                <button
                    on:click=move |_| navigate_action.dispatch(())
                    class="flex items-center w-full justify-center rounded-md border border-transparent bg-[var(--primary-color)] mt-2 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-blue-700"
                >
                    "Warenkorb anschauen"
                </button>

            </div>
            <div class="mt-6 flex justify-center text-center text-sm text-gray-500">
                <button
                    type="button"
                    class="font-medium text-lg text-indigo-200 hover:text-indigo-300"
                    on:click=move |_| cart_context.set_collapse()
                >
                    "Weiter einkaufen"
                </button>
            </div>
        </div>
    }
}
