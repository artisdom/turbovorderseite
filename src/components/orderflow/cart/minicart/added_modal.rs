use leptos::{html::Div, *};
use leptos_router::A;
use leptos_use::use_element_hover;
use model::Product;
use url::Url;
use wasm_bindgen::UnwrapThrowExt;

use crate::cdn::{CdnOptions, ContentProvider};

#[must_use]
#[component]
pub fn AddedModal(product: Product, quantity: i32, area_el: NodeRef<Div>) -> impl IntoView {
    let cdn = expect_context::<ContentProvider>();
    let modal_el = create_node_ref::<Div>();

    let is_modal_hovered = use_element_hover(modal_el);
    let close_modal = create_action(move |ignore_screen: &bool| {
        let ignore_screen = ignore_screen.to_owned();
        async move {
            if !is_modal_hovered.get_untracked() | ignore_screen {
                let _ = area_el
                    .get_untracked()
                    .unwrap_throw()
                    .style("display", "none");
            }
        }
    });

    view! {
        <div node_ref=area_el class="hidden">
            <div
                on:click=move |_| close_modal.dispatch(false)
                class="flex justify-center items-center fixed inset-0 z-50 overflow-auto bg-black/[.27] dark:bg-black/[.65]"
            >
                <div
                    node_ref=modal_el
                    class="bg-white dark:bg-[var(--secondary-color)] p-4 m-4 md:m-0 md:p-8 md:text-lg rounded-xl"
                >
                    <div class="flex items-center md:space-x-4">
                        <div class="hidden md:block">
                            {if let Some(preview_media) = product.preview_media {
                                let opt_url = cdn
                                    .provide_content_network(
                                        Url::parse(&preview_media.url),
                                        &CdnOptions {
                                            quality: Some(100),
                                            size: Some(256),
                                            crop_width: None,
                                            crop_height: None,
                                            crop_x: None,
                                            crop_y: None,
                                        },
                                        crate::cdn::CdnPullType::Image,
                                    );
                                view! { <img class="h-24 rounded-xl" src=opt_url.to_string()/> }
                                    .into_view()
                            } else {
                                view! { <div class="w-24 h-24 bg-grey-100 rounded-xl"></div> }
                                    .into_view()
                            }}

                        </div>
                        <div class="flex flex-col justify-between">
                            <p class="text-xl md:text-2xl font-bold">"Zum Warenkorb hinzugefügt!"</p>
                            <div class="flex items-center mt-1 space-x-4 md:space-x-2">
                                <b>{format!("{}x", quantity)}</b>
                                <p>{product.name}</p>
                            </div>
                            <div class="flex items-center mt-3 space-x-1">
                                <button
                                    on:click=move |_| close_modal.dispatch(true)
                                    type="button"
                                    class="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 me-2 dark:bg-[var(--secondary-color)] dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                                >
                                    "Zurück zum Produkt"
                                </button>
                                <A
                                    href="/cart"
                                    class="inline-flex justify-center w-56 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                >
                                    "Gehe zum Warenkorb"
                                    <svg
                                        class="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 14 10"
                                    >
                                        <path
                                            stroke="currentColor"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="2"
                                            d="M1 5h12m0 0L9 1m4 4L9 9"
                                        ></path>
                                    </svg>
                                </A>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}
