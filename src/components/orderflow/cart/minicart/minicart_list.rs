use crate::{
    components::orderflow::cart::minicart::minicart_item::MinicartItem,
    contexts::cart_context::CartContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn MinicartList() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    let items_resource = create_local_resource(
        move || cart_context.get_raw_items(),
        move |_| async move { cart_context.get_items().await },
    );

    view! {
        <ul role="list" class="-my-6 divide-y divide-gray-200">
            {move || match items_resource.get() {
                Some(items) => {
                    view! {
                        <For
                            each=move || items.clone()
                            key=|item| item.0.product_id.clone()
                            children=move |item| {
                                view! {
                                    <li class="flex py-6">
                                        <MinicartItem cart_item=item.1 quantity=item.0.quantity/>
                                    </li>
                                }
                                    .into_view()
                            }
                        />
                    }
                }
                None => view! { <p class="dark:text-white">"Lade Produkte..."</p> }.into_view(),
            }}

        </ul>
    }
}
