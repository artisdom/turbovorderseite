use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::cart_context::CartContext,
    economy::ShopCurrency,
};

use leptos::*;
use leptos_router::A;
use model::Product;
use url::Url;

#[must_use]
#[component]
pub fn MinicartItem(cart_item: Product, quantity: i32) -> impl IntoView {
    let cdn_context = expect_context::<ContentProvider>();
    let cart_context = expect_context::<CartContext>();

    let product_link = format!("/product/{}", cart_item.id);
    let mut image_alt = format!("product preview image of {}", cart_item.name);

    view! {
        <A href=product_link.clone()>
            <div class="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                {match cart_item.preview_media.clone() {
                    Some(preview_media) => {
                        let opt_url = cdn_context
                            .provide_content_network(
                                Url::parse(&preview_media.url),
                                &CdnOptions {
                                    quality: Some(100),
                                    size: Some(256),
                                    crop_width: None,
                                    crop_height: None,
                                    crop_x: None,
                                    crop_y: None,
                                },
                                CdnPullType::Image,
                            );
                        if let Some(specified_alt) = preview_media.alt {
                            image_alt = format!("product preview image of {specified_alt}");
                        }
                        view! {
                            <img
                                src=opt_url.to_string()
                                alt=image_alt
                                class="h-full w-full object-cover object-center"
                            />
                        }
                            .into_view()
                    }
                    None => view! { <div class="w-full h-full bg-gray-300"></div> }.into_view(),
                }}

            </div>
        </A>
        <div class="ml-4 flex flex-1 flex-col">
            <div>
                <div class="flex justify-between text-base font-medium text-gray-200">
                    <A href=product_link>
                        <p class="line-clamp-2">{cart_item.name}</p>
                    </A>
                    <div class="p-2">
                        <p class="whitespace-nowrap">
                            {match cart_item.price {
                                Some(price) => (ShopCurrency::from(price * quantity)).to_string(),
                                None => "Nicht verfügbar".to_owned(),
                            }}

                        </p>
                    </div>
                </div>
            </div>
            <div class="flex flex-1 items-baseline justify-between text-sm">
                <div class="flex items-center space-x-2 text-white">
                    <button
                        on:click={
                            let product_id = cart_item.id.clone();
                            move |_| {
                                cart_context.saturating_from_cart(&product_id, false);
                            }
                        }

                        class="text-lg font-bold hover:text-[var(--primary-color)]"
                    >
                        "-"
                    </button>
                    <p class="font-bold">{format!("{} x", quantity)}</p>
                    <button
                        on:click={
                            let product_id = cart_item.id.clone();
                            move |_| {
                                cart_context.saturating_from_cart(&product_id, true);
                            }
                        }

                        class="text-lg font-bold hover:text-[var(--primary-color)]"
                    >
                        "+"
                    </button>
                </div>

                <div class="flex">
                    <button
                        on:click=move |_| cart_context.remove_from_cart(&cart_item.id)
                        type="button"
                        class="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-1.5 me-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                    >
                        "Entfernen"
                    </button>
                </div>
            </div>
        </div>
    }
}
