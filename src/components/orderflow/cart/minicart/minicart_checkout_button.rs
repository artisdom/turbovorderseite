use crate::contexts::{cart_context::CartContext, order_context::OrderContext};

use leptos::*;
use leptos_oidc::Auth;
use leptos_router::{use_navigate, NavigateOptions};

#[must_use]
#[component]
pub fn MinicartCheckoutButton() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();
    let auth_context = expect_context::<Auth>();

    let lock_signal = create_rw_signal(false);

    let checkout_action = create_action(move |()| async move {
        let order_context = expect_context::<OrderContext>();
        let cart_context = expect_context::<CartContext>();
        lock_signal.set(true);
        if let Some(checkout_url) = order_context
            .send_checkout(cart_context.get_items().await)
            .await
        {
            let _ = window().location().set_href(&checkout_url);
        }
        lock_signal.set(false);
    });

    let checkout_forward_action = create_action(move |()| async move {
        let navigate = use_navigate();
        navigate("/checkout-forward", NavigateOptions::default());
        cart_context.set_collapse();
    });

    move || {
        if auth_context.authenticated() {
            view! {
                <button
                    on:click=move |_| checkout_action.dispatch(())
                    class="flex items-center w-full justify-center rounded-md border-2 border-[#10e090] px-6 py-3 text-base font-medium text-[#10e090] shadow-sm hover:bg-[#10e090] hover:text-black"
                >
                    "Zur Kasse"
                </button>
            }
            .into_view()
        } else if !lock_signal.get() {
            view! {
                <button
                    on:click=move |_| checkout_forward_action.dispatch(())
                    class="flex items-center w-full justify-center rounded-md border-2 border-[#10e090] px-6 py-3 text-base font-medium text-[#10e090] shadow-sm hover:bg-[#10e090] hover:text-black"
                >
                    "Zur Kasse"
                </button>
            }
            .into_view()
        } else {
            view! {
                <button
                    class="flex items-center w-full justify-center rounded-md border-2 border-[#10e090] px-6 py-3 text-base font-medium text-[#10e090] shadow-sm"
                    disabled
                >
                    "Lade Kasse..."
                </button>
            }.into_view()
        }
    }
}
