use crate::{
    components::orderflow::cart::minicart::{
        minicart_final::MinicartFinal, minicart_list::MinicartList,
    },
    contexts::cart_context::CartContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn MiniCartMenu() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    view! {
        <div class="relative z-20">
            <div
                on:click=move |_| cart_context.set_collapse()
                class="fixed inset-0 z-10 bg-gray-500 transition bg-opacity-50 transition-opacity"
            ></div>
            <div class="fixed z-30 overflow-hidden">
                <div class="absolute inset-0 overflow-hidden">
                    <div class="pointer-events-none fixed inset-y-0 right-0 flex max-w-full md:pl-10">
                        <div class="pointer-events-auto w-screen md:max-w-md">
                            <div class="flex h-full flex-col bg-slate-900 shadow-xl">
                                <div class="flex-1 overflow-y-auto px-4 py-6 sm:px-6 hide-scrollbar">
                                    <div class="flex items-start justify-between">
                                        <p
                                            class="text-lg font-medium text-gray-300"
                                            id="slide-over-title"
                                        >
                                            "Einkaufswagen"
                                        </p>
                                        <div class="ml-3 flex h-7 items-center">
                                            <button
                                                on:click=move |_| cart_context.set_collapse()
                                                type="button"
                                                class="relative -m-2 p-2 text-gray-400 hover:text-gray-500"
                                            >
                                                <span class="absolute -inset-0.5"></span>
                                                <span class="sr-only">Close panel</span>
                                                <svg
                                                    class="h-6 w-6"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    stroke-width="1.5"
                                                    stroke="currentColor"
                                                    aria-hidden="true"
                                                >
                                                    <path
                                                        stroke-linecap="round"
                                                        stroke-linejoin="round"
                                                        d="M6 18L18 6M6 6l12 12"
                                                    ></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="mt-8">
                                        <MinicartList/>
                                    </div>
                                </div>
                                <MinicartFinal/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}
