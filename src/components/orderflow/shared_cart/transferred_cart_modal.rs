use leptos::{html::Div, *};
use leptos_use::use_element_hover;
use wasm_bindgen::UnwrapThrowExt;

#[must_use]
#[component]
pub fn TransferredCartModal(item_count: i32, area_el: NodeRef<Div>) -> impl IntoView {
    let modal_el = create_node_ref::<Div>();

    let is_modal_hovered = use_element_hover(modal_el);
    let close_modal = create_action(move |ignore_screen: &bool| {
        let ignore_screen = ignore_screen.to_owned();
        async move {
            if !is_modal_hovered.get_untracked() | ignore_screen {
                let _ = area_el
                    .get_untracked()
                    .unwrap_throw()
                    .style("display", "none");
            }
        }
    });

    view! {
        <div node_ref=area_el class="hidden">
            <div
                on:click=move |_| close_modal.dispatch(false)
                class="flex justify-center items-center fixed inset-0 z-50 overflow-auto bg-black/[.27] dark:bg-black/[.65]"
            >
                <div
                    node_ref=modal_el
                    class="bg-white dark:bg-slate-900 p-4 m-4 md:m-0 md:p-8 md:text-lg rounded-xl"
                >
                    <p class="text-xl md:text-2xl"><b>{format!("{item_count} Artikel")}</b>" wurden zu deinem eigenen Einkaufswagen hinzugefügt"</p>
                    <button
                        on:click=move |_| close_modal.dispatch(true)
                        type="button"
                        class="text-gray-900 md:hidden mt-2 bg-white w-full border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 me-2 dark:bg-[var(--secondary-color)] dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                    >
                        "Zurück zum geteilten Warenkorb"
                    </button>
                </div>
            </div>
        </div>
    }
}
