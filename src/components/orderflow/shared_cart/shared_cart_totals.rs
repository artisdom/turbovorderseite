use crate::{
    components::orderflow::{
        cart::{cart_indicator::WarningIndicator, site::cart_totals::shipping_time::ShippingTime},
        shared_cart::transferred_cart_modal::TransferredCartModal,
    },
    contexts::{
        cart_context::{CartContext, CartItem},
        order_context::OrderContext,
    },
    economy::ShopCurrency,
    error::AppError,
};

use leptos::{html::Div, *};
use model::Product;
use wasm_bindgen::UnwrapThrowExt;

#[allow(clippy::similar_names)]
#[must_use]
#[component]
pub fn SharedCartTotals(cart_content: Vec<(CartItem, Result<Product, AppError>)>) -> impl IntoView {
    let cart_context = expect_context::<CartContext>();
    let area_el = create_node_ref::<Div>();

    let filtered_content = cart_content
        .iter()
        .filter(|(_, product_result)| product_result.is_ok())
        .map(|content| (content.0.clone(), content.1.clone().unwrap()))
        .collect::<Vec<_>>();
    let item_count = filtered_content
        .iter()
        .fold(0, |count, item| count + item.0.quantity);
    let sum = filtered_content.iter().fold(0, |sum, (item, product)| {
        sum + item.quantity * product.clone().price.unwrap_or_default()
    });
    let locked = filtered_content
        .iter()
        .any(|(_, product)| !product.clone().is_orderable);

    let transfer_action = create_action({
        let filtered_content = filtered_content.clone();
        move |()| {
            let filtered_content = filtered_content.to_owned();
            async move {
                cart_context.transfer_to_cart(
                    filtered_content
                        .iter()
                        .map(|(item, _)| item.clone())
                        .collect::<Vec<_>>(),
                );
                let _ = area_el
                    .get_untracked()
                    .unwrap_throw()
                    .style("display", "block");
            }
        }
    });

    let checkout_action = create_action(move |()| {
        let filtered_content = filtered_content.to_owned();
        async move {
            let order_context = expect_context::<OrderContext>();
            if let Some(checkout_url) = order_context.send_checkout(filtered_content).await {
                let _ = window().location().set_href(&checkout_url);
            }
        }
    });

    view! {
        <div class="rounded-xl h-fit mt-0 m-4 md:mt-4 p-4 bg-white dark:bg-[var(--secondary-color)] md:w-80 dark:text-white shadow">
            <div class="border-b border-gray-400 pb-2">
                <div class="flex justify-between mb-0.5">
                    <p>"Zwischensumme"</p>
                    <p>{ShopCurrency::from(sum).to_string()}</p>
                </div>
                <div class="flex justify-between">
                    <p>"Versandkosten"</p>
                    <p>{ShopCurrency::from(cart_context.check_shipping_costs(sum)).to_string()}</p>
                </div>
                <div class="w-72">
                    <p class="text-sm text-gray-800 dark:text-gray-300">
                        "MwSt. wird nicht ausgewiesen (Kleinunternehmer, § 19 UStG)"
                    </p>
                </div>
            </div>
            <div>
                <Show when=move || {
                    cart_context.check_shipping_costs(sum) > 0 && !locked && sum > 6000
                }>
                    <div class="w-full my-2.5">
                        <WarningIndicator>
                            "Noch " <b>{ShopCurrency::from(9000 - sum).to_string()}</b>
                            " um kostenlosen Versand zu erhalten."
                        </WarningIndicator>
                    </div>
                </Show>
            </div>
            <ShippingTime/>
            <div class="mt-2 flex justify-between text-lg">
                <b>"Gesamtsumme"</b>
                <b>
                    {ShopCurrency::from(sum + cart_context.check_shipping_costs(sum)).to_string()}
                </b>
            </div>
            <button
                on:click=move |_| transfer_action.dispatch(())
                class="relative w-full inline-flex items-center justify-center mt-2 p-0.5 mb-2 me-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-cyan-500 to-blue-500 group-hover:from-cyan-500 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-cyan-200 dark:focus:ring-cyan-800"
            >
                <span class="relative w-full px-5 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-[var(--secondary-color)] rounded-md group-hover:bg-opacity-0">
                    "In eigenen Warenkorb übertragen"
                </span>
            </button>
            <button
                on:click=move |_| checkout_action.dispatch(())
                class="inline-flex justify-center items-center text-white bg-gradient-to-r from-[var(--primary-color)] via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm w-full px-8 py-3 text-center me-2 mb-2"
            >
                "Diesen Warenkorb bestellen"
                <svg
                    class="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M1 5h12m0 0L9 1m4 4L9 9"
                    ></path>
                </svg>
            </button>
        </div>
        <TransferredCartModal
            item_count=item_count
            area_el=area_el
        />
    }
}
