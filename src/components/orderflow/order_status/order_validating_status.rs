use leptos::*;

#[must_use]
#[component]
pub fn OrderValidating() -> impl IntoView {
    view! {
        <div class="my-20">
            <div class="flex justify-center mb-8">
                <svg
                    class="w-20 h-20"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                >
                    <path
                        stroke="var(--primary-color)"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M17.651 7.65a7.131 7.131 0 0 0-12.68 3.15M18.001 4v4h-4m-7.652 8.35a7.13 7.13 0 0 0 12.68-3.15M6 20v-4h4"
                    ></path>
                </svg>
            </div>
            <div class="flex flex-col space-y-2 mb-8">
                <b class="text-3xl text-center text-[var(--primary-color)] uppercase">
                    "Bestellung wird validiert..."
                </b>
            </div>
        </div>
    }.into_view()
}
