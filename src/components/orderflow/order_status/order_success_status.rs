use leptos::*;

use leptos_router::{use_navigate, NavigateOptions};
use model::Order;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn OrderSuccess(order: Order) -> impl IntoView {
    view! {
        <div class="my-20">
            <div class="flex justify-center mb-8">
                <svg
                    class="w-20"
                    version="1.1"
                    id="Capa_1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    viewBox="0 0 50 50"
                    xml:space="preserve"
                >
                    <circle fill="var(--primary-color)" cx="25" cy="25" r="25"></circle>
                    <polyline
                        style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;"
                        points="
                        38,15 22,33 12,25 "
                    ></polyline>
                </svg>
            </div>
            <div class="flex flex-col space-y-2 mb-8">
                <b class="text-3xl text-center text-[var(--primary-color)] uppercase">
                    {format!("Danke für deine Bestellung #{}!", order.id)}
                </b>
                <b class="text-sm text-center text-[var(--primary-color)] uppercase">
                    "Wir senden dir deine Auftragsbestätigung mit Details und Informationen per E-Mail zu."
                </b>
            </div>
            <div class="flex justify-center">
                <button
                    on:click=move |_| {
                        let navigate = use_navigate();
                        navigate("", NavigateOptions::default());
                    }

                    class="mt-2 flex items-center space-x-2 text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2 "
                >
                    <svg
                        class="w-4 h-4 text-white"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 14 10"
                    >
                        <path
                            stroke="currentColor"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M13 5H1m0 0 4 4M1 5l4-4"
                        ></path>
                    </svg>
                    "Zurück zum Shop"
                </button>
            </div>
        </div>
    }.into_view()
}
