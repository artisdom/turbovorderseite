mod filter_sidebar_categories;
mod filter_sidebar_dropdown;
mod filter_sidebar_menu;
mod filter_sidebar_menu_item;
mod filter_sidebar_menu_item_head;
mod filter_sidebar_menu_item_list;
mod filter_sidebar_mobile_dialog;

use leptos::*;

use crate::components::{
    products::filter_sidebar::{
        filter_sidebar_dropdown::FilterSidebarDropdown, filter_sidebar_menu::FilterSidebarMenu,
        filter_sidebar_mobile_dialog::FilterSidebarMobileDialog,
    },
    standards::pagination::Pagination,
};

#[component]
pub fn FilterSideBar(
    children: ChildrenFn,
    category_signal: RwSignal<String>,
    sort_signal: RwSignal<Vec<String>>,
    page_signal: RwSignal<usize>,
    total_pages_signal: RwSignal<usize>,
) -> impl IntoView {
    view! {
        <div>
            <FilterSidebarMobileDialog/>
            <main class="mx-auto max-w-[110rem] px-4 sm:px-6 lg:px-8">
                <div class="flex items-baseline justify-between border-b border-gray-200 pb-6 pt-24">
                    <h1 class="text-4xl font-bold tracking-tight text-gray-900 dark:text-white">
                        {move || category_signal.get()}
                    </h1>
                    <div class="flex items-center">
                        <div class="relative inline-block text-left">
                            <FilterSidebarDropdown sort_signal=sort_signal/>
                        </div>
                        <button
                            type="button"
                            class="-m-2 ml-5 p-2 text-gray-400 hover:text-gray-500 sm:ml-7"
                        >
                            <span class="sr-only">View grid</span>
                            <svg
                                class="h-5 w-5"
                                aria-hidden="true"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fill-rule="evenodd"
                                    d="M4.25 2A2.25 2.25 0 002 4.25v2.5A2.25 2.25 0 004.25 9h2.5A2.25 2.25 0 009 6.75v-2.5A2.25 2.25 0 006.75 2h-2.5zm0 9A2.25 2.25 0 002 13.25v2.5A2.25 2.25 0 004.25 18h2.5A2.25 2.25 0 009 15.75v-2.5A2.25 2.25 0 006.75 11h-2.5zm9-9A2.25 2.25 0 0011 4.25v2.5A2.25 2.25 0 0013.25 9h2.5A2.25 2.25 0 0018 6.75v-2.5A2.25 2.25 0 0015.75 2h-2.5zm0 9A2.25 2.25 0 0011 13.25v2.5A2.25 2.25 0 0013.25 18h2.5A2.25 2.25 0 0018 15.75v-2.5A2.25 2.25 0 0015.75 11h-2.5z"
                                    clip-rule="evenodd"
                                ></path>
                            </svg>
                        </button>
                        <button
                            type="button"
                            class="-m-2 ml-4 p-2 text-gray-400 hover:text-gray-500 sm:ml-6 lg:hidden"
                        >
                            <span class="sr-only">Filters</span>
                            <svg
                                class="h-5 w-5"
                                aria-hidden="true"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fill-rule="evenodd"
                                    d="M2.628 1.601C5.028 1.206 7.49 1 10 1s4.973.206 7.372.601a.75.75 0 01.628.74v2.288a2.25 2.25 0 01-.659 1.59l-4.682 4.683a2.25 2.25 0 00-.659 1.59v3.037c0 .684-.31 1.33-.844 1.757l-1.937 1.55A.75.75 0 018 18.25v-5.757a2.25 2.25 0 00-.659-1.591L2.659 6.22A2.25 2.25 0 012 4.629V2.34a.75.75 0 01.628-.74z"
                                    clip-rule="evenodd"
                                ></path>
                            </svg>
                        </button>
                    </div>
                </div>

                <section aria-labelledby="products-heading" class="pb-18 pt-6">
                    <h2 id="products-heading" class="sr-only dark:text-white">
                        "Products"
                    </h2>

                    <div class="flex">
                        // <!-- Filters -->
                        <FilterSidebarMenu page_signal=page_signal/>

                        // <!-- Product grid -->
                        <ul class="lg:col-span-3 flex flex-wrap h-fit">{children}</ul>
                    </div>
                </section>
                <Pagination page_signal=page_signal total_pages=total_pages_signal/>
            </main>
        </div>
    }
}
