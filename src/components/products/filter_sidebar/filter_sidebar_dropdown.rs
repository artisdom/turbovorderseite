use leptos::*;

#[component]
pub fn FilterSidebarDropdown(sort_signal: RwSignal<Vec<String>>) -> impl IntoView {
    let collapse_signal = create_rw_signal(false);

    let sort_action = create_action(move |sort_property: &String| {
        let mut sort_list = Vec::new();
        let sort_property = sort_property.to_owned();
        sort_list.push(sort_property);
        async move {
            collapse_signal.update(|upd| *upd = false);
            sort_signal.set(sort_list);
        }
    });

    view! {
        <div class="dark:text-white">
            <Show when=move || (sort_signal.get().len() > 0)>
                <button
                    type="button"
                    on:click=move |_| sort_signal.update(|upd| upd.clear())
                    class="py-2.5 px-5 me-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-[var(--secondary-color)] dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                >
                    "Zurücksetzen"
                </button>
            </Show>
            <button
                on:click=move |_| collapse_signal.update(|upd| *upd = !*upd)
                type="button"
                class="group inline-flex justify-center text-sm font-medium text-gray-700 hover:text-gray-900 dark:text-gray-100 dark:hover:text-gray-300"
                id="menu-button"
            >
                {move || {
                    if let Some(category) = sort_signal.get().first() {
                        let category = category.clone();
                        match category.as_str() {
                            "price:asc" => "Preis: Aufsteigend",
                            "price:desc" => "Preis: Absteigend",
                            "name:asc" => "Name: Aufsteigend",
                            "name:desc" => "Name: Absteigend",
                            "updated:asc" => "Geändert: Aufsteigend",
                            "updated:desc" => "Geändert: Absteigend",
                            "created:asc" => "Erstellt: Aufsteigend",
                            "created:desc" => "Erstellt: Absteigend",
                            _ => "Sortieren",
                        }
                            .to_owned()
                    } else {
                        "Sortieren".to_owned()
                    }
                }}

                <svg
                    class="-mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    aria-hidden="true"
                >
                    <path
                        fill-rule="evenodd"
                        d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                        clip-rule="evenodd"
                    ></path>
                </svg>
            </button>
        </div>
        <Show when=move || collapse_signal.get()>
            <div
                class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none"
                aria-orientation="vertical"
                aria-labelledby="menu-button"
                tabindex="-1"
            >
                <div class="py-1" role="none">
                    <button
                        on:click=move |_| sort_action.dispatch("price:asc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Preis: Aufsteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("price:desc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-3"
                    >
                        "Preis: Absteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("name:asc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Name: Aufsteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("name:desc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Name: Absteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("updated:asc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Geändert: Aufsteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("updated:desc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Geändert: Absteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("created:asc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Erstellt: Aufsteigend"
                    </button>
                    <button
                        on:click=move |_| sort_action.dispatch("created:desc".to_owned())
                        class="text-gray-500 block px-4 py-2 text-sm"
                        tabindex="-1"
                        id="menu-item-4"
                    >
                        "Erstellt: Absteigend"
                    </button>
                </div>
            </div>
        </Show>
    }
}
