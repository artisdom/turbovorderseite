use leptos::*;

#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn FilterSidebarMenuItemHead(name: String, collapse_signal: RwSignal<bool>) -> impl IntoView {
    view! {
        <h3 class="-my-3 flow-root">
            <button
                type="button"
                on:click=move |_| collapse_signal.update(|upd| *upd = !*upd)
                class="flex w-full items-center justify-between py-3 text-sm text-gray-400 hover:text-gray-500"
                aria-controls="filter-section-0"
                aria-expanded="false"
            >
                <span class="font-medium text-gray-900 dark:text-white">
                    {name.replace("metadata.", "")}
                </span>
                <span class="ml-6 flex items-center">
                    {move || {
                        if collapse_signal.get() {
                            view! {
                                <svg
                                    class="h-5 w-5"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                    aria-hidden="true"
                                >
                                    <path
                                        fill-rule="evenodd"
                                        d="M4 10a.75.75 0 01.75-.75h10.5a.75.75 0 010 1.5H4.75A.75.75 0 014 10z"
                                        clip-rule="evenodd"
                                    ></path>
                                </svg>
                            }
                        } else {
                            view! {
                                <svg
                                    class="h-5 w-5"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                    aria-hidden="true"
                                >
                                    <path d="M10.75 4.75a.75.75 0 00-1.5 0v4.5h-4.5a.75.75 0 000 1.5h4.5v4.5a.75.75 0 001.5 0v-4.5h4.5a.75.75 0 000-1.5h-4.5v-4.5z"></path>
                                </svg>
                            }
                        }
                    }}

                </span>
            </button>
        </h3>
    }
}
