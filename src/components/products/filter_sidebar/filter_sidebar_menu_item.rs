use super::filter_sidebar_menu::MetadataItem;
use crate::{
    components::products::filter_sidebar::{
        filter_sidebar_menu_item_head::FilterSidebarMenuItemHead,
        filter_sidebar_menu_item_list::FilterSidebarMenuItemList,
    },
    contexts::filter_context::FilterContext,
};

use leptos::*;
use std::collections::HashMap;

#[component]
pub fn FilterSidebarMenuItem(
    item: MetadataItem,
    metadata_filters: Option<HashMap<String, usize>>,
    page_signal: RwSignal<usize>,
) -> impl IntoView {
    let filter_context = expect_context::<FilterContext>();
    let collapse_signal = create_rw_signal(filter_context.is_filter_key_exist(&item.clone().name));

    view! {
        <div class="border-b border-gray-200 py-6">
            <FilterSidebarMenuItemHead name=item.clone().name collapse_signal=collapse_signal/>
            <Show when=move || collapse_signal.get()>
                <FilterSidebarMenuItemList
                    metadata_item=item.clone()
                    metadata_filters=metadata_filters.clone()
                    page_signal=page_signal
                />
            </Show>
        </div>
    }
}
