use crate::contexts::filter_context::FilterContext;

use super::filter_sidebar_menu::MetadataItem;

use leptos::*;
use std::collections::{BTreeMap, HashMap};

#[component]
pub fn FilterSidebarMenuItemList(
    metadata_item: MetadataItem,
    metadata_filters: Option<HashMap<String, usize>>,
    page_signal: RwSignal<usize>,
) -> impl IntoView {
    let items: BTreeMap<_, _> = metadata_item.items.into_iter().collect();

    let filter_action = create_action(move |filter_value: &String| {
        let filter_value = filter_value.to_owned();
        let filter_signal = expect_context::<FilterContext>();
        let filter_name = metadata_item.name.clone();
        page_signal.set(1);
        async move {
            filter_signal.toggle_filter(filter_name, filter_value.clone());
        }
    });

    view! {
        <div class="pt-6" id="filter-section-0">
            <div class="space-y-4">
                <For
                    each=move || items.clone()
                    key=|item| item.1
                    children=move |(name, count)| {
                        let is_disabled = metadata_filters
                            .as_ref()
                            .map_or(
                                false,
                                |metadata_filters| !metadata_filters.keys().any(|key| key.eq(&name)),
                            );
                        let filter_context = expect_context::<FilterContext>();
                        let is_filter_value_exist = filter_context
                            .is_filter_value_exist(&name.clone());
                        view! {
                            <div class="flex items-center">

                                {
                                    let name = name.clone();
                                    view! {
                                        <Show
                                            when=move || is_filter_value_exist
                                            fallback={
                                                let name = name.clone();
                                                move || {
                                                    view! {
                                                        <input
                                                            on:change=move |ev| {
                                                                filter_action.dispatch(event_target_value(&ev));
                                                            }

                                                            id=format!("filter-{}", name)
                                                            value=name.clone()
                                                            type="checkbox"
                                                            class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                                        />
                                                    }
                                                        .into_view()
                                                }
                                            }
                                        >

                                            <input
                                                on:change=move |ev| {
                                                    filter_action.dispatch(event_target_value(&ev));
                                                }

                                                checked="checked"
                                                id=format!("filter-{}", name)
                                                value=name.clone()
                                                type="checkbox"
                                                class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                            />
                                        </Show>
                                    }
                                }
                                <label
                                    for=format!("filter-{}", name)
                                    class=move || {
                                        format!(
                                            "{} ml-3 text-sm ",
                                            if is_disabled {
                                                "text-grey-700 dark:text-gray-400"
                                            } else {
                                                "text-gray-600 dark:text-white"
                                            },
                                        )
                                    }
                                >

                                    {format!("{name} ({count})")}
                                </label>
                            </div>
                        }
                    }
                />

            </div>
        </div>
    }
}
