use leptos::*;

use super::filter_sidebar_menu::MetadataItem;

#[component]
pub fn FilterSidebarCategories(metadata_set: Vec<MetadataItem>) -> impl IntoView {
    view! {
        <h3 class="sr-only">"Kategorien"</h3>
        <ul
            role="list"
            class="space-y-4 border-b border-gray-200 pb-6 text-sm font-medium text-gray-900"
        >
            <For
                each=move || metadata_set.clone()
                key=|metadata| metadata.name.clone()
                children=move |metadata: MetadataItem| {
                    metadata
                        .items
                        .iter()
                        .map(|(name, amount)| {
                            let is_parent = metadata.name.ends_with("lvl0");
                            view! {
                                <li>
                                    <a href="#" class=if is_parent { "font-bold" } else { "ml-3" }>
                                        {format!(
                                            "{} ({})",
                                            if let Some((_, sub)) = name.split_once('>') {
                                                sub
                                            } else {
                                                name
                                            },
                                            amount,
                                        )}

                                    </a>
                                </li>
                            }
                        })
                        .collect_view()
                }
            />

        </ul>
    }
}
