use std::collections::{BTreeMap, HashMap};

use leptos::*;
use leptos_meilisearch::MultiSearch;
use model::MeilisearchProduct;

use crate::components::products::filter_sidebar::{
    filter_sidebar_categories::FilterSidebarCategories,
    filter_sidebar_menu_item::FilterSidebarMenuItem,
};

#[derive(Clone)]
pub struct MetadataItem {
    pub name: String,
    pub items: HashMap<String, usize>,
}

const UNTRACKED_METADATA: [&str; 2] = ["metadata.gewicht", "metadata.ean"];

#[component]
pub fn FilterSidebarMenu(page_signal: RwSignal<usize>) -> impl IntoView {
    let meilisearch = expect_context::<MultiSearch<MeilisearchProduct>>();
    let metadatas_signal = create_rw_signal(Vec::new());
    let categories_signal = create_rw_signal(Vec::new());
    let metadata_distributions_signal = create_rw_signal(HashMap::new());

    create_effect(move |_| {
        if let Some(results) = meilisearch.results() {
            let facet_distributions = results
                .clone()
                .into_iter()
                .flat_map(|results| results.facet_distribution.into_iter().flatten())
                .collect::<BTreeMap<_, _>>();
            let metadata_set = facet_distributions
                .into_iter()
                .map(|(name, items)| MetadataItem { name, items })
                .filter(|metadata| {
                    !UNTRACKED_METADATA.contains(&metadata.name.to_lowercase().as_str())
                        && !metadata.items.is_empty()
                })
                .collect::<Vec<MetadataItem>>();
            metadatas_signal.set(metadata_set.clone());
            categories_signal.set(
                metadata_set
                    .into_iter()
                    .filter(|metadata| metadata.name.starts_with("categories."))
                    .collect::<Vec<MetadataItem>>(),
            );
            metadata_distributions_signal.set(results.first().map_or_else(
                HashMap::new,
                |metadata_results| {
                    metadata_results
                        .facet_distribution
                        .clone()
                        .unwrap_or_default()
                        .into_iter()
                        .filter(|(_, distribution_facets)| !distribution_facets.is_empty())
                        .collect::<HashMap<_, _>>()
                },
            ));
        }
    });

    view! {
        <form class="hidden lg:block min-w-[18rem]">
            <FilterSidebarCategories metadata_set=categories_signal.get_untracked()/>
            {move || {
                let metadata_distributions = metadata_distributions_signal.get();
                metadatas_signal
                    .get()
                    .into_iter()
                    .filter(|metadata| metadata.name.starts_with("metadata."))
                    .filter(|metadata| {
                        metadata_distributions.is_empty()
                            | metadata_distributions.contains_key(&metadata.name)
                    })
                    .map(|metadata| {
                        let metadata_filters = metadata_distributions.get(&metadata.name).cloned();
                        view! {
                            <FilterSidebarMenuItem
                                item=metadata
                                metadata_filters=metadata_filters.clone()
                                page_signal=page_signal
                            />
                        }
                            .into_view()
                    })
                    .collect_view()
            }}

        </form>
    }
}
