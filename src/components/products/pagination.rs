use leptos::*;
use leptos_meilisearch::SingleSearch;
use model::MeilisearchProduct;

#[must_use]
#[component]
pub fn Pagination() -> impl IntoView {
    let meilisearch = expect_context::<SingleSearch<MeilisearchProduct>>();
    let (last_query, set_last_query) = create_signal(String::new());
    let (page, set_page) = create_signal(1);
    let (total_pages, set_total_pages) = create_signal(1);

    create_effect(move |_| {
        meilisearch
            .search_query()
            .update(|upd| upd.page = Some(page.get()));

        if let Some(query) = meilisearch.query() {
            if !query.eq(&last_query.get()) {
                set_page(1);
                set_last_query(query);
            }
        }

        set_total_pages(meilisearch.total_pages().unwrap_or_default());

        let mut options = web_sys::ScrollToOptions::new();
        options.top(0.0);
        web_sys::window()
            .expect("Cannot bind window")
            .scroll_to_with_scroll_to_options(&options);
    });

    view! {
        <div class="flex flex-col items-center my-4">
            <span class="text-sm text-gray-700 dark:text-gray-400">
                "Seite " <span class="font-semibold text-gray-900 dark:text-white">{page}</span>
                " von "
                <span class="font-semibold text-gray-900 dark:text-white">{total_pages}</span>
                " Seiten"
            </span>
            <div class="inline-flex mt-2 xs:mt-0">
                <Show when=move || (page.get() > 1)>
                    <button
                        on:click=move |_| set_page.update(|upd| *upd -= 1)
                        class="flex items-center justify-center px-4 h-10 text-base font-medium text-white bg-gray-800 rounded-s hover:bg-gray-900 dark:bg-[var(--secondary-color)] dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                    >
                        <svg
                            class="w-3.5 h-3.5 me-2 rtl:rotate-180"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 10"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M13 5H1m0 0 4 4M1 5l4-4"
                            ></path>
                        </svg>
                        "Zurück"
                    </button>
                </Show>
                <Show when=move || (page.get() < total_pages.get())>
                    <button
                        on:click=move |_| set_page.update(|upd| *upd += 1)
                        class="flex items-center justify-center px-4 h-10 text-base font-medium text-white bg-gray-800 border-0 border-s border-gray-700 rounded-e hover:bg-gray-900 dark:bg-[var(--secondary-color)] dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                    >
                        "Nächste"
                        <svg
                            class="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 10"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9"
                            ></path>
                        </svg>
                    </button>
                </Show>
            </div>
        </div>
    }
}
