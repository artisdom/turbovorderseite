use leptos::*;

#[must_use]
#[component]
pub fn HeadInfo() -> impl IntoView {
    view! {
        <div class="flex items-center justify-center w-full h-6 bg-[#10e090] text-xs md:text-sm font-bold space-x-6">
            <p>"Free Shipping from 90€ 📦"</p>
            <p class="hidden sm:block">"Based in Germany 🇩🇪"</p>
            <p class="hidden sm:block" class="italic">
                "Supercharged Moto Shop 🚀"
            </p>
        </div>
    }
}
