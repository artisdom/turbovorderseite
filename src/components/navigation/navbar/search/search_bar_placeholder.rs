use leptos::*;

#[must_use]
#[component]
pub fn SearchBarPlaceholder() -> impl IntoView {
    view! {}
}
