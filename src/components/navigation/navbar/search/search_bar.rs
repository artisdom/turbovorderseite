use leptos_router::Form;

use leptos::*;
use leptos_meilisearch::SingleSearch;
use leptos_router::{use_location, use_navigate, use_query_map, NavigateOptions, State};

use model::MeilisearchProduct;
use rand::Rng;

const PLACEHOLDER: [&str; 12] = [
    "Spiegel",
    "Auspuffanlagen",
    "Kennzeichenhalter",
    "Reifen",
    "Schlagschrauber",
    "Getriebeöl",
    "Zündkerze",
    "Kurbelwelle",
    "Zylinder",
    "Felgenwerkzeug",
    "Abdeckplane",
    "Motoröl",
];

#[must_use]
#[component]
pub fn SearchBar() -> impl IntoView {
    let location = use_location();
    view! {
        <div class="relative w-full text-gray-600 md:mx-4">
            <Show
                when=move || !location.pathname.get().eq("/products")
                fallback=move || {
                    view! {
                        <Bar locked=false/>
                        <div class="absolute right-0 top-0 mt-[0.5rem] mr-4">
                            <svg
                                class="w-6 h-6 text-[var(--primary-color)] fill-current"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                            >
                                <path
                                    stroke="currentColor"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M15 13.9a5 5 0 0 1 2 4V21M7 3v3.2A5 5 0 0 0 8.9 10M17 3v3.2a5 5 0 0 1-2.4 4.3l-5.2 3A5 5 0 0 0 7 17.8V21M7 5h10M7.4 8h9.3M8 16h8.7M7 19h10"
                                ></path>
                            </svg>
                        </div>
                    }
                }
            >

                <Form action="/products" method="get">
                    <Bar locked=true/>
                    <button
                        type="submit"
                        class="absolute right-0 top-0 mt-[0.7rem] mr-4"
                        aria-label="search"
                    >
                        <svg
                            class="text-[var(--primary-color)] fill-current"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            version="1.1"
                            id="Capa_1"
                            x="0px"
                            y="0px"
                            viewBox="0 0 56.966 56.966"
                            xmlSpace="preserve"
                            width="18px"
                            height="18px"
                        >
                            <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z"></path>
                        </svg>
                    </button>
                </Form>
            </Show>
        </div>
    }
}
#[must_use]
#[component]
fn Bar(locked: bool) -> impl IntoView {
    let products = expect_context::<SingleSearch<MeilisearchProduct>>();
    let placeholder = create_rw_signal(String::new());

    create_isomorphic_effect(move |_| {
        let mut rng = rand::thread_rng();
        let word = PLACEHOLDER[rng.gen_range(0..PLACEHOLDER.len())];
        placeholder.set(word.to_string());
    });

    view! {
        <div class="flex focus:ring-4">
            // <CategorySelect/>
            <input
                class="bg-white border-[var(--primary-color)] w-full h-10 px-5 rounded-full font-rota text-md dark:bg-white"
                type="search"
                name="search"
                placeholder=move || placeholder.get()
                value=move || use_query_map().with(|query| query.get("search").cloned())
                on:input=move |ev| {
                    if !locked {
                        let navigate = use_navigate();
                        let ev_target = event_target_value(&ev);
                        products
                            .search_query()
                            .update({
                                let ev_target = ev_target.clone();
                                move |search_query| {
                                    search_query.query = Some(ev_target);
                                }
                            });
                        navigate(
                            &format!("/products?search={ev_target}"),
                            NavigateOptions {
                                resolve: false,
                                replace: true,
                                scroll: true,
                                state: State(None),
                            },
                        );
                    }
                }
            />
        </div>
    }
}
