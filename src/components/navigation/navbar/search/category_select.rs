use std::collections::BTreeSet;

use leptos::*;
use leptos_meilisearch::{AuthParameters, SearchQueryBuilder, Selectors, SingleSearch};
use model::MeilisearchProduct;

use crate::contexts::configuration_context::ConfigurationContext;

#[must_use]
#[component]
pub fn CategorySelect() -> impl IntoView {
    // let search_context = expect_context::<SearchContext>();
    let configuration_context = expect_context::<ConfigurationContext>();
    let app_environment = configuration_context.get_app_environment();

    let categories_signal = create_rw_signal(BTreeSet::new());

    // let select_el = create_node_ref::<Select>();

    let once_search = SingleSearch::<MeilisearchProduct>::create(
        AuthParameters {
            host: app_environment.meilisearch_endpoint,
            api_key: Some(app_environment.meilisearch_api_key),
        },
        "products",
        &SearchQueryBuilder::default()
            .limit(0_usize)
            .hits_per_page(0_usize)
            .facets(Selectors::All),
    )
    .expect("unable to init meili single-search");

    create_effect(move |_| {
        if let Some(facets) = once_search.facet_distribution() {
            if let Some((_, categories)) = facets
                .into_iter()
                .find(|(facet_name, _)| facet_name.eq_ignore_ascii_case("categories.lvl0"))
            {
                categories_signal.set(categories.keys().cloned().collect::<BTreeSet<_>>());
            }
        }
    });

    // let category_text_width = move || {
    //     get_text_width(
    //         search_context.get_category().unwrap_or_default(),
    //         "Montserrat".to_owned(),
    //         12,
    //     )
    // };

    view! {
        // <select _ref=select_el style=move || format!("width:{}px", category_text_width()) on:change=move |ev| search_context.set_category(event_target_value(&ev)) id="dropdown-button" data-dropdown-toggle="dropdown" class="flex-shrink-0 z-10 inline-flex items-center h-10 px-4 text-sm font-medium text-center text-gray-900 bg-white border border-gray-300 rounded-s-full hover:bg-gray-200 dark:border-gray-600" type="button">
        //     <option value="">
        //         "Alle"
        //     </option>
        //     {move || categories_signal.get().iter().map(|category| view! {
        //         <option>
        //             {category}
        //         </option>
        //     }).collect_view()}
        //     <svg class="w-2.5 h-2.5 ms-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
        //         <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 4 4 4-4"/>
        //     </svg>
        // </select>
    }
}
