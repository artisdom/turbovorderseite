use leptos::*;
use leptos_router::A;

const CATEGORIES: [(&str, &str); 6] = [
    ("Zylinder", "lvl0/zylinder"),
    ("Auspuffanlagen", "lvl0/auspuffanlagen"),
    ("Fahreraustattung", "lvl0/fahreraustattung"),
    ("Freizeit", "lvl0/streetwear"),
    ("Werkstatt", "lvl0/werkstatt"),
    ("Customizing", "lvl0/styling parts"),
];

#[must_use]
#[component]
pub fn CategoryItems() -> impl IntoView {
    CATEGORIES
        .map(|item| {
            view! {
                <li class="flex items-center">
                    <A
                        href=format!("/category/{}", item.1)
                        class="hover:text-gray-800 ease-in-out duration-200"
                    >
                        {item.0}
                    </A>
                </li>
            }
        })
        .collect_view()
}
