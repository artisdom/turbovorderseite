use leptos::*;
use leptos_router::A;

use super::category_items::CategoryItems;

#[must_use]
#[component]
pub fn CategoryBar() -> impl IntoView {
    view! {
        <div class="hidden lg:block w-full bg-[var(--primary-color)]">
            <div class="text-white font-semibold text-base flex items-center justify-center">
                <div class="flex items-center justify-center">
                    <div class="bg-transparent px-10 py-1.5 uppercase">
                        <ul class="flex flex-wrap items-center space-x-6 md:space-x-5 uppercase">
                            <CategoryItems/>
                        </ul>
                    </div>
                    <div class="hidden xl:block md:ml-2">
                        <ul class="px-10 py-1.5 flex flex-wrap items-center space-x-6 uppercase">
                            <li>
                                <A
                                    href="/support"
                                    class="hover:text-gray-800 ease-in-out duration-200"
                                >
                                    "Support"
                                </A>
                            </li>
                            <li>
                                <A
                                    href="/shipping"
                                    class="hover:text-gray-800 ease-in-out duration-200"
                                >
                                    "Versand"
                                </A>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    }
}
