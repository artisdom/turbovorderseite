use leptos::*;
use leptos_oidc::Auth;
use leptos_router::A;

use crate::{authentication::UserClaims, contexts::configuration_context::ConfigurationContext};

#[must_use]
#[component]
pub fn ProfileButton() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let auth = expect_context::<Auth>();
    let get_token_data = move || {
        auth.decoded_id_token::<UserClaims>(
            jsonwebtoken::Algorithm::EdDSA,
            &[&configuration_context.get_app_environment().auth_audience],
        )
    };

    view! {
        <div class="ml-3">
            {move || match get_token_data().flatten() {
                Some(token_data) => {
                    let claims = token_data.claims;
                    view! {
                        <A
                            href="/profile"
                            class="flex w-max text-md text-white font-rota items-center space-x-1.5 xl:border border-white rounded-lg p-1.5 font-raleway"
                        >
                            <UserIcon/>
                            <p class="hidden xl:block mt-0.5 font-semibold">
                                {format!("Hallo {}", claims.given_name.clone())}
                            </p>
                        </A>
                    }
                        .into_view()
                }
                None => view! { <LoginButton/> }.into_view(),
            }}

        </div>
    }
}

#[component]
fn LoginButton() -> impl IntoView {
    view! {
        <a
            href={
                let auth = expect_context::<Auth>();
                move || auth.login_url()
            }

            class="hidden lg:flex items-center mx-2 text-white bg-[var(--primary-color)] hover:bg-[var(--primary-color)]/0.8 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700"
        >
            "Anmelden"
            <ArrowIcon/>
        </a>
        <a
            href={
                let auth = expect_context::<Auth>();
                move || auth.login_url()
            }

            class="lg:hidden"
        >
            <UserIcon/>
        </a>
    }
}

#[component]
fn ArrowIcon() -> impl IntoView {
    view! {
        <svg
            class="w-3.5 h-3.5 ml-2"
            role="button"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 10"
        >
            <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M1 5h12m0 0L9 1m4 4L9 9"
            ></path>
        </svg>
    }
}

#[component]
fn UserIcon() -> impl IntoView {
    view! {
        <svg
            class="w-7 h-7 text-gray-800 dark:text-white"
            aria-hidden="true"
            aria-label="profile"
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
            viewBox="0 0 20 20"
        >
            <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z"></path>
        </svg>
    }
}
