use crate::{
    components::orderflow::cart::minicart::minicart_menu::MiniCartMenu,
    contexts::cart_context::CartContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn CartButton() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    let amount_resource = create_local_resource(
        move || cart_context.get_item_count(),
        move |it| async move { it },
    );

    view! {
        <button
            type="button"
            aria-label="minicart"
            class="relative flex items-center text-white hover:animate-shake"
            on:click=move |_| cart_context.set_collapse()
        >
            <svg
                class="fill-white"
                xmlns="http://www.w3.org/2000/svg"
                width="30"
                height="30"
                viewBox="0 0 24 24"
            >
                <path d="M10 19.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5zm3.5-1.5c-.828 0-1.5.671-1.5 1.5s.672 1.5 1.5 1.5 1.5-.671 1.5-1.5c0-.828-.672-1.5-1.5-1.5zm1.336-5l1.977-7h-16.813l2.938 7h11.898zm4.969-10l-3.432 12h-12.597l.839 2h13.239l3.474-12h1.929l.743-2h-4.195z"></path>
            </svg>
            <Show when=move || { amount_resource.get().unwrap_or_default() > 0 }>
                <div class="flex items-center justify-center animate-float w-6 absolute bottom-5 mt-2.5 text-md">
                    <p class="text-white font-bold">
                        {move || amount_resource.get().unwrap_or_default()}
                    </p>
                </div>
            </Show>
        </button>
        <Show when=move || { cart_context.is_collapsed() }>
            <MiniCartMenu/>
        </Show>
    }
}
