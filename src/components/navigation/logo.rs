use leptos::*;
use leptos_router::A;

#[must_use]
#[component]
pub fn Logo() -> impl IntoView {
    view! {
        <A href="/">
            <img class="h-[3rem]" src="/static/logo/logo_white.svg" alt="moturbo logo"/>
        </A>
    }
}
