use leptos::*;
use leptos_router::A;

#[must_use]
#[component]
pub fn FooterElement(title: String, path: String) -> impl IntoView {
    let is_http = !title.contains("http");
    let is_ext_app = !title.contains("moturbo.com");
    view! {
        <li class="mb-2">
            {if is_http {
                if is_ext_app {
                    view! {
                        <a rel="external" href=path class="hover:underline">
                            {title}
                        </a>
                    }
                        .into_view()
                } else {
                    view! {
                        <a href=path class="hover:underline">
                            {title}
                        </a>
                    }
                        .into_view()
                }
            } else {
                view! {
                    <A href=path class="hover:underline">
                        {title}
                    </A>
                }
                    .into_view()
            }}

        </li>
    }
}
