use crate::components::{
    navigation::{footer::footer_element::FooterElement, logo::Logo},
    product::site::product_detail_payment_methods::ProductDetailPaymentMethods,
    standards::trapezoid_seperator::{Position, TrapezoidSeperator},
};

use leptos::*;

pub mod footer_element;

const FAQ_LIST: [(&str, &str); 3] = [
    ("Lieferung", "/shipping"),
    ("Reklamation", "/retoure"),
    ("Kontakt & Support", "/support"),
];
const WE_LIST: [(&str, &str); 2] = [
    ("Status", "https://status.moturbo.com/"),
    ("Quellcode", "/source"),
];
const LEGAL_LIST: [(&str, &str); 6] = [
    ("AGB", "/terms"),
    ("Datenschutzerklärung", "/privacy"),
    ("Widerruf", "/revocation"),
    ("Zahlung und Versand", "/payment"),
    ("Batteriehinweise", "/battery"),
    ("Impressum", "/imprint"),
];

#[must_use]
#[allow(clippy::too_many_lines)]
#[component]
pub fn Footer() -> impl IntoView {
    view! {
        <footer class="bg-[var(--secondary-color)] dark:bg-[var(--secondary-color)] mt-auto mb-0">
            <div class="mb-8">
                <TrapezoidSeperator position=Position::Top/>
            </div>
            <div class="mx-auto w-full md:max-w-[84rem] px-6 md:px-0 lg:pb-8">
                <div class="md:flex md:justify-between md:space-x-12 space-x-0">
                    <div class="mb-6 md:mb-0">
                        <div class="mb-2 md:mb-1.5 me-3">
                            <Logo/>
                        </div>
                        <ProductDetailPaymentMethods/>
                        <div class="mt-[1rem] md:mt-[2rem]">
                            <a
                                href="https://logo.haendlerbund.de/show.php?uuid=faf1fbb3-0315-11ef-a43e-0242ac130003-6445088392"
                                target="_blank"
                            >
                                <img
                                    src="https://logo.haendlerbund.de/logo.php?uuid=faf1fbb3-0315-11ef-a43e-0242ac130003-6445088392&size=120&variant=1"
                                    title="Händlerbund Mitglied"
                                    alt="Mitglied im Händlerbund"
                                    border="0"
                                />
                            </a>
                        </div>
                    </div>

                    <div class="flex gap-[1.5rem] md:gap-[1.5rem] flex-wrap max-w-[40rem] sm:gap-6">
                        <FooterSection
                            title="FAQ".to_string()
                            list=FAQ_LIST
                                .to_vec()
                                .iter()
                                .map(|(title, path)| ((*title).to_string(), (*path).to_string()))
                                .collect()
                        />
                        <FooterSection
                            title="MOTURBO".to_string()
                            list=WE_LIST
                                .to_vec()
                                .iter()
                                .map(|(title, path)| ((*title).to_string(), (*path).to_string()))
                                .collect()
                        />
                        <FooterSection
                            title="RECHTLICHES".to_string()
                            list=LEGAL_LIST
                                .to_vec()
                                .iter()
                                .map(|(title, path)| ((*title).to_string(), (*path).to_string()))
                                .collect()
                        />
                    </div>
                </div>
                <hr class="my-6 border-gray-200 sm:mx-auto lg:my-8"/>
                <div class="sm:flex sm:items-center sm:justify-between">
                    <span class="text-sm text-gray-500 sm:text-center">
                        "© 2024 Moturbo UG (haftungsbeschränkt). All Rights Reserved."
                    </span>
                    <div class="flex mt-4 sm:justify-center sm:mt-0">
                        <a
                            href="https://discord.gg/aK8p5JCUuR"
                            class="text-[var(--primary-color)] hover:text-gray-500 ms-5"
                        >
                            <svg
                                class="w-6 h-6"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                viewBox="0 0 21 16"
                            >
                                <path d="M16.942 1.556a16.3 16.3 0 0 0-4.126-1.3 12.04 12.04 0 0 0-.529 1.1 15.175 15.175 0 0 0-4.573 0 11.585 11.585 0 0 0-.535-1.1 16.274 16.274 0 0 0-4.129 1.3A17.392 17.392 0 0 0 .182 13.218a15.785 15.785 0 0 0 4.963 2.521c.41-.564.773-1.16 1.084-1.785a10.63 10.63 0 0 1-1.706-.83c.143-.106.283-.217.418-.33a11.664 11.664 0 0 0 10.118 0c.137.113.277.224.418.33-.544.328-1.116.606-1.71.832a12.52 12.52 0 0 0 1.084 1.785 16.46 16.46 0 0 0 5.064-2.595 17.286 17.286 0 0 0-2.973-11.59ZM6.678 10.813a1.941 1.941 0 0 1-1.8-2.045 1.93 1.93 0 0 1 1.8-2.047 1.919 1.919 0 0 1 1.8 2.047 1.93 1.93 0 0 1-1.8 2.045Zm6.644 0a1.94 1.94 0 0 1-1.8-2.045 1.93 1.93 0 0 1 1.8-2.047 1.918 1.918 0 0 1 1.8 2.047 1.93 1.93 0 0 1-1.8 2.045Z"></path>
                            </svg>
                            <span class="sr-only">"Discord community"</span>
                        </a>
                        <a
                            href="https://www.instagram.com/moturbo_shop"
                            class="text-[var(--primary-color)] hover:text-gray-500 ms-5"
                        >
                            <svg
                                class="w-6 h-6"
                                fill="currentColor"
                                xmlns="http://www.w3.org/2000/svg" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 512 512"><path fill-rule="nonzero" d="M170.663 256.157c-.083-47.121 38.055-85.4 85.167-85.482 47.121-.092 85.407 38.029 85.499 85.159.091 47.13-38.047 85.4-85.176 85.492-47.112.09-85.399-38.039-85.49-85.169zm-46.108.092c.141 72.602 59.106 131.327 131.69 131.185 72.592-.14 131.35-59.089 131.209-131.691-.141-72.577-59.114-131.336-131.715-131.194-72.585.141-131.325 59.114-131.184 131.7zm237.104-137.092c.033 16.954 13.817 30.682 30.772 30.649 16.961-.034 30.689-13.811 30.664-30.765-.033-16.954-13.818-30.69-30.78-30.656-16.962.033-30.689 13.818-30.656 30.772zm-208.696 345.4c-24.958-1.086-38.511-5.234-47.543-8.709-11.961-4.628-20.496-10.177-29.479-19.093-8.966-8.951-14.532-17.461-19.202-29.397-3.508-9.033-7.73-22.569-8.9-47.527-1.269-26.983-1.559-35.078-1.683-103.433-.133-68.338.116-76.434 1.294-103.441 1.069-24.941 5.242-38.512 8.709-47.536 4.628-11.977 10.161-20.496 19.094-29.478 8.949-8.983 17.459-14.532 29.403-19.202 9.025-3.526 22.561-7.715 47.511-8.9 26.998-1.278 35.085-1.551 103.423-1.684 68.353-.133 76.448.108 103.456 1.294 24.94 1.086 38.51 5.217 47.527 8.709 11.968 4.628 20.503 10.145 29.478 19.094 8.974 8.95 14.54 17.443 19.21 29.413 3.524 8.999 7.714 22.552 8.892 47.494 1.285 26.998 1.576 35.094 1.7 103.432.132 68.355-.117 76.451-1.302 103.442-1.087 24.957-5.226 38.52-8.709 47.56-4.629 11.953-10.161 20.488-19.103 29.471-8.941 8.949-17.451 14.531-29.403 19.201-9.009 3.517-22.561 7.714-47.494 8.9-26.998 1.269-35.086 1.56-103.448 1.684-68.338.133-76.424-.124-103.431-1.294zM149.977 1.773c-27.239 1.286-45.843 5.648-62.101 12.019-16.829 6.561-31.095 15.353-45.286 29.603C28.381 57.653 19.655 71.944 13.144 88.79c-6.303 16.299-10.575 34.912-11.778 62.168C.172 178.264-.102 186.973.031 256.489c.133 69.508.439 78.234 1.741 105.548 1.302 27.231 5.649 45.827 12.019 62.092 6.569 16.83 15.353 31.089 29.611 45.289 14.25 14.2 28.55 22.918 45.404 29.438 16.282 6.294 34.902 10.583 62.15 11.777 27.305 1.203 36.022 1.468 105.521 1.336 69.532-.133 78.25-.44 105.555-1.734 27.239-1.302 45.826-5.664 62.1-12.019 16.829-6.585 31.095-15.353 45.288-29.611 14.191-14.251 22.917-28.55 29.428-45.404 6.304-16.282 10.592-34.904 11.777-62.134 1.195-27.323 1.478-36.049 1.344-105.557-.133-69.516-.447-78.225-1.741-105.522-1.294-27.256-5.657-45.844-12.019-62.118-6.577-16.829-15.352-31.08-29.602-45.288-14.25-14.192-28.55-22.935-45.404-29.429-16.29-6.304-34.903-10.6-62.15-11.778C333.747.164 325.03-.101 255.506.031c-69.507.133-78.224.431-105.529 1.742z"/>
                            </svg>
                            <span class="sr-only">"Instagram"</span>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    }
}

#[allow(clippy::needless_pass_by_value)]
#[component]
fn FooterSection(title: String, list: Vec<(String, String)>) -> impl IntoView {
    view! {
        <div class="w-36 lg:w-48">
            <h2 class="mb-6 text-sm font-semibold text-gray-200 uppercase">{title}</h2>
            <ul class="text-gray-400 font-medium">
                {list
                    .into_iter()
                    .map(|(title, path)| {
                        view! {
                            <FooterElement title=(*title).to_string() path=(*path).to_string()/>
                        }
                    })
                    .collect_view()}
            </ul>
        </div>
    }
}
