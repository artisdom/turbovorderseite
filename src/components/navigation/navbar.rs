use crate::components::standards::trapezoid_seperator::{Position, TrapezoidSeperator};

use super::{
    logo::Logo,
    navbar::{
        buttons::{
            cart_button::CartButton, dark_button::DarkButton, profile_button::ProfileButton,
            wishlist_button::WishlistButton,
        },
        category::category_bar::CategoryBar,
        head_info::HeadInfo,
        search::search_bar::SearchBar,
    },
};

use leptos::*;

pub mod buttons;
pub mod category;
pub mod head_info;
pub mod search;

#[must_use]
#[component]
pub fn Navbar() -> impl IntoView {
    view! {
        <div class="bg-[var(--secondary-color)] pb-2">
            <div class="">
                <HeadInfo/>
            </div>
            <div class="flex flex-wrap justify-center place-items">
                <nav class="flex justify-between items-center max-w-[84rem] w-full mt-2 p-4 md:p-2">
                    <Logo/>
                    <div class="flex items-center justify-end w-full">
                        <div class="hidden md:flex w-full mx-1">
                            <SearchBar/>
                        </div>
                        <div class="flex items-center justify-center">
                            <CartButton/>
                            <WishlistButton/>
                            <ProfileButton/>
                            <DarkButton/>
                        </div>
                    </div>
                </nav>
                <div class="md:hidden w-full pr-4 pl-4 pb-6">
                    <SearchBar/>
                </div>
            </div>
        </div>
        <div class="md:hidden">
            <TrapezoidSeperator position=Position::Down/>
        </div>
        <CategoryBar/>
    }
}
