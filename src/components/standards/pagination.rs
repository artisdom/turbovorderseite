use leptos::*;

#[must_use]
#[component]
pub fn Pagination(page_signal: RwSignal<usize>, total_pages: RwSignal<usize>) -> impl IntoView {
    create_effect(move |_| {
        page_signal.track();
        let mut options = web_sys::ScrollToOptions::new();
        options.top(0.0);
        web_sys::window()
            .expect("Cannot bind window")
            .scroll_to_with_scroll_to_options(&options);
    });

    view! {
        <div class="flex flex-col items-center my-4">
            <span class="text-sm text-gray-700 dark:text-gray-400">
                "Seite "
                <span class="font-semibold text-gray-900 dark:text-white">
                    {move || page_signal.get()}
                </span> " von "
                <span class="font-semibold text-gray-900 dark:text-white">
                    {move || total_pages}
                </span> " Seiten"
            </span>
            <div class="inline-flex mt-2 xs:mt-0">
                <Show
                    when=move || (page_signal.get() > 1)
                    fallback=move || {
                        view! { <BeforePage page_signal=page_signal.clone() disabled=true/> }
                    }
                >
                    <BeforePage page_signal=page_signal disabled=false/>
                </Show>
                <Show
                    when=move || (page_signal.get() < total_pages.get())
                    fallback=move || {
                        view! { <NextPage page_signal=page_signal.clone() disabled=true/> }
                    }
                >
                    <NextPage page_signal=page_signal disabled=false/>
                </Show>
            </div>
        </div>
    }
}

#[must_use]
#[component]
pub fn BeforePage(page_signal: RwSignal<usize>, disabled: bool) -> impl IntoView {
    if disabled {
        view! {
            <button
                on:click=move |_| page_signal.update(|upd| *upd -= 1)
                class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-400 bg-gray-900 rounded-s dark:bg-[var(--secondary-color)] dark:border-gray-800 dark:text-gray-600"
                disabled
            >
                <svg
                    class="w-3.5 h-3.5 me-2 rtl:rotate-180"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M13 5H1m0 0 4 4M1 5l4-4"
                    ></path>
                </svg>
                "Zurück"
            </button>
        }
    } else {
        view! {
            <button
                on:click=move |_| page_signal.update(|upd| *upd -= 1)
                class="flex items-center justify-center px-4 h-10 text-base font-medium text-white bg-gray-800 rounded-s hover:bg-gray-900 dark:bg-[var(--secondary-color)] dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            >
                <svg
                    class="w-3.5 h-3.5 me-2 rtl:rotate-180"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M13 5H1m0 0 4 4M1 5l4-4"
                    ></path>
                </svg>
                "Zurück"
            </button>
        }
    }
}

#[must_use]
#[component]
pub fn NextPage(page_signal: RwSignal<usize>, disabled: bool) -> impl IntoView {
    if disabled {
        view! {
            <button
                on:click=move |_| page_signal.update(|upd| *upd += 1)
                class="flex items-center justify-center px-4 h-10 text-base font-medium text-gray-400 bg-gray-900 rounded-e dark:bg-[var(--secondary-color)] dark:border-gray-800 dark:text-gray-600"
                disabled
            >
                "Nächste"
                <svg
                    class="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M1 5h12m0 0L9 1m4 4L9 9"
                    ></path>
                </svg>
            </button>
        }
    } else {
        view! {
            <button
                on:click=move |_| page_signal.update(|upd| *upd += 1)
                class="flex items-center justify-center px-4 h-10 text-base font-medium text-white bg-gray-800 rounded-e hover:bg-gray-900 dark:bg-[var(--secondary-color)] dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            >
                "Nächste"
                <svg
                    class="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M1 5h12m0 0L9 1m4 4L9 9"
                    ></path>
                </svg>
            </button>
        }
    }
}
