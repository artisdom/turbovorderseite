use leptos::*;

#[derive(Clone)]
pub enum Position {
    Top,
    Down,
}

#[must_use]
#[component]
pub fn TrapezoidSeperator(position: Position) -> impl IntoView {
    let offset = if matches!(position, Position::Top) {
        "-50%"
    } else {
        "-42%"
    };
    view! {
        <div class="relative">
            <div
                class="flex justify-center items-center z-10"
                style=move || { format!("transform:translateY({})", offset) }
            >

                <svg class="w-[11px] h-[22px]">
                    <polygon
                        class="fill-white md:fill-gray-100 dark:fill-[var(--dark-background-color)]"
                        points="0,11 11,0 11,22"
                    ></polygon>
                </svg>
                <div class="bg-white md:bg-gray-100 dark:bg-[var(--dark-background-color)] w-3/4 md:w-full max-w-[84rem] h-[22px]"></div>
                <svg class="w-[11px] h-[22px]">
                    <polygon
                        class="fill-white md:fill-gray-100 dark:fill-[var(--dark-background-color)]"
                        points="0,0 11,11 0,22"
                    ></polygon>
                </svg>
            </div>
        </div>
    }
}
