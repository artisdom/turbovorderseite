use leptos::*;
use leptos_meta::Title;

#[must_use]
#[component]
pub fn FooterPageTemplate(page_name: &'static str, children: ChildrenFn) -> impl IntoView {
    view! {
        <Title text=page_name/>
        <div class="flex justify-center">
            <div class="text-lg my-20 dark:text-white w-[1150px]">
                <p class="my-4 text-3xl md:text-6xl tracking-tight font-extrabold text-gray-900 dark:text-white">
                    {page_name}
                </p>
                <div class="mt-4 md:mt-14 space-y-2">{children}</div>
            </div>
        </div>
    }
}
