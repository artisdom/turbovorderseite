use leptos::*;
use url::Url;

use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    contexts::configuration_context::ConfigurationContext,
};

#[must_use]
#[component]
pub fn SourceArticleTemplate(
    name: String,
    description: String,
    image_url: String,
    is_open_source: bool,
    is_active: bool,
    #[prop(optional)] repository_link: String,
) -> impl IntoView {
    let cdn = expect_context::<ContentProvider>();
    let opt_url = cdn.provide_content_network(
        Url::parse(&image_url),
        &CdnOptions {
            quality: Some(90),
            size: Some(355),
            crop_width: Some(355),
            crop_height: Some(190),
            crop_x: Some(0),
            crop_y: Some(10),
        },
        crate::cdn::CdnPullType::Image,
    );

    if repository_link.is_empty() {
        view! {
            <div class="p-4 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-[var(--secondary-color)] dark:border-gray-700 lg:transform lg:duration-300 lg:hover:scale-105">
                <InnerArticle
                    name=name
                    description=description
                    image_url=opt_url.to_string()
                    is_active=is_active
                    is_open_source=is_open_source
                />
            </div>
        }.into_view()
    } else {
        view! {
            <a
                href=repository_link
                target="_blank"
                class="p-4 bg-white rounded-lg border border-gray-200 shadow-md dark:bg-[var(--secondary-color)] dark:border-gray-700 lg:transform lg:duration-300 lg:hover:scale-105"
            >
                <InnerArticle
                    name=name
                    description=description
                    image_url=opt_url.to_string()
                    is_active=is_active
                    is_open_source=is_open_source
                />
            </a>
        }.into_view()
    }
}

#[component]
fn InnerArticle(
    name: String,
    description: String,
    image_url: String,
    is_active: bool,
    is_open_source: bool,
) -> impl IntoView {
    let cdn_context = expect_context::<ContentProvider>();
    let configuration_context = expect_context::<ConfigurationContext>();
    let gitlab_opt_url = cdn_context.provide_content_network(
        Url::parse(&format!(
            "{}/static/source-images/gitlab-logo-500.png",
            configuration_context.get_app_environment().app_deployment
        )),
        &CdnOptions {
            quality: Some(100),
            size: Some(64),
            crop_width: None,
            crop_height: None,
            crop_x: None,
            crop_y: None,
        },
        CdnPullType::Image,
    );

    view! {
        <img
            class="mb-5 rounded-lg"
            width="355"
            height="190"
            src=image_url
            alt="rusty industry turbine"
        />
        <span class="bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900">
            {if is_active { "Online" } else { "Offline" }}

        </span>
        <h2 class="my-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{name}</h2>
        <p class="mb-4 font-light text-gray-500 dark:text-gray-400">{description}</p>
        <div class="flex items-center space-x-4">
            <img class="w-10 h-10 rounded-full" src=gitlab_opt_url.to_string() alt="gitlab logo"/>
            <div class="font-medium dark:text-white">
                <p>"Gitlab Repository"</p>
                <div class="text-sm font-normal text-gray-500 dark:text-gray-400">
                    {if is_open_source { "Open Source" } else { "Closed Source" }}

                </div>
            </div>
        </div>
    }
}
