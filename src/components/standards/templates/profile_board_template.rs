use leptos::*;
use leptos_meta::Title;

#[must_use]
#[component]
pub fn ProfileBoardTemplate(page_name: String, children: ChildrenFn) -> impl IntoView {
    view! {
        <Title text=page_name.clone()/>
        <div class="flex flex-col h-full">
            <p class="text-2xl font-bold">{page_name}</p>
            <hr class="mt-2 mb-4 border border-[var(--primary-color)]"/>
            {children}
        </div>
    }
}
