#![allow(clippy::needless_pass_by_value)]

use leptos::*;

#[component]
pub fn AvailableIndicator(children: ChildrenFn) -> impl IntoView {
    view! {
        <span class="inline-flex items-center bg-green-100 text-green-800 text-sm font-medium px-2.5 py-0.5 rounded-full dark:bg-green-900 dark:text-green-300">
            <span class="relative flex h-2 w-2 mr-1">
                <span class="absolute animate-ping w-2 h-2 me-1 bg-green-500 rounded-full"></span>
                <span class="absolute w-2 h-2 me-1 bg-green-500 rounded-full"></span>
            </span>
            {children()}
        </span>
    }
}

#[component]
pub fn UnavailableIndicator(children: ChildrenFn) -> impl IntoView {
    view! {
        <span class="inline-flex items-center bg-red-100 text-red-800 text-sm font-medium px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300">
            <span class="w-2 h-2 me-1 bg-red-500 rounded-full"></span>
            {children()}
        </span>
    }
}
