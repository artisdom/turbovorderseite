use leptos::*;

use crate::legal::{get_legal_document, LegalDocument};

#[must_use]
#[component]
pub fn LegalLoader(legal_document: LegalDocument) -> impl IntoView {
    let legal_resource = create_blocking_resource(
        || (),
        move |_| async move {
            get_legal_document(legal_document)
                .await
                .expect("Cannot load legal text")
        },
    );
    view! {
        <Suspense>
            <div
                class="text-xl"
                inner_html=move || legal_resource.try_get().unwrap_or_default()
            ></div>
        </Suspense>
    }
}
