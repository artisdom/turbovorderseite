use crate::contexts::profile_context::ProfileContext;

use super::site::menu::{
    profile_menu_head::ProfileMenuHead, profile_menu_list::ProfileMenuList,
    profile_mobile_toggle::ProfileMobileToggle,
};
use leptos::*;
use leptos_use::use_media_query;

#[must_use]
#[component]
pub fn ProfileMenu(fullname: String) -> impl IntoView {
    let medium_screen_signal = use_media_query("(min-width: 768px)");

    create_local_resource(
        move || medium_screen_signal.get(),
        move |is_medium_screen| async move {
            let profile_signal = expect_context::<ProfileContext>();
            profile_signal.set_menu_collapse(is_medium_screen);
        },
    );

    view! {
        <div class="w-full md:w-64 bg-[var(--primary-color)] shadow text-gray-300 rounded-xl">
            <div class="p-4 flex justify-between items-center">
                <ProfileMenuHead fullname=fullname/>
                <ProfileMobileToggle/>
            </div>
            <Show when={
                let profile_signal = expect_context::<ProfileContext>();
                move || profile_signal.is_menu_collapsed()
            }>
                <ProfileMenuList/>
            </Show>
        </div>
    }
}
