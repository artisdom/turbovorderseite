use super::site::boards::{
    profile_invoices_board::InvoicesBoard, profile_orders_board::OrdersBoard,
    profile_overview_board::OverviewBoard, profile_retoure_board::RetoureBoard,
};
use crate::{
    authentication::UserClaims,
    components::profile::site::boards::profile_order_detail_board::OrderDetailBoard,
    contexts::profile_context::{ProfileBoard, ProfileContext},
};

use leptos::*;

#[must_use]
#[component]
pub fn ProfileBoard(claims: UserClaims) -> impl IntoView {
    let profile_context = expect_context::<ProfileContext>();
    view! {
        <div class="h-full bg-white dark:bg-[var(--secondary-color)] grow w-full md:w-auto rounded-xl p-4 mt-2 md:mt-0 text-gray-700 dark:text-gray-300 shadow">
            {move || match profile_context.get_current_board() {
                ProfileBoard::Overview => {
                    view! { <OverviewBoard claims=claims.clone()/> }.into_view()
                }
                ProfileBoard::Orders => view! { <OrdersBoard/> }.into_view(),
                ProfileBoard::Invoices => view! { <InvoicesBoard/> }.into_view(),
                ProfileBoard::Retoure => view! { <RetoureBoard/> }.into_view(),
                ProfileBoard::OrderDetail => view! { <OrderDetailBoard/> }.into_view(),
            }}

        </div>
    }
}
