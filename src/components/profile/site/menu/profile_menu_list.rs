use crate::{
    components::profile::site::menu::profile_menu_list_element::ProfileMenuListElement,
    contexts::profile_context::ProfileBoard,
};
use leptos::*;

#[must_use]
#[component]
#[allow(clippy::too_many_lines)]
pub fn ProfileMenuList() -> impl IntoView {
    view! {
        <ul class="dark:space-y-0.5 text-sm text-gray-700 dark:text-gray-100 rounded-ee-xl">
            <ProfileMenuListElement board=ProfileBoard::Overview>
                <svg
                    class="w-5 h-5 text-gray-700 dark:text-gray-100"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 20 16"
                >
                    <path
                        class=format!("stroke-gray-900 dark:stroke-white")
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M13 1v14M7 1v14M2 1h16a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1Z"
                    ></path>
                </svg>
                <p class="mx-3">"Übersicht"</p>
            </ProfileMenuListElement>
            <ProfileMenuListElement board=ProfileBoard::Orders end=true>
                <svg
                    class="w-5 h-5 text-gray-700 dark:text-gray-100"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 18 20"
                >
                    <path
                        class=format!("stroke-gray-900 dark:stroke-white")
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M12 9V4a3 3 0 0 0-6 0v5m9.92 10H2.08a1 1 0 0 1-1-1.077L2 6h14l.917 11.923A1 1 0 0 1 15.92 19Z"
                    ></path>
                </svg>
                <p class="mx-3">"Meine Bestellungen"</p>
            </ProfileMenuListElement>
        // <ProfileMenuListElement board=ProfileBoard::Invoices>
        // <svg
        // class="w-5 h-5 text-gray-700 dark:text-gray-100"
        // aria-hidden="true"
        // xmlns="http://www.w3.org/2000/svg"
        // fill="none"
        // viewBox="0 0 16 20"
        // >
        // <path
        // class=format!("stroke-gray-900 dark:stroke-white")
        // strokeLinecap="round"
        // strokeLinejoin="round"
        // strokeWidth="2"
        // d="M6 1v4a1 1 0 0 1-1 1H1m8-2h3M9 7h3m-4 3v6m-4-3h8m3-11v16a.969.969 0 0 1-.932 1H1.934A.97.97 0 0 1 1 18V5.828a2 2 0 0 1 .586-1.414l2.828-2.828A2 2 0 0 1 5.829 1h8.239A.969.969 0 0 1 15 2ZM4 10h8v6H4v-6Z"
        // ></path>
        // </svg>
        // <p class="mx-3">"Alle Rechnungen"</p>
        // </ProfileMenuListElement>
        // <ProfileMenuListElement board=ProfileBoard::Retoure end=true>
        // <svg
        // class="w-5 h-5 text-gray-700 dark:text-gray-100"
        // aria-hidden="true"
        // xmlns="http://www.w3.org/2000/svg"
        // fill="none"
        // viewBox="0 0 12 16"
        // >
        // <path
        // class=format!("stroke-gray-900 dark:stroke-white")
        // strokeLinecap="round"
        // strokeLinejoin="round"
        // strokeWidth="2"
        // d="M1 1v14m8.336-.479-6.5-5.774a1 1 0 0 1 0-1.494l6.5-5.774A1 1 0 0 1 11 2.227v11.546a1 1 0 0 1-1.664.748Z"
        // ></path>
        // </svg>
        // <p class="mx-3">Reklamationen</p>
        // </ProfileMenuListElement>
        </ul>
    }
}
