use leptos::*;

#[must_use]
#[component]
pub fn ProfileMenuHead(fullname: String) -> impl IntoView {
    view! {
        <div class="flex items-center text-white">
            <svg
                class="w-8 h-8 dark:text-white"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 20"
            >
                <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M10 19a9 9 0 1 0 0-18 9 9 0 0 0 0 18Zm0 0a8.949 8.949 0 0 0 4.951-1.488A3.987 3.987 0 0 0 11 14H9a3.987 3.987 0 0 0-3.951 3.512A8.948 8.948 0 0 0 10 19Zm3-11a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
                ></path>
            </svg>
            <div class="mx-3">
                <p class="text-sm">Willkommen</p>
                <p class="text-lg font-bold">{fullname}</p>
            </div>
        </div>
    }
}
