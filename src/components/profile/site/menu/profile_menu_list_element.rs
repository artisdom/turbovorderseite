use leptos::*;

use crate::contexts::profile_context::{ProfileBoard, ProfileContext};

const FOCUSED: &str = "bg-gray-200 dark:opacity-60";
const UNFOCUSED: &str = "hover:opacity-70 bg-white dark:bg-[var(--secondary-color)]";

#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn ProfileMenuListElement(
    children: ChildrenFn,
    board: ProfileBoard,
    #[prop(default = false)] end: bool,
) -> impl IntoView {
    view! {
        <li class=move || {
            let profile_context = expect_context::<ProfileContext>();
            format!(
                "dark:bg-[var(--secondary-color)] transition duration-100 ease-in-out {} {}",
                if profile_context.get_current_board().eq(&board.clone()) {
                    FOCUSED
                } else {
                    UNFOCUSED
                },
                if end { "rounded-b-xl" } else { "" },
            )
        }>

            <button
                class="flex items-center p-4 w-full"
                on:click=move |_| {
                    let profile_context = expect_context::<ProfileContext>();
                    profile_context.set_current_board(board);
                }
            >

                {children}
            </button>
        </li>
    }
}
