use crate::contexts::profile_context::ProfileContext;

use leptos::*;

#[must_use]
#[component]
pub fn ProfileMobileToggle() -> impl IntoView {
    view! {
        <button
            class="block md:hidden mr-4"
            on:click=move |_| {
                let profile_context = expect_context::<ProfileContext>();
                profile_context.collapse_menu()
            }
        >

            <svg
                class="w-6 h-6 text-gray-200 dark:text-white"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 8"
            >
                <Show
                    when=move || {
                        let profile_context = expect_context::<ProfileContext>();
                        !profile_context.is_menu_collapsed()
                    }

                    fallback=|| {
                        view! {
                            <path
                                stroke="currentColor"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M13 7 7.674 1.3a.91.91 0 0 0-1.348 0L1 7"
                            ></path>
                        }
                    }
                >

                    <path
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="m1 1 5.326 5.7a.909.909 0 0 0 1.348 0L13 1"
                    ></path>
                </Show>
            </svg>
        </button>
    }
}
