pub mod orders_pagination;
pub mod profile_orders_board_item;

use self::orders_pagination::OrdersPagination;
use crate::{
    components::{
        profile::site::boards::profile_orders_board::profile_orders_board_item::OrdersBoardItem,
        standards::templates::profile_board_template::ProfileBoardTemplate,
    },
    contexts::order_context::OrderContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn OrdersBoard() -> impl IntoView {
    view! {
        <ProfileBoardTemplate page_name="Bestellungen".to_owned()>
            <OrderList/>
        </ProfileBoardTemplate>
    }
}

#[component]
#[allow(clippy::needless_pass_by_value)]
fn OrderList() -> impl IntoView {
    let order_context = expect_context::<OrderContext>();

    let orders_resource = create_local_resource(order_context.current_orders_page, {
        move |_| {
            let order = order_context.to_owned();
            async move { order.get_orders(25, "desc".to_owned(), false).await }
        }
    });

    move || match orders_resource.get() {
        Some(orders) => view! {
            <div class="flex flex-col justify-between h-full">
                <ul class="space-y-2">
                    <For
                        each=move || orders.clone()
                        key=|order| order.id.clone()
                        children=move |order| {
                            view! { <OrdersBoardItem order=order/> }
                        }
                    />

                </ul>
                <OrdersPagination/>
            </div>
        }
        .into_view(),
        None => view! { <p>"Lade Bestellungen..."</p> }.into_view(),
    }
}
