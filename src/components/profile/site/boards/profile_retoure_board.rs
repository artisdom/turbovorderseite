use leptos::*;

#[must_use]
#[component]
pub fn RetoureBoard() -> impl IntoView {
    view! {
        <div class="flex justify-between flex-col h-full">
            <div>
                <p class="text-2xl font-bold">"Orders"</p>
                <hr class="mt-2 border border-[var(--primary-color)]"/>
            </div>
        </div>
    }
}
