use crate::{
    authentication::UserClaims,
    components::standards::templates::profile_board_template::ProfileBoardTemplate,
};

use leptos::*;
use leptos_oidc::Auth;
use leptos_router::A;

#[must_use]
#[component]
pub fn OverviewBoard(claims: UserClaims) -> impl IntoView {
    let auth_context = expect_context::<Auth>();
    let is_admin = claims.groups.contains(&"administrator".to_owned());

    view! {
        <ProfileBoardTemplate page_name="Übersicht".to_owned()>
            <div class="flex flex-col justify-between h-full">
                <div class="text-gray-600 dark:text-gray-300">
                    <p class="text-sm">"Name"</p>
                    <p class="text-gray-800 dark:text-gray-400">
                        {format!("{} {}", claims.given_name, claims.family_name)}
                    </p>
                    <p class="mt-3 text-sm">"Email"</p>
                    <p class="text-gray-800 dark:text-gray-400">{claims.email.clone()}</p>
                    <p class="mt-3 text-sm">"Konto-Id"</p>
                    <p class="text-gray-800 dark:text-gray-400">{claims.sub.clone()}</p>
                </div>
                <div class="flex justify-end space-x-2 mt-3 md:mt-0">
                    <Show when=move || is_admin>
                        <A
                            href="/admin"
                            class="text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                        >
                            "Admin"
                        </A>
                    </Show>
                    <a
                        type="button"
                        href="mailto:support@moturbo.com"
                        class="text-white bg-gradient-to-r from-purple-500 via-purple-600 to-purple-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-purple-300 dark:focus:ring-purple-800 shadow-lg shadow-purple-500/50 dark:shadow-lg dark:shadow-purple-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                    >
                        <div class="flex items-center space-x-2">
                            <svg
                                class="w-4 h-4 text-white"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 20 20"
                            >
                                <path
                                    stroke="currentColor"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M4.333 6.764a3 3 0 1 1 3.141-5.023M2.5 16H1v-2a4 4 0 0 1 4-4m7.379-8.121a3 3 0 1 1 2.976 5M15 10a4 4 0 0 1 4 4v2h-1.761M13 7a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm-4 6h2a4 4 0 0 1 4 4v2H5v-2a4 4 0 0 1 4-4Z"
                                ></path>
                            </svg>
                            <p>"Support"</p>
                        </div>
                    </a>
                    <a
                        href=auth_context.logout_url()
                        class="text-white bg-gradient-to-r from-red-600 via-red-650 to-red-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-400 dark:focus:ring-red-800 shadow-lg shadow-red-500/50 dark:shadow-lg dark:shadow-red-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                    >
                        <div class="flex items-center space-x-2">
                            <svg
                                class="w-4 h-4 text-white"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 18 15"
                            >
                                <path
                                    stroke="currentColor"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M1 7.5h11m0 0L8 3.786M12 7.5l-4 3.714M12 1h3c.53 0 1.04.196 1.414.544.375.348.586.82.586 1.313v9.286c0 .492-.21.965-.586 1.313A2.081 2.081 0 0 1 15 14h-3"
                                ></path>
                            </svg>
                            <p>"Abmelden"</p>
                        </div>
                    </a>
                </div>
            </div>
        </ProfileBoardTemplate>
    }
}
