use leptos::*;

use crate::contexts::order_context::OrderContext;

#[must_use]
#[component]
pub fn OrdersPagination() -> impl IntoView {
    let order_context = expect_context::<OrderContext>();

    let order_page = order_context.get_page();
    let order_total_pages = order_context.get_total_pages();

    view! {
        <div class="flex flex-col items-center mb-2 mt-4 md:my-4">
            <span class="text-sm text-gray-700 dark:text-gray-400">
                "Seite "
                <span class="font-semibold text-gray-900 dark:text-white">
                    {move || order_page}
                </span> " von "
                <span class="font-semibold text-gray-900 dark:text-white">
                    {order_context.get_total_pages()}
                </span> " Seiten"
            </span>
            <div class="inline-flex mt-2 xs:mt-0">
                <Show when=move || (order_total_pages > 1)>
                    <button
                        on:click={
                            let order_context = expect_context::<OrderContext>();
                            move |_| order_context.decrement_page()
                        }

                        class="flex items-center justify-center px-4 h-10 text-base font-medium text-white bg-gray-800 rounded-s hover:bg-gray-900 dark:bg-[var(--secondary-color)] dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                    >
                        <svg
                            class="w-3.5 h-3.5 me-2 rtl:rotate-180"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 10"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M13 5H1m0 0 4 4M1 5l4-4"
                            ></path>
                        </svg>
                        "Zurück"
                    </button>
                </Show>
                <Show when=move || (order_page < order_total_pages)>
                    <button
                        on:click={
                            let order_context = expect_context::<OrderContext>();
                            move |_| order_context.increment_page()
                        }

                        class="flex items-center justify-center px-4 h-10 text-base font-medium text-white bg-gray-800 border-0 border-s border-gray-700 rounded-e hover:bg-gray-900 dark:bg-[var(--secondary-color)] dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
                    >
                        "Nächste"
                        <svg
                            class="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 10"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M1 5h12m0 0L9 1m4 4L9 9"
                            ></path>
                        </svg>
                    </button>
                </Show>
            </div>
        </div>
    }
}
