use leptos::*;
use model::Order;

use crate::{
    components::profile::order_status::OrderStatus, contexts::profile_context::ProfileContext,
    economy::ShopCurrency,
};

#[must_use]
#[component]
pub fn OrdersBoardItem(order: Order) -> impl IntoView {
    let profile = expect_context::<ProfileContext>();

    view! {
        <button
            on:click={
                let order = order.clone();
                move |_| { profile.set_order_board(order.to_owned()) }
            }

            class="w-full flex justify-between items-center p-4 dark:bg-gray-900 dark:text-white rounded-xl shadow"
        >
            <div class="flex items-center space-x-2 md:space-x-6">
                <b>{format!("#{}", order.id)}</b>
                <OrderStatus status=order.status/>
            </div>
            <p>{ShopCurrency::from(order.amount_total).to_string()}</p>
        </button>
    }
}
