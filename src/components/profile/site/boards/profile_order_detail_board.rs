use leptos::*;
use model::Order;

use crate::{
    components::{
        order::ordered_items_list::OrderedItemsList,
        standards::templates::profile_board_template::ProfileBoardTemplate,
    },
    contexts::profile_context::ProfileContext,
    economy::{format_rfc3339_to_date, ShopCurrency},
    order::format_status,
};

#[must_use]
#[component]
pub fn OrderDetailBoard() -> impl IntoView {
    let profile = expect_context::<ProfileContext>();

    move || match profile.get_focused_order() {
        Some(order) => view! {
            <ProfileBoardTemplate page_name=format!("Bestellung #{}", order.id)>
                <OrderDetails order=order.clone()/>
            </ProfileBoardTemplate>
        }
        .into_view(),
        None => view! {
            <ProfileBoardTemplate page_name="Fehler".to_owned()>
                <p>"Bestellung konnte nicht geladen werden"</p>
            </ProfileBoardTemplate>
        }
        .into_view(),
    }
}

#[must_use]
#[component]
pub fn OrderDetails(order: Order) -> impl IntoView {
    view! {
        <div class="space-y-4">
            <div class="flex flex-col lg:flex-row space-y-2 lg:space-y-0 lg:space-x-24">
                <div class="space-y-0 md:space-y-1">
                    <p class="text-lg font-bold">"Bestelldetails"</p>
                    <p>"Status: " <span class="font-bold">{format_status(&order.status)}</span></p>
                    <p>"Bestellnummer: " <span class="font-bold">{order.id}</span></p>
                    <p>
                        "Bestelldatum: "
                        <span class="font-bold">
                            {format_rfc3339_to_date(&order.created.clone())}
                        </span>
                    </p>
                    <p>
                        "Gesamtsumme: "
                        <span class="font-bold">
                            {ShopCurrency::from(order.amount_total).to_string()}
                        </span>
                    </p>
                </div>
                <div class="space-y-0 md:space-y-1">
                    <p class="text-lg font-bold">"Lieferinformationen"</p>
                    <p>{order.shipping_name}</p>
                    <p>{order.shipping_line1}</p>
                    <p>{order.shipping_line2}</p>
                    <p>{format!("{} {}", order.shipping_zip_code, order.shipping_city)}</p>
                    <p>{order.shipping_state}</p>
                    <p>{order.shipping_country}</p>
                </div>
            </div>
            <OrderedItemsList items=order.items/>
        </div>
    }
}
