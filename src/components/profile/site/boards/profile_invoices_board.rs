use leptos::*;

#[must_use]
#[component]
pub fn InvoicesBoard() -> impl IntoView {
    view! {
        <div class="flex justify-between flex-col h-full">
            <div>
                <p class="text-2xl font-bold">"Invoices"</p>
                <hr class="mt-2 border border-[var(--primary-color)]"/>
            </div>
        </div>
    }
}
