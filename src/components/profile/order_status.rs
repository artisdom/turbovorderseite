use leptos::*;
use model::OrderStatus;

#[allow(clippy::needless_pass_by_value)]
#[must_use]
#[component]
pub fn OrderStatus(status: OrderStatus) -> impl IntoView {
    match status {
        OrderStatus::Pending => view! {
            <span class="inline-flex items-center bg-orange-100 text-orange-800 text-xs md:text-sm font-medium px-1.5 md:px-2.5 py-0.5 rounded-full dark:bg-orange-900 dark:text-orange-300">
                "Zahlung ausstehend"
            </span>
        },
        OrderStatus::Processing => view! {
            <span class="inline-flex items-center bg-orange-100 text-orange-800 text-xs md:text-sm font-medium px-1.5 md:px-2.5 py-0.5 rounded-full dark:bg-orange-900 dark:text-orange-300">
                "In Bearbeitung"
            </span>
        },
        OrderStatus::Shipped => view! {
            <span class="inline-flex items-center bg-orange-100 text-orange-800 text-xs md:text-sm font-medium px-1.5 md:px-2.5 py-0.5 rounded-full dark:bg-orange-900 dark:text-orange-300">
                "Unterwegs"
            </span>
        },
        OrderStatus::Delivered => view! {
            <span class="inline-flex items-center bg-green-100 text-green-800 text-xs md:text-sm font-medium px-1.5 md:px-2.5 py-0.5 rounded-full dark:bg-green-900 dark:text-green-300">
                "Ausgeliefert"
            </span>
        },
        OrderStatus::Canceled => view! {
            <span class="inline-flex items-center bg-red-100 text-red-800 text-xs md:text-sm font-medium px-1.5 md:px-2.5 py-0.5 rounded-full dark:bg-red-900 dark:text-red-300">
                "Storniert"
            </span>
        },
    }
}
