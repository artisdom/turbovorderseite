use super::order_detail_list_item::OrderDetailsListItem;

use leptos::*;
use model::OrderItem;

#[must_use]
#[component]
pub fn OrderDetailList(order_items: Vec<OrderItem>) -> impl IntoView {
    view! {
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            "Menge"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Artikel-Nr."
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Produktname"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Rabatt"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Steuern"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Einzelpreis"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Gesamtpreis"
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <For
                        each=move || order_items.clone()
                        key=|order_item| order_item.product_id.clone()
                        children=|order_item| {
                            view! { <OrderDetailsListItem order_item=order_item/> }
                        }
                    />

                </tbody>
            </table>
        </div>
    }
}
