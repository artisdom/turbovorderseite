use leptos::*;
use model::OrderItem;

#[must_use]
#[component]
pub fn OrderDetailButtonCsv(order_id: String, order_items: Vec<OrderItem>) -> impl IntoView {
    let csv_parsed = order_items
        .iter()
        .map(|item| format!("{};{}\n", item.product_id, item.quantity))
        .collect::<String>();
    view! {
        <a
            href=format!("data:text/csv;charset=utf-8,{}", encode_as_uri(&csv_parsed))
            download=format!("{}.csv", order_id)
            class="text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
        >
            "Download as CSV"
        </a>
    }
}

fn encode_as_uri(text: &str) -> String {
    let mut encoded = String::new();
    for ch in text.chars() {
        if ch.is_ascii_alphanumeric() || "-_.!~*'()".contains(ch) {
            encoded.push(ch);
        } else {
            for byte in ch.to_string().as_bytes() {
                encoded.push_str(&format!("%{:02X}", byte));
            }
        }
    }
    encoded
}
