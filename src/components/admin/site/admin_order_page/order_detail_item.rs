use leptos::*;

#[must_use]
#[component]
pub fn OrderDetailItem(key: String, value: String) -> impl IntoView {
    view! {
        <tr class="bg-white dark:bg-[var(--secondary-color)]">
            <th
                scope="row"
                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
            >
                {key}
            </th>
            <td class="px-6 py-4">{value}</td>
        </tr>
    }
}
