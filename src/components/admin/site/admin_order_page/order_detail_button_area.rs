use crate::components::admin::site::admin_order_page::order_detail_button_csv::OrderDetailButtonCsv;

use leptos::*;
use model::OrderItem;

#[must_use]
#[component]
pub fn OrderDetailButtonArea(order_id: String, order_items: Vec<OrderItem>) -> impl IntoView {
    view! {
        <div class="mb-2 flex">
            <OrderDetailButtonCsv order_id=order_id order_items=order_items/>
        </div>
    }
}
