use leptos::*;

#[must_use]
#[component]
pub fn OrderDetailTable(children: ChildrenFn) -> impl IntoView {
    view! {
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg w-full">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <tbody>{children}</tbody>
            </table>
        </div>
    }
}
