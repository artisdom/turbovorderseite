use crate::{
    components::admin::site::{
        admin_order_page::{
            order_detail_item::OrderDetailItem, order_detail_table::OrderDetailTable,
        },
        boards::admin_orders_board::order_status_select::SelectOrderStatus,
    },
    economy::{format_rfc3339_to_date, ShopCurrency},
};

use leptos::*;
use model::Order;

#[must_use]
#[component]
pub fn OrderDetailsMatrix(order: Order) -> impl IntoView {
    let order_id = order.id.clone();
    let status_order = order.clone();

    view! {
        <div class="flex flex-col">
            <div class="flex space-x-2">
                <div class="flex flex-col space-y-2">
                    <div class="w-[24rem]">
                        <OrderDetailTable>
                            <OrderDetailItem
                                key="Identifikation".to_owned()
                                value=order_id.clone()
                            />
                            <tr class="bg-white dark:bg-[var(--secondary-color)]">
                                <th
                                    scope="row"
                                    class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                                >
                                    "Status"
                                </th>
                                <td class="px-6 py-4">
                                    <SelectOrderStatus order=status_order.to_owned()/>
                                </td>
                            </tr>
                        </OrderDetailTable>
                    </div>
                    <div class="w-[24rem]">
                        <OrderDetailTable>
                            <OrderDetailItem
                                key="Liefername".to_owned()
                                value=order.shipping_name.clone()
                            />
                            <OrderDetailItem
                                key="Address Linie 1".to_owned()
                                value=order.shipping_line1.clone()
                            />
                            {if let Some(line2) = order.shipping_line2.clone() {
                                view! {
                                    <OrderDetailItem key="Address Linie 2".to_owned() value=line2/>
                                }
                                    .into_view()
                            } else {
                                view! {}.into_view()
                            }}

                            <OrderDetailItem
                                key="Stadt".to_owned()
                                value=format!(
                                    "{} {}",
                                    order.shipping_zip_code.clone(),
                                    order.shipping_city.clone(),
                                )
                            />

                            <OrderDetailItem
                                key="Land".to_owned()
                                value=order.shipping_country.clone()
                            />
                        </OrderDetailTable>
                    </div>
                </div>
                <div class="flex flex-col space-y-2">
                    <div class="w-[20rem]">
                        <OrderDetailTable>
                            <OrderDetailItem
                                key="Rabatt".to_owned()
                                value=ShopCurrency::from(order.amount_discount).to_string()
                            />
                            <OrderDetailItem
                                key="Steuern".to_owned()
                                value=ShopCurrency::from(order.amount_tax).to_string()
                            />
                            <OrderDetailItem
                                key="Lieferkosten".to_owned()
                                value=ShopCurrency::from(order.shipping_fee).to_string()
                            />
                            <OrderDetailItem
                                key="Gesamtkosten".to_owned()
                                value=ShopCurrency::from(order.amount_total).to_string()
                            />
                        </OrderDetailTable>
                    </div>
                    <div class="w-[20rem]">
                        <OrderDetailTable>
                            <OrderDetailItem
                                key="Erstellt".to_owned()
                                value=format_rfc3339_to_date(&order.created)
                            />
                            <OrderDetailItem
                                key="Geändert".to_owned()
                                value=format_rfc3339_to_date(&order.updated)
                            />
                        </OrderDetailTable>
                    </div>
                </div>
            </div>
        </div>
    }
        .into_view()
}
