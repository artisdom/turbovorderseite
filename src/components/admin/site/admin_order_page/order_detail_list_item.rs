use crate::economy::ShopCurrency;

use leptos::*;
use leptos_router::A;

use model::OrderItem;

#[must_use]
#[component]
pub fn OrderDetailsListItem(order_item: OrderItem) -> impl IntoView {
    view! {
        <tr class="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-[var(--secondary-color)] border-b dark:border-gray-700">
            <th
                scope="row"
                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
            >
                {order_item.quantity}
            </th>
            <td class="px-6 py-4">{order_item.product_id.clone()}</td>
            <td class="px-6 py-4">
                <A target="_blank" href=format!("/product/{}", order_item.product_id)>
                    {order_item.product_name}
                </A>
            </td>
            <td class="px-6 py-4">{ShopCurrency::from(order_item.amount_discount).to_string()}</td>
            <td class="px-6 py-4">{ShopCurrency::from(order_item.amount_tax).to_string()}</td>
            <td class="px-6 py-4">{ShopCurrency::from(order_item.price).to_string()}</td>
            <td class="px-6 py-4">
                {ShopCurrency::from(order_item.price * order_item.quantity).to_string()}
            </td>
        </tr>
    }
}
