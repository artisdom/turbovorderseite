use crate::components::admin::site::admin_product_catalog_page::admin_catalog_table::AdminCatalogTable;
use crate::components::standards::templates::admin_board_template::AdminBoardTemplate;

use leptos::*;

#[must_use]
#[component]
pub fn AdminProductsBoard() -> impl IntoView {
    view! {
        <AdminBoardTemplate page_name="Produktkatalog".to_owned()>
            <AdminCatalogTable/>
        </AdminBoardTemplate>
    }
}
