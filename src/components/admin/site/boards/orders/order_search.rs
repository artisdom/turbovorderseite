use crate::contexts::configuration_context::ConfigurationContext;

use leptos::{html::Input, *};
use leptos_router::{use_navigate, NavigateOptions};

#[component]
pub fn OrderSearch() -> impl IntoView {
    let search_ref = create_node_ref::<Input>();
    let configuration_signal = expect_context::<ConfigurationContext>();

    let forward_action = create_action(move |_| {
        let search = search_ref.get().expect("search to exist").value();
        let navigate = use_navigate();
        async move {
            navigate(
                &format!(
                    "{}/admin/order/{}",
                    configuration_signal.get_app_environment().app_deployment,
                    search
                ),
                NavigateOptions::default(),
            )
        }
    });

    view! {
        <form
            class="space-y-4 md:space-y-6"
            action="#"
            on:submit=move |ev| {
                ev.prevent_default();
                forward_action.dispatch(());
            }
        >

            <div class="flex items-center space-x-2">
                <div class="relative">
                    <div class="absolute inset-y-0 rtl:inset-r-0 start-0 flex items-center ps-3 pointer-events-none">
                        <svg
                            class="w-4 h-4 text-gray-500 dark:text-gray-400"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 20 20"
                        >
                            <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                            ></path>
                        </svg>
                    </div>
                    <input
                        type="text"
                        id="table-search-users"
                        class="block p-2 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="Search order"
                        _ref=search_ref
                    />
                </div>
                <button
                    type="submit"
                    class="mt-1.5 text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 shadow-lg shadow-lime-500/50 dark:shadow-lg dark:shadow-lime-800/80 font-medium rounded-lg text-sm px-5 py-1.5 text-center me-2 mb-2"
                >
                    "Suchen"
                </button>
            </div>
        </form>
    }
}
