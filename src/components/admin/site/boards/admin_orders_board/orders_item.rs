use crate::{
    components::admin::site::boards::admin_orders_board::order_status_select::SelectOrderStatus,
    economy::ShopCurrency,
};

use chrono::{DateTime, Datelike};
use leptos::*;
use leptos_router::A;
use model::Order;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[component]
pub fn OrdersItem(order: Order) -> impl IntoView {
    view! {
        <tr class="bg-white dark:bg-[var(--secondary-color)] hover:bg-gray-50 dark:hover:bg-gray-600">
            <td class="w-4 p-4">
                <div class="flex items-center">
                    <input
                        id="checkbox-table-search-3"
                        type="checkbox"
                        class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                    />
                    <label for="checkbox-table-search-3" class="sr-only">
                        checkbox
                    </label>
                </div>
            </td>
            <th class="px-6 py-4">
                <p class="font-semibold">{order.shipping_name.clone()}</p>
            </th>
            <td class="px-6 py-4">
                <p>{order.id.clone()}</p>
            </td>
            <td class="px-6 py-4">
                <SelectOrderStatus order=order.clone()/>
            </td>
            <td class="px-6 py-4">
                <p>{format_timestamp(&order.created)}</p>
            </td>
            <td class="px-6 py-4">
                <p>{format_timestamp(&order.updated)}</p>
            </td>
            <td class="px-6 py-4">
                <p>{ShopCurrency::from(order.amount_total).to_string()}</p>
            </td>
            <td class="px-6 py-4">
                <A
                    href=format!("/admin/order/{}", order.id)
                    class="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                >
                    "Anschauen"
                </A>
            </td>
        </tr>
    }
}

#[allow(clippy::needless_pass_by_value)]
fn format_timestamp(timestamp: &str) -> Option<String> {
    let parsed = DateTime::parse_from_rfc3339(timestamp).ok()?;
    let formatted_date = format!(
        "{:02}.{:02}.{}",
        parsed.day(),
        parsed.month(),
        parsed.year()
    );
    Some(formatted_date)
}
