use crate::components::admin::site::boards::orders::order_search::OrderSearch;

use leptos::*;
use model::Order;

use crate::components::admin::site::boards::admin_orders_board::orders_item::OrdersItem;

#[must_use]
#[allow(clippy::too_many_lines)]
#[component]
pub fn OrdersList(orders: Vec<Order>) -> impl IntoView {
    view! {
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <div class="flex items-center justify-between flex-column flex-wrap md:flex-row space-y-4 md:space-y-0 pb-4">
                <label for="table-search" class="sr-only">
                    Search
                </label>
                <OrderSearch/>
            </div>
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="p-4">
                            <div class="flex items-center">
                                <input
                                    id="checkbox-all-search"
                                    type="checkbox"
                                    class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                />
                                <label for="checkbox-all-search" class="sr-only">
                                    checkbox
                                </label>
                            </div>
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Liefername"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Bestellnummer"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Status"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Erstellt"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Geändert"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Gesamtsumme"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Aktion"
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <For
                        each=move || orders.clone()
                        key=|order| order.id.clone()
                        children=move |order| {
                            view! { <OrdersItem order=order/> }
                        }
                    />

                </tbody>
            </table>
        </div>
    }
}
