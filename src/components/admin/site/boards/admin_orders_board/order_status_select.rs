use leptos::*;
use model::{Order, OrderStatus};

use crate::contexts::admin_context::AdminContext;

#[must_use]
#[component]
pub fn SelectOrderStatus(order: Order) -> impl IntoView {
    let admin_context = expect_context::<AdminContext>();

    let change_signal = create_rw_signal(None);
    let status_signal = create_rw_signal(order.status.clone());

    let order_id = order.id;

    let update_action = create_action(move |status_code: &String| {
        let status = match status_code.parse::<i16>().unwrap() {
            0 => Some(OrderStatus::Pending),
            1 => Some(OrderStatus::Processing),
            2 => Some(OrderStatus::Shipped),
            3 => Some(OrderStatus::Delivered),
            4 => Some(OrderStatus::Canceled),
            _ => None,
        };
        async move {
            if let Some(status) = status {
                change_signal.set(Some(status))
            }
        }
    });

    let submit_action = create_action(move |status: &Option<OrderStatus>| {
        let status = status.to_owned();
        let order_id = order_id.to_owned();
        if let Some(status) = status.clone() {
            status_signal.set_untracked(status);
        }
        async move {
            if let Some(status) = status {
                admin_context.update_status(order_id, status.clone()).await;
                change_signal.set(None);
            }
        }
    });

    let change_resource = create_local_resource(change_signal, move |change_value| {
        let order_status = status_signal.get_untracked();
        async move {
            if let Some(status_code) = change_value {
                return status_code != order_status;
            }
            false
        }
    });

    view! {
        <div class="flex items-center">
            <select
                on:change=move |ev| update_action.dispatch(event_target_value(&ev))
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            >
                {OrderStatus::VALUES
                    .iter()
                    .enumerate()
                    .map(|(idx, status)| {
                        if status.eq(&order.status) {
                            view! {
                                <option value=idx selected>
                                    <b>{status.to_string()}</b>
                                </option>
                            }
                                .into_view()
                        } else {
                            view! { <option value=idx>{status.to_string()}</option> }.into_view()
                        }
                    })
                    .collect_view()}
            </select>
            {move || match change_resource.get() {
                Some(value) => {
                    if value {
                        view! {
                            <button
                                on:click=move |_| {
                                    submit_action.dispatch(change_signal.get_untracked())
                                }

                                type="button"
                                class="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 shadow-lg shadow-red-500/50 dark:shadow-lg dark:shadow-red-800/80 font-medium rounded-lg text-sm px-3 py-1.5 text-center me-2 mb-2"
                            >
                                "Ändern"
                            </button>
                        }
                            .into_view()
                    } else {
                        view! {}.into_view()
                    }
                }
                None => view! { "Loading..." }.into_view(),
            }}

        </div>
    }
}
