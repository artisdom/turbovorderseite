pub mod order_status_select;
pub mod orders_item;
pub mod orders_list;

use crate::{
    components::{
        admin::site::boards::admin_orders_board::orders_list::OrdersList,
        standards::templates::admin_board_template::AdminBoardTemplate,
    },
    contexts::order_context::OrderContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn AdminOrdersBoard() -> impl IntoView {
    let order = expect_context::<OrderContext>();
    let orders_resource = create_local_resource(order.current_orders_page, {
        move |_| {
            let order = order.to_owned();
            async move { order.get_orders(100, "desc".to_owned(), true).await }
        }
    });
    view! {
        <AdminBoardTemplate page_name="Orders"
            .to_owned()>
            {move || match orders_resource.get() {
                Some(orders) => view! { <OrdersList orders=orders/> }.into_view(),
                None => view! { <p class="text-2xl">"Lade Bestellungen..."</p> }.into_view(),
            }}

        </AdminBoardTemplate>
    }
}
