use leptos::*;

#[must_use]
#[allow(clippy::too_many_lines)]
#[component]
pub fn AdminOverviewLatestCustomers() -> impl IntoView {
    view! {
        <div class="p-4 mb-4 h-full bg-white rounded-lg shadow sm:p-6 dark:bg-[var(--secondary-color)]">
            <div class="flex justify-between items-center mb-4">
                <h3 class="text-xl font-bold leading-none text-gray-900 dark:text-white">
                    Latest
                    Customers
                </h3>
                <a
                    href="#"
                    class="inline-flex items-center p-2 text-sm font-medium rounded-lg text-primary-700 hover:bg-gray-100 dark:text-primary-500 dark:hover:bg-gray-700"
                >
                    View all
                </a>
            </div>
            <div class="flow-root">
                <ul role="list" class="divide-y divide-gray-200 dark:divide-gray-700">
                    <li class="py-3 sm:py-4">
                        <div class="flex items-center space-x-4">
                            <div class="flex-shrink-0">
                                <img
                                    class="w-8 h-8 rounded-full"
                                    src="../images/users/neil-sims.png"
                                    alt="Neil image"
                                />
                            </div>
                            <div class="flex-1 min-w-0">
                                <p class="text-sm font-medium text-gray-900 truncate dark:text-white">
                                    /* customer */
                                </p>
                                <p class="text-sm text-gray-500 truncate dark:text-gray-400">
                                    /* email */
                                </p>
                            </div>
                            <div class="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                                $320
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="flex justify-between items-center pt-3 border-t border-gray-200 sm:pt-6 dark:border-gray-700">
                <div>
                    <button
                        class="inline-flex items-center p-2 text-sm font-medium text-center text-gray-500 rounded-lg hover:text-gray-900 dark:text-gray-400 dark:hover:text-white"
                        type="button"
                        data-dropdown-toggle="latest-customers-dropdown"
                    >
                        /* time span */
                        <svg
                            class="ml-2 w-4 h-4"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M19 9l-7 7-7-7"
                            ></path>
                        </svg>
                    </button>
                    <div
                        class="hidden z-50 my-4 text-base list-none bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600"
                        id="latest-customers-dropdown"
                    >
                        <div class="py-3 px-4" role="none">
                            <p
                                class="text-sm font-medium text-gray-900 truncate dark:text-white"
                                role="none"
                            >
                                /* time span */
                            </p>
                        </div>
                        <ul class="py-1" role="none">
                            <li>
                                <a
                                    href="#"
                                    class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white"
                                    role="menuitem"
                                >
                                    /* week day */
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="flex-shrink-0">
                    <a
                        href="#"
                        class="inline-flex items-center p-2 text-xs font-medium uppercase rounded-lg text-primary-700 sm:text-sm hover:bg-gray-100 dark:text-primary-500 dark:hover:bg-gray-700"
                    >
                        Sales Report
                        <svg
                            class="ml-1 w-4 h-4 sm:w-5 sm:h-5"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M9 5l7 7-7 7"
                            ></path>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    }
}
