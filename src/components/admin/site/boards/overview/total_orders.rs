use leptos::*;

#[must_use]
#[component]
pub fn AdminOverviewTotalOrders() -> impl IntoView {
    view! {
        <div class="p-4 bg-white rounded-lg shadow sm:p-6 xl:p-8 dark:bg-[var(--secondary-color)]">
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <span class="text-2xl font-bold leading-none text-gray-900 sm:text-3xl dark:text-white">
                        /* order amount */
                    </span>
                    <h3 class="text-base font-normal text-gray-500 dark:text-gray-400">
                        Total orders this
                        week
                    </h3>
                </div>
                <div class="flex flex-1 justify-end items-center ml-5 w-0 text-base font-bold text-green-500 dark:text-green-400">
                    /* percentage */
                    <svg
                        class="w-5 h-5"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            fill-rule="evenodd"
                            d="M5.293 7.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L6.707 7.707a1 1 0 01-1.414 0z"
                            clip-rule="evenodd"
                        ></path>
                    </svg>
                </div>
            </div>
            <div id="new-products-chart"></div>
        </div>
    }
}
