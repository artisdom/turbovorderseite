use crate::components::{
    admin::site::boards::overview::{
        total_orders::AdminOverviewTotalOrders, week_sales::AdminOverviewWeekSales,
    },
    standards::templates::admin_board_template::AdminBoardTemplate,
};
use leptos::*;

#[must_use]
#[allow(clippy::too_many_lines)]
#[component]
pub fn AdminOverviewBoard() -> impl IntoView {
    view! {
        <AdminBoardTemplate page_name="Overview".to_owned()>
            <div class="flex overflow-hidden pt-2">
                <div id="main-content" class="overflow-y-auto relative w-full h-full">
                    <div class="px-4 pt-6">
                        <AdminOverviewWeekSales/>
                        <div class="grid grid-cols-1 gap-4 mt-4 w-full md:grid-cols-2 xl:grid-cols-3">
                            <AdminOverviewTotalOrders/>
                            <div class="p-4 bg-white rounded-lg shadow sm:p-6 xl:p-8 dark:bg-[var(--secondary-color)]">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0">
                                        <h3 class="text-base font-normal text-gray-500 dark:text-gray-400">
                                            Visitors this
                                            week
                                        </h3>
                                    </div>
                                    <div class="flex flex-1 justify-end items-center ml-5 w-0 text-base font-bold text-green-500 dark:text-green-400">
                                        <svg
                                            class="w-5 h-5"
                                            fill="currentColor"
                                            viewBox="0 0 20 20"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path
                                                fill-rule="evenodd"
                                                d="M5.293 7.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 5.414V17a1 1 0 11-2 0V5.414L6.707 7.707a1 1 0 01-1.414 0z"
                                                clip-rule="evenodd"
                                            ></path>
                                        </svg>
                                    </div>
                                </div>
                                <div id="visitors-chart"></div>
                            </div>

                            <div class="p-4 bg-white rounded-lg shadow sm:p-6 xl:p-8 dark:bg-[var(--secondary-color)]">
                                <div class="flex items-center">
                                    <div class="flex-shrink-0">
                                        <h3 class="text-base font-normal text-gray-500 dark:text-gray-400">
                                            User signups this
                                            week
                                        </h3>
                                    </div>
                                    <div class="flex flex-1 justify-end items-center ml-5 w-0 text-base font-bold text-red-500 dark:text-red-400">
                                        <svg
                                            class="w-5 h-5"
                                            fill="currentColor"
                                            viewBox="0 0 20 20"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path
                                                fill-rule="evenodd"
                                                d="M14.707 12.293a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l2.293-2.293a1 1 0 011.414 0z"
                                                clip-rule="evenodd"
                                            ></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-4 my-4 bg-white rounded-lg shadow sm:p-6 xl:p-8 dark:bg-[var(--secondary-color)]">
                            <div class="flex justify-between items-center mb-4">
                                <div>
                                    <h3 class="mb-2 text-xl font-bold text-gray-900 dark:text-white">
                                        Transactions
                                    </h3>
                                    <span class="text-base font-normal text-gray-500 dark:text-gray-400">
                                        This is a list of
                                        latest transactions
                                    </span>
                                </div>
                                <div class="flex-shrink-0">
                                    <a
                                        href="#"
                                        class="p-2 text-sm font-medium rounded-lg text-primary-700 hover:bg-gray-100 dark:text-primary-500 dark:hover:bg-gray-700"
                                    >
                                        View
                                        all
                                    </a>
                                </div>
                            </div>
                            <div class="flex flex-col mt-8">
                                <div class="overflow-x-auto rounded-lg">
                                    <div class="inline-block min-w-full align-middle">
                                        <div class="overflow-hidden shadow sm:rounded-lg">
                                            <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-600">
                                                <thead class="bg-gray-50 dark:bg-gray-700">
                                                    <tr>
                                                        <th
                                                            scope="col"
                                                            class="p-4 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-white"
                                                        >
                                                            Transaction
                                                        </th>
                                                        <th
                                                            scope="col"
                                                            class="p-4 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-white"
                                                        >
                                                            Date & Time
                                                        </th>
                                                        <th
                                                            scope="col"
                                                            class="p-4 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-white"
                                                        >
                                                            Amount
                                                        </th>
                                                        <th
                                                            scope="col"
                                                            class="p-4 text-xs font-medium tracking-wider text-left text-gray-500 uppercase dark:text-white"
                                                        >
                                                            Status
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="bg-white dark:bg-[var(--secondary-color)]">
                                                    <tr>
                                                        <td class="p-4 text-sm font-normal text-gray-900 whitespace-nowrap dark:text-white">
                                                            /* transaction */
                                                        </td>
                                                        <td class="p-4 text-sm font-normal text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                            /* date & time*/
                                                        </td>
                                                        <td class="p-4 text-sm font-semibold text-gray-900 whitespace-nowrap dark:text-white">
                                                            /* amount */
                                                        </td>
                                                        <td class="p-4 whitespace-nowrap">
                                                            <span class="bg-green-100 dark:bg-green-200 text-green-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded-md">
                                                                /* status */
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AdminBoardTemplate>
    }
}
