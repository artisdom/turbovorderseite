use leptos::*;

use crate::contexts::admin_context::{AdminBoard, AdminContext};

#[must_use]
#[allow(clippy::too_many_lines)]
#[component]
pub fn AdminMenu() -> impl IntoView {
    let admin_context = expect_context::<AdminContext>();
    view! {
        <ul class="mt-12 text-gray-900 dark:text-white">
            <li class=move || {
                format!(
                    "flex w-full justify-between cursor-pointer items-center mb-6 {}",
                    if admin_context.get_current_board().eq(&AdminBoard::Dashboard) {
                        "text-gray-600 dark:text-gray-600"
                    } else {
                        "text-gray-800 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-600"
                    },
                )
            }>
                <button
                    on:click=move |_| admin_context.set_current_board(AdminBoard::Dashboard)
                    class="flex items-center py-2"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-grid"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        fill="none"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    >
                        <path stroke="none" d="M0 0h24v24H0z"></path>
                        <rect x="4" y="4" width="6" height="6" rx="1"></rect>
                        <rect x="14" y="4" width="6" height="6" rx="1"></rect>
                        <rect x="4" y="14" width="6" height="6" rx="1"></rect>
                        <rect x="14" y="14" width="6" height="6" rx="1"></rect>
                    </svg>
                    <span class="text-sm ml-2 font-bold text-lg">"Dashboard"</span>
                </button>
                <div class="py-1 px-3 bg-gray-600 rounded text-gray-300 flex items-center justify-center text-xs">
                    5
                </div>
            </li>
            <li class=move || {
                format!(
                    "flex w-full justify-between cursor-pointer items-center mb-6 {}",
                    if admin_context.get_current_board().eq(&AdminBoard::Orders) {
                        "text-gray-600 dark:text-gray-600"
                    } else {
                        "text-gray-800 hover:text-gray-700 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-600"
                    },
                )
            }>
                <button
                    on:click=move |_| admin_context.set_current_board(AdminBoard::Orders)
                    class="flex items-center py-2"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-puzzle"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        fill="none"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    >
                        <path stroke="none" d="M0 0h24v24H0z"></path>
                        <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1"></path>
                    </svg>
                    <span class="text-sm ml-2 font-bold text-lg">"Bestellungen"</span>
                </button>
            </li>
            <li class=move || {
                format!(
                    "flex w-full justify-between cursor-pointer items-center mb-6 {}",
                    if admin_context.get_current_board().eq(&AdminBoard::Products) {
                        "text-gray-600 dark:text-gray-600"
                    } else {
                        "text-gray-800 hover:text-gray-700 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-600"
                    },
                )
            }>
                <button
                    on:click=move |_| admin_context.set_current_board(AdminBoard::Products)
                    class="flex items-center py-2"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-compass"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        fill="none"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    >
                        <path stroke="none" d="M0 0h24v24H0z"></path>
                        <polyline points="8 16 10 10 16 8 14 14 8 16"></polyline>
                        <circle cx="12" cy="12" r="9"></circle>
                    </svg>
                    <span class="text-sm ml-2 font-bold text-lg">"Produktkatalog"</span>
                </button>
            </li>
            <li class=move || {
                format!(
                    "flex w-full justify-between cursor-pointer items-center mb-6 {}",
                    if admin_context.get_current_board().eq(&AdminBoard::Invoices) {
                        "text-gray-600 dark:text-gray-600"
                    } else {
                        "text-gray-800 hover:text-gray-700 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-600"
                    },
                )
            }>
                <button
                    on:click=move |_| admin_context.set_current_board(AdminBoard::Invoices)
                    class="flex items-center py-2"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-puzzle"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        fill="none"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    >
                        <path stroke="none" d="M0 0h24v24H0z"></path>
                        <path d="M4 7h3a1 1 0 0 0 1 -1v-1a2 2 0 0 1 4 0v1a1 1 0 0 0 1 1h3a1 1 0 0 1 1 1v3a1 1 0 0 0 1 1h1a2 2 0 0 1 0 4h-1a1 1 0 0 0 -1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-1a2 2 0 0 0 -4 0v1a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1h1a2 2 0 0 0 0 -4h-1a1 1 0 0 1 -1 -1v-3a1 1 0 0 1 1 -1"></path>
                    </svg>
                    <span class="text-sm ml-2 font-bold text-lg">Invoices</span>
                </button>
                <div class="py-1 px-3 bg-gray-600 rounded text-gray-300 flex items-center justify-center text-xs">
                    25
                </div>
            </li>
            <li class=move || {
                format!(
                    "flex w-full justify-between cursor-pointer items-center mb-6 {}",
                    if admin_context.get_current_board().eq(&AdminBoard::Inventory) {
                        "text-gray-600 dark:text-gray-600"
                    } else {
                        "text-gray-800 hover:text-gray-700 hover:text-gray-700 dark:text-gray-400 dark:hover:text-gray-600"
                    },
                )
            }>
                <button
                    on:click=move |_| admin_context.set_current_board(AdminBoard::Inventory)
                    class="flex items-center py-2"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-stack"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        fill="none"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    >
                        <path stroke="none" d="M0 0h24v24H0z"></path>
                        <polyline points="12 4 4 8 12 12 20 8 12 4"></polyline>
                        <polyline points="4 12 12 16 20 12"></polyline>
                        <polyline points="4 16 12 20 20 16"></polyline>
                    </svg>
                    <span class="text-sm ml-2">"Lagerbestand"</span>
                </button>
            </li>
            <li class="flex w-full justify-between text-gray-400 hover:text-gray-300 cursor-pointer items-center">
                <a href="javascript:void(0)" class="flex items-center py-2">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        class="icon icon-tabler icon-tabler-settings"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        fill="none"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                    >
                        <path stroke="none" d="M0 0h24v24H0z"></path>
                        <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 0 0 2.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 0 0 1.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 0 0 -1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 0 0 -2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 0 0 -2.573 -1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 0 0 -1.065 -2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 0 0 1.066 -2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                        <circle cx="12" cy="12" r="3"></circle>
                    </svg>
                    <span class="text-sm ml-2 font-bold text-lg">Settings</span>
                </a>
            </li>
        </ul>
    }
}
