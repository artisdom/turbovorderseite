use leptos::*;

use crate::components::admin::site::menu::AdminMenu;

#[must_use]
#[allow(clippy::needless_pass_by_value)]
#[allow(clippy::too_many_lines)]
#[component]
pub fn AdminSideBar(children: ChildrenFn) -> impl IntoView {
    let openMenu = create_rw_signal(false);
    view! {
        <div class="w-full h-full">
            <dh-component>
                <div class="flex flex-no-wrap">
                    <div class="w-64 mb-6 absolute sm:relative md:h-full flex-col justify-between hidden sm:flex">
                        <div class="pl-12">
                            <div class="h-16 w-full flex items-center">
                                <p class="text-3xl text-[var(--primary-color)] font-bold">
                                    "Staff only"
                                </p>
                            </div>
                            <AdminMenu/>
                        </div>
                    </div>
                    <div
                        class="w-64 z-40 absolute bg-[var(--dark-background-color)] shadow md:h-full flex-col lg:justify-between sm:hidden transition duration-150 ease-in-out"
                        id="mobile-nav"
                    >
                        <button
                            aria-label="toggle sidebar"
                            id="openSideBar"
                            class="h-10 w-10 bg-gray-800 absolute right-0 -mr-10 flex items-center shadow rounded-tr rounded-br justify-center cursor-pointer focus:outline-none focus:ring-2 focus:ring-offset-2 rounded focus:ring-gray-800"
                            on:click=move |_| openMenu.update(|upd| *upd = !*upd)
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                class="icon icon-tabler icon-tabler-adjustments"
                                width="20"
                                height="20"
                                viewBox="0 0 24 24"
                                stroke-width="1.5"
                                stroke="#FFFFFF"
                                fill="none"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            >
                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                <circle cx="6" cy="10" r="2"></circle>
                                <line x1="6" y1="4" x2="6" y2="8"></line>
                                <line x1="6" y1="12" x2="6" y2="20"></line>
                                <circle cx="12" cy="16" r="2"></circle>
                                <line x1="12" y1="4" x2="12" y2="14"></line>
                                <line x1="12" y1="18" x2="12" y2="20"></line>
                                <circle cx="18" cy="7" r="2"></circle>
                                <line x1="18" y1="4" x2="18" y2="5"></line>
                                <line x1="18" y1="9" x2="18" y2="20"></line>
                            </svg>
                        </button>
                        <button
                            aria-label="Close sidebar"
                            id="closeSideBar"
                            class="hidden h-10 w-10 bg-gray-800 absolute right-0 mr-10 shadow rounded-tr rounded-br justify-center cursor-pointer text-white"
                            on:click=move |_| openMenu.update(|upd| *upd = false)
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                class="icon icon-tabler icon-tabler-x"
                                width="20"
                                height="20"
                                viewBox="0 0 24 24"
                                stroke-width="1.5"
                                stroke="currentColor"
                                fill="none"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                            >
                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                        <Show when=move || openMenu.get()>
                            <div class="px-8">
                                <div class="h-16 w-full flex items-center">
                                    <p class="text-3xl text-[var(--primary-color)]">"Admin"</p>
                                </div>
                                <div class="flex justify-center mt-48 mb-4 w-full">
                                    <div class="relative">
                                        <div class="text-gray-300 absolute ml-4 inset-0 m-auto w-4 h-4">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                class="icon icon-tabler icon-tabler-search"
                                                width="16"
                                                height="16"
                                                viewBox="0 0 24 24"
                                                stroke-width="1.5"
                                                stroke="currentColor"
                                                fill="none"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                            >
                                                <path stroke="none" d="M0 0h24v24H0z"></path>
                                                <circle cx="10" cy="10" r="7"></circle>
                                                <line x1="21" y1="21" x2="15" y2="15"></line>
                                            </svg>
                                        </div>
                                        <input
                                            class="bg-gray-700 focus:outline-none focus:ring-1 focus:ring-gray-100  rounded w-full text-sm text-gray-300 placeholder-gray-400 bg-gray-100 pl-10 py-2"
                                            type="text"
                                            placeholder="Search"
                                        />
                                    </div>
                                </div>
                            </div>
                        </Show>
                    </div>
                    <div class="container mx-auto">
                        <div class="w-full h-full text-gray-900 dark:text-white">{children()}</div>
                    </div>
                </div>
            </dh-component>
        </div>
    }
}
