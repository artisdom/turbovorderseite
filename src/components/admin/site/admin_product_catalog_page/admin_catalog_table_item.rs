use leptos::*;
use model::Product;

use crate::economy::{format_rfc3339_to_date, ShopCurrency};

#[must_use]
#[component]
pub fn AdminCatalogTableItem(product: Product) -> impl IntoView {
    view! {
        <tr class="bg-white border-b dark:bg-[var(--secondary-color)] dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
            <th
                scope="row"
                class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
            >
                {product.id}
            </th>
            <td class="px-6 py-4">{product.name}</td>
            <td class="px-6 py-4">{if product.is_orderable { "Ja" } else { "Nein" }}</td>
            <td class="px-6 py-4">
                {ShopCurrency::from(product.price.unwrap_or_default()).to_string()}
            </td>
            <td class="px-6 py-4">{format_rfc3339_to_date(&product.updated)}</td>
            <td class="px-6 py-4">{format_rfc3339_to_date(&product.created)}</td>
            <td class="px-6 py-4 text-right">
                <a href="#" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                    Edit
                </a>
            </td>
        </tr>
    }
}
