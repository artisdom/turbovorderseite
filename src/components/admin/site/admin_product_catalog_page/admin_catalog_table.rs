use crate::components::admin::site::admin_product_catalog_page::admin_catalog_table_item::AdminCatalogTableItem;
use crate::components::standards::pagination::Pagination;
use leptos::*;
use model::Product;
use pagination::models::Pagination;

use crate::{contexts::configuration_context::ConfigurationContext, fetch::api_get_call};

#[must_use]
#[component]
pub fn AdminCatalogTable() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let page_signal = create_rw_signal(1);
    let total_pages_signal = create_rw_signal(1);

    let products_resource = create_local_resource(page_signal, move |page| async move {
        api_get_call::<Pagination<Product>>(
            &format!(
                "{}/products?page={page}&size=20",
                configuration_context.get_app_environment().backend_endoint
            ),
            None,
        )
        .await
    });

    view! {
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            "Artikel-Nr."
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Name"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Verfügbar"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Preis"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Geändert"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            "Erstellt"
                        </th>
                        <th scope="col" class="px-6 py-3">
                            <span class="sr-only">"Bearbeiten"</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {move || {
                        products_resource
                            .map(|response| {
                                match response {
                                    Ok(products) => {
                                        total_pages_signal.set(products.total_pages);
                                        products
                                            .data
                                            .iter()
                                            .map(|product| {
                                                view! { <AdminCatalogTableItem product=product.clone()/> }
                                            })
                                            .collect_view()
                                    }
                                    Err(_) => view! { "No products found" }.into_view(),
                                }
                            })
                            .collect_view()
                    }}
                </tbody>
            </table>
            <div class="flex justify-center">
                <Pagination page_signal=page_signal total_pages=total_pages_signal/>
            </div>
        </div>
    }
}
