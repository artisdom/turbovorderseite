use crate::components::standards::legal_loader::LegalLoader;
use crate::components::standards::templates::footer_page_template::FooterPageTemplate;
use crate::legal::LegalDocument;

use leptos::*;

#[must_use]
#[component]
pub fn BatteryPage() -> impl IntoView {
    view! {
        <FooterPageTemplate page_name="Hinweise zur Batterieentsorgung">
            <LegalLoader legal_document=LegalDocument::Battery/>
        </FooterPageTemplate>
    }
}
