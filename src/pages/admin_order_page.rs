use crate::{
    authentication::UserClaims,
    components::admin::site::admin_order_page::{
        order_detail_button_area::OrderDetailButtonArea, order_detail_list::OrderDetailList,
        order_details_matrix::OrderDetailsMatrix,
    },
    contexts::{
        admin_context::AdminContext, configuration_context::ConfigurationContext,
        order_context::OrderContext,
    },
};

use leptos::*;
use leptos_oidc::Auth;
use leptos_router::{use_navigate, use_params_map, NavigateOptions};

#[must_use]
#[component]
pub fn AdminOrderDetailPage() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let auth = expect_context::<Auth>();
    let get_token_data = move || {
        auth.decoded_id_token::<UserClaims>(
            jsonwebtoken::Algorithm::EdDSA,
            &[&configuration_context.get_app_environment().auth_audience],
        )
    };

    let params_map = use_params_map();

    let back_action = create_action(move |()| async move {
        let navigate = use_navigate();
        let admin_context = expect_context::<AdminContext>();

        admin_context.set_current_board(crate::contexts::admin_context::AdminBoard::Orders);
        navigate("/admin", NavigateOptions::default());
    });

    let order_resource = create_local_resource(
        move || expect_context::<Auth>().access_token(),
        move |_| async move {
            let order_context = expect_context::<OrderContext>();
            let order_id =
                params_map.with_untracked(|params| params.get("id").cloned().unwrap_or_default());
            order_context.get_order(order_id).await
        },
    );

    view! {
        <Show
            when=move || {
                if let Some(token_data) = get_token_data().flatten() {
                    return token_data.claims.groups.contains(&"admin".to_string());
                }
                false
            }

            fallback=move || view! { "Forbidden" }
        >
            <div class="my-12">
                {move || match order_resource.get().flatten() {
                    Some(order) => {
                        view! {
                            <div class="flex flex-col">
                                <button
                                    on:click=move |_| back_action.dispatch(())
                                    class="text-white ml-12 my-2 w-36 bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2"
                                >
                                    "Zurück"
                                </button>
                                <div class="flex justify-center w-full space-x-2">
                                    <OrderDetailList order_items=order.items.clone()/>
                                    <div>
                                        <OrderDetailButtonArea
                                            order_id=order.id.clone()
                                            order_items=order.items.clone()
                                        />
                                        <OrderDetailsMatrix order=order/>
                                    </div>
                                </div>
                            </div>
                        }
                            .into_view()
                    }
                    None => {
                        view! { <p class="text-2xl">"Bestellung nicht gefunden"</p> }.into_view()
                    }
                }}

            </div>
        </Show>
    }
}
