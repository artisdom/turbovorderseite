use crate::{
    components::{
        product::product_cards::search_collection_card::SearchCollectionCard,
        standards::pagination::Pagination,
    },
    contexts::search_context::SearchContext,
};

#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_meilisearch::SingleSearch;
use leptos_meta::Title;
use leptos_router::{use_navigate, use_query_map, NavigateOptions};

use model::MeilisearchProduct;

// Loss of precision i32 -> f32 is 23 bits wide
#[allow(clippy::cast_precision_loss)]
#[must_use]
#[component]
pub fn ProductsPage() -> impl IntoView {
    let (processing_time_ms, set_processing_time_ms) = create_signal(0);
    let total_signal = create_rw_signal(0);

    let meilisearch = expect_context::<SingleSearch<MeilisearchProduct>>();
    if let Some(param) = use_query_map().with_untracked(|query| query.get("search").cloned()) {
        meilisearch
            .search_query()
            .update_untracked(move |search_query| {
                search_query.query = Some(param);
            });
    }

    create_effect(move |_| {
        if let Ok(time) = i32::try_from(meilisearch.processing_time_ms().unwrap_or_default()) {
            set_processing_time_ms(time);
        }
        total_signal.set(meilisearch.total_hits().unwrap_or_default());
    });

    view! {
        <Title text="Suche"/>
        <p class="text-center dark:text-white py-1 md:py-4 font-bold text-lg">
            {move || total_signal.get()} " Ergebnisse ("
            {move || (processing_time_ms.get() as f32 / 1000.0)} " Sekunden)"
        </p>
        <SearchResults/>
    }
    .into_view()
}
#[component]
fn SearchResults() -> impl IntoView {
    let meilisearch = expect_context::<SingleSearch<MeilisearchProduct>>();
    let search_context = expect_context::<SearchContext>();

    let hits_signal = create_rw_signal(Vec::new());
    let pages_signal = create_rw_signal(1);
    let total_pages_signal = create_rw_signal(0);

    let last_query_signal = create_rw_signal(String::new());
    let last_category_signal = create_rw_signal(String::new());

    let navigate = use_navigate();

    create_effect(move |_| {
        meilisearch.search_query().update(|upd| {
            upd.page = Some(pages_signal.get());
            if let Some(category) = search_context.get_category() {
                upd.filter = Some(format!("categories.lvl0=\"{category}\""));
                if !category.eq(&last_category_signal.get_untracked()) {
                    pages_signal.set(1);
                    last_category_signal.set_untracked(category);
                }
            }
        });
        let hits = meilisearch
            .hits()
            .map(|search_result| {
                search_result
                    .into_iter()
                    .map(|hit| hit.result)
                    .collect::<Vec<MeilisearchProduct>>()
            })
            .unwrap_or_default();
        if let Some(query) = meilisearch.query() {
            if hits
                .iter()
                .any(|product| product.id.to_lowercase().eq(&query.to_lowercase()))
            {
                navigate(&format!("/product/{query}"), NavigateOptions::default());
            } else {
                hits_signal.set(hits);
            }
        }
        if let Some(query) = meilisearch.query() {
            if !query.eq(&last_query_signal.get_untracked()) {
                pages_signal.set(1);
                last_query_signal.set_untracked(query);
            }
        }
        total_pages_signal.set(meilisearch.total_pages().unwrap_or_default());
    });

    view! {
        <div class="flex justify-center">
            <ul class="flex flex-wrap w-full md:w-[132rem] justify-center">
                <For
                    each=hits_signal
                    key=|product| product.id.clone()
                    children=move |product: MeilisearchProduct| {
                        view! {
                            <li class="flex justify-center items-center">
                                <SearchCollectionCard product=product/>
                            </li>
                        }
                            .into_view()
                    }
                />

            </ul>
        </div>
        <Show when=move || (total_pages_signal.get() > 0)>
            <div class="flex justify-center">
                <Pagination page_signal=pages_signal total_pages=total_pages_signal/>
            </div>
        </Show>
    }
}
