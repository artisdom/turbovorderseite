use crate::components::standards::legal_loader::LegalLoader;
use crate::components::standards::templates::footer_page_template::FooterPageTemplate;
use crate::legal::LegalDocument;

use leptos::*;

#[must_use]
#[component]
pub fn RevocationPage() -> impl IntoView {
    view! {
        <FooterPageTemplate page_name="Widerruf">
            <LegalLoader legal_document=LegalDocument::Revocation/>
        </FooterPageTemplate>
    }
}
