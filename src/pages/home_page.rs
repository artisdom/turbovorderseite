use crate::{
    cdn::{CdnOptions, CdnPullType, ContentProvider},
    components::{
        collections::{
            last_seen_collection::LastSeenCollection,
            meilisearch_category_collection::MeilisearchCategoryCollection,
        },
        home::site::{
            bestseller_collection::BestsellerCollection, category_headline::CategoryHeadline,
            we_are::WeAre,
        },
        standards::trapezoid_seperator::{Position, TrapezoidSeperator},
    },
    contexts::configuration_context::ConfigurationContext,
};

#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_meta::Title;
use url::Url;

#[must_use]
#[component]
pub fn HomePage() -> impl IntoView {
    let cdn_context = expect_context::<ContentProvider>();
    let configuration_context = expect_context::<ConfigurationContext>();

    let opt_url = cdn_context.provide_content_network(
        Url::parse(&format!(
            "{}/static/banner001.jpg",
            configuration_context.get_app_environment().app_deployment
        )),
        &CdnOptions {
            quality: Some(90),
            size: Some(2150),
            crop_width: Some(2150),
            crop_height: Some(440),
            crop_x: Some(0),
            crop_y: Some(620),
        },
        CdnPullType::Image,
    );

    view! {
        <Title text="MOTURBO - Deutschland"/>
        <section
            class="hidden md:flex items-center justify-center h-[18rem] lg:h-[27.5rem] w-full"
            style=format!(
                "background-image: url({opt_url});background-size:cover;background-repeat:no-repeat;background-position-y:center;background-attachment:fixed;background-position:center;background-repeat:no-repeat;background-size:cover;",
            )

            id="interactive-network"
        >
            <WeAre/>
        </section>
        <TrapezoidSeperator position=Position::Down/>
        <div class="mb-10 md:mb-20 md:mt-10 dark:bg-[var(--dark-background-color)]">
            <div class="flex justify-center">
                <div class="flex flex-col">
                    <div class="justify-center md:justify-start">
                        <CategoryHeadline>"Unsere Bestseller"</CategoryHeadline>
                        <BestsellerCollection/>
                        <div class="mt-8">
                            <CategoryHeadline>"Zylinderkits"</CategoryHeadline>
                            <MeilisearchCategoryCollection
                                category_name="Zylinder > Zylinderkits".to_owned()
                                count=10
                            />
                        </div>
                        <div class="mt-8">
                            <CategoryHeadline>"Variomatik"</CategoryHeadline>
                            <MeilisearchCategoryCollection
                                category_name="Variomatik und Keilriemen > Variomatik Kits"
                                    .to_owned()
                                count=10
                            />
                        </div>
                        <div class="mt-8">
                            <LastSeenCollection/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
    .into_view()
}
