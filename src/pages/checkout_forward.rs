use crate::contexts::{cart_context::CartContext, order_context::OrderContext};

use leptos::*;
use leptos_oidc::Auth;
use leptos_router::{use_navigate, NavigateOptions};

#[must_use]
#[component]
pub fn CheckoutForward() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();
    let auth_context = expect_context::<Auth>();

    let _ = create_local_resource(
        move || cart_context.get_raw_items(),
        move |items| async move {
            let navigate = use_navigate();
            if items.is_empty() {
                navigate("/", NavigateOptions::default());
            }
        },
    );

    let checkout_action = create_action(move |()| async move {
        let order = expect_context::<OrderContext>();
        let cart_context = expect_context::<CartContext>();
        let _ =
            if let Some(checkout_url) = order.send_checkout(cart_context.get_items().await).await {
                window().location().set_href(&checkout_url)
            } else {
                window().location().set_href("/")
            };
    });

    create_effect(move |_| {
        let auth_context = expect_context::<Auth>();
        if auth_context.authenticated() {
            checkout_action.dispatch(());
        }
    });

    view! {
        <div class="flex justify-center dark:text-white my-5 md:my-12">
            <div class="flex flex-col">
                <p class="text-5xl font-extrabold mb-4">"Zur Kasse"</p>
                <div class="flex flex-col space-y-2 md:flex-row md:space-y-0 items-center space-x-0 md:space-x-4">
                    <div class="bg-white dark:bg-[var(--secondary-color)] rounded-lg shadow w-full md:w-auto p-8">
                        <p class="text-lg">"Als Kunde bestellen"</p>
                        <a
                            href=auth_context.login_url()
                            class="inline-flex w-60 justify-center items-center mt-2 text-white bg-gradient-to-r from-[var(--primary-color)] via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-8 py-2.5 text-center me-2 mb-2"
                        >
                            "Anmelden"
                            <svg
                                class="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 14 10"
                            >
                                <path
                                    stroke="currentColor"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M1 5h12m0 0L9 1m4 4L9 9"
                                ></path>
                            </svg>
                        </a>
                    </div>
                    <div class="bg-white dark:bg-[var(--secondary-color)] rounded-lg w-full md:w-auto shadow p-8">
                        <p class="text-lg">"Als Gast bestellen"</p>
                        <button
                            on:click=move |_| checkout_action.dispatch(())
                            class="inline-flex w-60 justify-center items-center mt-2 text-white bg-gradient-to-r from-teal-400 via-teal-500 to-teal-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-teal-300 dark:focus:ring-teal-800 shadow-lg shadow-teal-500/50 dark:shadow-lg dark:shadow-teal-800/80 font-medium rounded-lg text-sm px-8 py-2.5 text-center me-2 mb-2"
                        >
                            "Zur Kasse als Gast"
                            <svg
                                class="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 14 10"
                            >
                                <path
                                    stroke="currentColor"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M1 5h12m0 0L9 1m4 4L9 9"
                                ></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    }
}
