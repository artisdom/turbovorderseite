use leptos::*;
use leptos_router::A;

use crate::components::standards::indicators::AvailableIndicator;

#[must_use]
#[component]
pub fn CatalogPage() -> impl IntoView {
    view! { <HomeProductCard/> }
}

#[must_use]
#[component]
fn HomeProductCard() -> impl IntoView {
    view! {
        <p class="text-2xl text-center">"Product Cards"</p>
        <div class="flex items-center justify-center">
            <div class="flex flex-col justify-between border border-gray-100 w-[16rem] rounded shadow-lg transform duration-300 hover:scale-105">
                <A href="">
                    <img
                        style="object-fit: cover;object-position: center 50%;width: 100%;height: 178px;"
                        loading="lazy"
                        src="https://www.scooter-attack.com/media/pimg/CA/thumbs/540916_4415983.jpg"
                    />
                </A>
                <div>
                    <A href="">
                        <p class="m-4 line-clamp-2">"Wartungskit Castrol 2T Schaltmoped"</p>
                    </A>
                    <div class="flex justify-between px-4 pb-4">
                        <p class="text-2xl font-bold">"36,00 €"</p>
                        <AvailableIndicator>"Verfügbar"</AvailableIndicator>
                    </div>
                </div>

            </div>
        </div>
    }
}
