#[allow(clippy::wildcard_imports)]
use leptos::*;

use crate::{
    components::collections::specified_collection::SpecifiedCollection,
    contexts::wishlist_context::WishlistContext,
};

#[must_use]
#[component]
pub fn WishlistPage() -> impl IntoView {
    let wishlist_context = create_rw_signal(expect_context::<WishlistContext>());

    view! {
        <div class="dark:text-white">
            <p class="text-3xl my-6 text-center">"Merkliste"</p>
            <Show when=move || (wishlist_context.get().items_count() == 0)>
                <p class="text-xl text-center mt-4">"Derzeit keine Artikel auf der Merkliste."</p>
            </Show>
            <div class="flex justify-center">
                <ul class="m-8 flex flex-wrap justify-center md:justify-start">
                    {move || {
                        let product_id_set = wishlist_context.get().products.0.get();
                        view! { <SpecifiedCollection product_id_set=product_id_set/> }
                    }}

                </ul>
            </div>
        </div>
    }
    .into_view()
}
