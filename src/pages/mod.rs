// => Dead code elimination
#![allow(clippy::wildcard_imports)]
// => Usage from leptos-components
#![allow(clippy::module_name_repetitions)]

pub mod admin_order_page;
pub mod admin_page;
pub mod battery;
pub mod cart_page;
pub mod catalog_page;
pub mod category_page;
pub mod checkout_forward;
pub mod faq_page;
pub mod home_page;
pub mod imprint;
pub mod order_handler;
pub mod payment;
pub mod privacy;
pub mod product_page;
pub mod products_page;
pub mod profile_page;
pub mod revocation;
pub mod shared_cart;
pub mod source_page;
pub mod terms;
pub mod wishlist_page;
