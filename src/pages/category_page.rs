use crate::{
    components::{
        product::product_cards::search_collection_card::SearchCollectionCard,
        products::filter_sidebar::FilterSideBar,
    },
    contexts::filter_context::FilterContext,
};

use leptos::*;
use leptos_meilisearch::{MultiSearch, SearchQueryBuilder, Selectors};
use leptos_meta::Title;
use leptos_router::{use_location, use_params_map};

use model::MeilisearchProduct;

#[must_use]
#[component]
pub fn CategoryPage() -> impl IntoView {
    let params_map = use_params_map();
    let location = use_location();

    let meilisearch_context = expect_context::<MultiSearch<MeilisearchProduct>>();
    let filter_context = expect_context::<FilterContext>();

    let hits_signal = create_rw_signal(Vec::<MeilisearchProduct>::new());
    let current_category_context = create_rw_signal(String::new());
    let sort_signal = create_rw_signal(Vec::<String>::new());

    let page_signal = create_rw_signal(1usize);
    let total_pages_signal = create_rw_signal(1usize);

    create_effect(move |_| {
        location.pathname.track();
        page_signal.set(1);
    });

    create_effect(move |_| {
        let lvl = params_map
            .with(|params| params.get("lvl").cloned().unwrap_or_default())
            .to_lowercase();
        let id = params_map
            .with(|params| params.get("id").cloned().unwrap_or_default())
            .to_lowercase();
        current_category_context.set(id.to_uppercase());
        meilisearch_context.search_queries().update(|upd| {
            upd.queries.clear();
            upd.queries.push(
                SearchQueryBuilder::default()
                    .facets(Selectors::All)
                    .limit(28usize)
                    .index_uid("products")
                    .filter(
                        filter_context
                            .build_signature_once(Some((format!("categories.{lvl}"), id))),
                    )
                    .sort(sort_signal.get())
                    .page(page_signal.get())
                    .build()
                    .unwrap(),
            );
            let mapped_filter = filter_context.get_mapped_facets();
            if mapped_filter
                .iter()
                .any(|(facet_head, _)| facet_head.starts_with("metadata"))
            {
                for (facet_head, _) in filter_context.get_filters() {
                    let filters = filter_context.build_inversion_filter_set(&facet_head);
                    if let Some((mapped_head, _)) = facet_head.split_once('.') {
                        upd.queries.push(
                            SearchQueryBuilder::default()
                                .facets(Selectors::Some(vec![mapped_head.to_string()]))
                                .limit(0usize)
                                .index_uid("products")
                                .filter(filter_context.build_signature(&filters))
                                .build()
                                .unwrap(),
                        );
                    }
                }
            }
        });
        if let Some(results) = meilisearch_context.results() {
            if let Some(filtered_query) = results.first() {
                let result_hits = filtered_query
                    .clone()
                    .hits
                    .into_iter()
                    .map(|hit| hit.result)
                    .collect::<Vec<_>>();
                hits_signal.set(result_hits);
                total_pages_signal.set(filtered_query.total_pages.unwrap_or_default());
            }
        }
    });

    view! {
        <Title text=move || current_category_context.get()/>
        <FilterSideBar
            category_signal=current_category_context
            sort_signal=sort_signal
            page_signal=page_signal
            total_pages_signal=total_pages_signal
        >
            <For
                each=move || hits_signal.get()
                key=|product| product.id.clone()
                children=move |product: MeilisearchProduct| {
                    view! {
                        <li>
                            <SearchCollectionCard product=product/>
                        </li>
                    }
                }
            />

        </FilterSideBar>
    }
}
