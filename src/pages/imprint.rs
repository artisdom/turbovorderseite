use crate::{
    components::standards::templates::footer_page_template::FooterPageTemplate,
    contexts::configuration_context::ConfigurationContext,
};

use leptos::*;

#[must_use]
#[component]
pub fn ImprintPage() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let app_configuration = configuration_context.get_app_environment();

    view! {
        <FooterPageTemplate page_name="Impressum">
            <div>
                <p>"Gesetzliche Anbieterkennung:"</p>
                <p class="font-bold">{app_configuration.company_name.clone()}</p>
                <p>{app_configuration.company_street.clone()}</p>
                <p>{format!("{} {}", app_configuration.company_postalcode, app_configuration.company_city).clone()}</p>
                <p>"Deutschland"</p>
            </div>
            <div>
                <p class="font-bold">"Vertr. d. d. Geschäftsführer"</p>
                <p>"Tim von Bobrutzki"</p>
                <p>"Daniel Kerkmann"</p>
            </div>
            <div>
                <p class="font-bold">"Kontakt"</p>
                <p>{format!("Telefon: {}", app_configuration.company_phone)}</p>
                <p>{format!("Email: {}", app_configuration.company_email)}</p>
            </div>
            <div>
                <p>"Eingetragen im Handelsregister des Amtsgerichtes Freiburg"</p>
                <p>{format!("Handelsregisternummer {}", app_configuration.company_registration)}</p>
            </div>
            <div>
                <p>
                    "Aufgrund des Kleinunternehmerstatus wird gemäß § 19 UStG die MwSt. in der Rechnung nicht ausgewiesen."
                </p>
            </div>
            <div>
                <p class="font-bold">"Alternative Streitbeilegung:"</p>
                <p>
                    "Die Europäische Kommission stellt eine Plattform für die außergerichtliche Online-Streitbeilegung (OS-Plattform) bereit, aufrufbar unter "
                    <span>
                        <a href="https://ec.europa.eu/consumers/odr">"https://ec.europa.eu/odr"</a>
                    </span> "."
                </p>
            </div>
            <div>
                <p>
                    "Wir sind nicht bereit und nicht verpflichtet, an Streitbeilegungsverfahren vor Verbraucherschlichtungsstellen teilzunehmen."
                </p>
            </div>
        </FooterPageTemplate>
    }
}
