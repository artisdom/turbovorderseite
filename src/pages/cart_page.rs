use crate::{
    components::orderflow::cart::site::cart_list::CartList, contexts::cart_context::CartContext,
};

use leptos::*;
use leptos_meta::Title;

#[must_use]
#[component]
pub fn CartPage() -> impl IntoView {
    let cart_context = expect_context::<CartContext>();

    let items_resource = create_local_resource(
        move || cart_context.get_raw_items(),
        move |_| async move { cart_context.get_items().await },
    );

    view! {
        <Title text="Warenkorb".to_owned()/>
        <div class="dark:text-white">
            <p class="font-bold text-4xl mb-4 mt-6 md:mb-10 md:mt-12 text-center">"Warenkorb"</p>
            <div class="my-2 flex flex-col md:flex-row justify-center">
                {move || match items_resource.get() {
                    Some(cart_items) => {
                        if cart_items.clone().is_empty() {
                            view! {
                                <p class="text-center text-3xl mt-4 mb-8">
                                    "Dein Warenkorb ist leer"
                                </p>
                            }
                                .into_view()
                        } else {
                            view! { <CartList cart_items=cart_items/> }
                        }
                    }
                    None => {
                        view! { <p class="text-center text-3xl mt-4 mb-8">"Lade Warenkorb..."</p> }
                            .into_view()
                    }
                }}

            </div>
        </div>
    }
}
