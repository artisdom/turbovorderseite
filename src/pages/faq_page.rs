use crate::{
    components::faq::site::{
        boards::{
            faq_retoure_board::FaqRetoureBoard, faq_shipping_board::FaqShippingBoard,
            faq_support_board::FaqSupportBoard,
        },
        side_bar::FaqSideBar,
    },
    contexts::faq_context::{FaqBoard, FaqContext},
};

use leptos::*;
use leptos_meta::Title;

#[must_use]
#[component]
pub fn FaqPage() -> impl IntoView {
    let faq_context = expect_context::<FaqContext>();
    view! {
        <Title text="Kundencenter"/>
        <div class="flex flex-col md:flex-row justify-center space-y-2 mb-2 md:my-10">
            <FaqSideBar>
                {move || match faq_context.get_current_board() {
                    FaqBoard::Shipping => view! { <FaqShippingBoard/> },
                    FaqBoard::Retoure => view! { <FaqRetoureBoard/> },
                    FaqBoard::Support => view! { <FaqSupportBoard/> },
                }}

            </FaqSideBar>
        </div>
    }
}
