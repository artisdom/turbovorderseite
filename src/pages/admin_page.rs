use crate::{
    authentication::UserClaims,
    components::admin::site::{
        boards::{
            admin_inventory_board::AdminInventoryBoard, admin_invoices_board::AdminInvoicesBoard,
            admin_orders_board::AdminOrdersBoard, admin_overview_board::AdminOverviewBoard,
            admin_products_board::AdminProductsBoard,
        },
        side_bar::AdminSideBar,
    },
    contexts::{
        admin_context::{AdminBoard, AdminContext},
        configuration_context::ConfigurationContext,
    },
};

use leptos::*;
use leptos_meta::Title;
use leptos_oidc::Auth;

#[must_use]
#[component]
pub fn AdminPage() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let auth = expect_context::<Auth>();
    let admin_context = expect_context::<AdminContext>();

    let get_token_data = move || {
        auth.decoded_id_token::<UserClaims>(
            jsonwebtoken::Algorithm::EdDSA,
            &[&configuration_context.get_app_environment().auth_audience],
        )
    };

    view! {
        <Title text="Admin"/>
        <Show
            when=move || {
                if let Some(token_data) = get_token_data().flatten() {
                    return token_data.claims.groups.contains(&"admin".to_string());
                }
                false
            }

            fallback=move || view! { "Forbidden" }
        >
            <div class="my-12">
                <AdminSideBar>
                    {move || match admin_context.get_current_board() {
                        AdminBoard::Dashboard => view! { <AdminOverviewBoard/> },
                        AdminBoard::Orders => view! { <AdminOrdersBoard/> }.into_view(),
                        AdminBoard::Products => view! { <AdminProductsBoard/> }.into_view(),
                        AdminBoard::Invoices => view! { <AdminInvoicesBoard/> }.into_view(),
                        AdminBoard::Inventory => view! { <AdminInventoryBoard/> }.into_view(),
                    }}

                </AdminSideBar>
            </div>
        </Show>
    }
}
