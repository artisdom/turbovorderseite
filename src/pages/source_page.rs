use crate::{
    components::standards::templates::source_article_template::SourceArticleTemplate,
    contexts::configuration_context::ConfigurationContext,
};
use leptos::*;
use leptos_meta::Title;

#[must_use]
#[component]
pub fn SourcePage() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();

    view! {
        <Title text="Quelle"/>
        <div class="dark:bg-[var(--dark-background-color)]">
            <div class="py-8 px-4 mx-auto max-w-[80rem] lg:py-16 lg:px-6">
                <div class="mx-auto max-w-[46rem] text-center mb-8 lg:mb-16">
                    <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">
                        "Rust, Rust .. warte was?!"
                    </h2>
                    <p class="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                        "From scratch!"
                    </p>
                    <p class="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                        "Das Shop System wurde from scratch in der Programmiersprache Rust von Daniél Kerkmann und Tim von Bobrutzki entwickelt."
                    </p>
                </div>
                <div class="grid gap-8 sm:grid-cols-2 lg:grid-cols-3">
                    <SourceArticleTemplate
                        is_active=true
                        is_open_source=true
                        name="Oxidized Frontend".to_owned()
                        description="Unser WebAssembly-Frontend übertrifft selbst die schnellsten modernen Webanwendungen und setzt dabei ausschließlich auf die Programmiersprache Rust in Verbindung mit einem Multi-Threading-WASM-Framework und dem Crate Axum. Lokale Tests mit über 110.000 Anfragen pro Sekunde erwiesen sich als mühelos machbar."
                            .to_owned()
                        image_url=format!(
                            "{}/static/source-images/loc.jpg",
                            configuration_context.get_app_environment().app_deployment,
                        )
                    />
                    <SourceArticleTemplate
                        is_active=true
                        is_open_source=true
                        name="Oxidized Backend".to_owned()
                        description="Das Backend wurde nach langer Entwicklung vollständig in Rust geschrieben und hat dabei hohe Leistungen erzielt. Unsere Entscheidung fiel auf die Rust-Graphdatenbank SurrealDB, die durchschnittliche Antwortzeiten von unter 300 µs (persistent) liefert und somit eine solide Basis für unsere Anforderungen bietet."
                            .to_owned()
                        repository_link="https://gitlab.com/turbomarktplatz/turbohinterseite"
                            .to_owned()
                        image_url=format!(
                            "{}/static/source-images/industry.jpg",
                            configuration_context.get_app_environment().app_deployment,
                        )
                    />
                    <SourceArticleTemplate
                        is_active=true
                        is_open_source=true
                        name="Oxidized CDN".to_owned()
                        description="Um laufende Kosten zu senken, hatten wir uns ab einem gewissen Zeitpunkt dazu entschieden ein Content-Delivery-Network für die zuverlässige bereitstellung von Medien selber zu entwickeln. Wieder in der Programmiersprache Rust und natürlich eine Menge Performance. Schnelle Ladezeiten durch Caching und Optimizing werden garantiert."
                            .to_owned()
                        repository_link="https://gitlab.com/turbomarktplatz/turbobilder".to_owned()
                        image_url=format!(
                            "{}/static/source-images/spiral.jpg",
                            configuration_context.get_app_environment().app_deployment,
                        )
                    />
                </div>
            </div>
        </div>
    }
}
