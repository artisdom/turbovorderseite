use crate::{
    components::orderflow::order_status::{
        order_success_status::OrderSuccess, order_validating_status::OrderValidating,
    },
    contexts::{cart_context::CartContext, order_context::OrderContext},
};

use leptos::*;
use leptos_meta::Title;
use leptos_router::{use_navigate, use_params_map, use_query_map, NavigateOptions};

#[must_use]
#[component]
pub fn OrderHandler() -> impl IntoView {
    let params_map = use_params_map();
    let query_map = use_query_map();
    let status =
        params_map.with_untracked(|params| params.get("status").cloned().unwrap_or_default());

    let validate_resource = create_local_resource(
        || (),
        move |()| {
            let session_id = query_map
                .with_untracked(|queries| queries.get("session_id").cloned().unwrap_or_default());
            async move {
                let order = expect_context::<OrderContext>();
                order.validate_checkout(session_id).await
            }
        },
    );
    let back_to_cart = move || {
        let navigate = use_navigate();
        navigate("/cart", NavigateOptions::default());
        view! { /* redirecting to cart */ }
    };

    view! {
        <Title text="Bestellung Status"/>
        {move || match status.as_str() {
            "success" => {
                match validate_resource.get().flatten() {
                    Some(order) => {
                        let cart = expect_context::<CartContext>();
                        cart.remove_all_from_cart();
                        view! { <OrderSuccess order=order/> }.into_view()
                    }
                    None => view! { <OrderValidating/> }.into_view(),
                }
            }
            _ => back_to_cart().into_view(),
        }}
    }
}
