use crate::{
    authentication::UserClaims,
    components::profile::{profile_board::ProfileBoard, profile_menu::ProfileMenu},
    contexts::configuration_context::ConfigurationContext,
    error::{entry::ErrorEntry, AppError},
};

#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_meta::Title;
use leptos_oidc::Auth;

#[must_use]
#[component]
pub fn ProfilePage() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let auth = expect_context::<Auth>();
    let get_token_data = move || {
        auth.decoded_id_token::<UserClaims>(
            jsonwebtoken::Algorithm::EdDSA,
            &[&configuration_context.get_app_environment().auth_audience],
        )
    };

    view! {
        <Title text="Profil"/>
        {move || match get_token_data() {
            Some(response) => {
                match response {
                    Some(token_data) => {
                        let claims = token_data.claims;
                        view! {
                            <div class="flex items-start justify-center md:my-20">
                                <div class="flex items-start flex-col md:flex-row justify-center w-full m-10 md:m-0 md:w-[68em] md:h-auto md:min-h-[30em] md:space-x-4">
                                    <ProfileMenu fullname=format!(
                                        "{} {}",
                                        claims.given_name,
                                        claims.family_name,
                                    )/>
                                    <ProfileBoard claims=claims/>
                                </div>
                            </div>
                        }
                            .into_view()
                    }
                    None => view! { <ErrorEntry error=AppError::Forbidden/> }.into_view(),
                }
            }
            None => view! { <p class="mt-10 text-3xl">"Lade Profil..."</p> }.into_view(),
        }}
    }.into_view()
}
