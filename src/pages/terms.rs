use crate::{
    components::standards::{
        legal_loader::LegalLoader, templates::footer_page_template::FooterPageTemplate,
    },
    legal::LegalDocument,
};
use leptos::*;

#[must_use]
#[component]
pub fn TermsPage() -> impl IntoView {
    view! {
        <FooterPageTemplate page_name="Allgemeine Geschäftsbedingungen und Kundeninformationen">
            <LegalLoader legal_document=LegalDocument::Terms/>
        </FooterPageTemplate>
    }
}
