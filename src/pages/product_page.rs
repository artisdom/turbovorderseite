use crate::contexts::configuration_context::ConfigurationContext;
use crate::contexts::last_seen_context::LastSeenContext;
use crate::error::template::ErrorTemplate;
use crate::fetch::cached_api_get_call;
use crate::{
    components::product::site::{
        product_detail_breadcrumb::ProductBreadcrumb,
        product_detail_description::ProductDetailDescription,
        product_detail_gallery::ProductDetailGallery,
        product_detail_meta_list::ProductDetailMetaList,
        product_detail_properties::ProductDetailProperties,
        product_page_skeleton::ProductPageSkeleton,
    },
    error::AppError,
};

#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_meta::{Meta, Title};
use leptos_router::use_params_map;

use model::Product;

#[must_use]
#[component]
pub fn ProductPage() -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();

    let product_resource = create_blocking_resource(use_params_map(), move |map| async move {
        if let Some(id) = map.get("id") {
            cached_api_get_call::<Product>(
                format!(
                    "{}/products/{id}",
                    configuration_context.get_app_environment().backend_endoint
                )
                .as_str(),
                None,
            )
            .await
        } else {
            Err(AppError::NotFound)
        }
    });

    view! {
        <Suspense fallback=ProductPageSkeleton>
            <ErrorBoundary fallback=|errors| {
                view! { <ErrorTemplate errors=errors/> }
            }>
                {move || match product_resource.get() {
                    Some(product_result) => {
                        product_result
                            .map(|product| {
                                view! { <ProductDetails product=product.clone()/> }.into_view()
                            })
                    }
                    None => Ok(ProductPageSkeleton().into_view()),
                }}

            </ErrorBoundary>
        </Suspense>
    }
    .into_view()
}

#[component]
#[allow(clippy::needless_pass_by_value)]
fn ProductDetails(product: Product) -> impl IntoView {
    // Push into product seen history
    let last_seen_context = expect_context::<LastSeenContext>();
    last_seen_context.push(product.id.clone());

    // Break up lifetimes
    let product_description = product.description.clone();
    let product_preview = product.preview_media.clone();
    let product_category = product.category.clone();
    let product_name = product.name.clone();

    view! {
        <Title text=product.name.clone()/>
        <Show when={
            let product_description_exists = product.description.is_some();
            move || product_description_exists
        }>
            <Meta
                name="description"
                content=format!(
                    "{}…",
                    product_description
                        .clone()
                        .unwrap_or_default()
                        .chars()
                        .take(150)
                        .collect::<String>(),
                )
            />

        </Show>
        <Show when={
            let product_preview_exists = product.preview_media.is_some();
            move || product_preview_exists
        }>
            <Meta name="twitter:image" content=product_preview.clone().unwrap().url/>
            <Meta property="og:image:width" content="1200"/>
            <Meta property="og:image:height" content="627"/>
        </Show>

        <div class="w-full md:flex md:justify-center">
            <div class="flex flex-col md:grid gap-x-1 gap-y-0 gap-x-4 gap-y-4 md:max-w-[84rem] md:my-12">
                <Show when={
                    let product_category_exists = product.category.is_some();
                    move || product_category_exists
                }>
                    <ProductBreadcrumb category=product_category.clone() name=product_name.clone()/>
                </Show>
                <div class="dark:text-white row-start-2 row-end-3">
                    <div class="p-4 dark:bg-[var(--secondary-color)] bg-white md:shadow md:rounded-2xl md:flex justify-between inline-block">
                        <div class="md:hidden mb-2">
                            <p class="font-black text-2xl">{product.name.clone()}</p>
                        </div>
                        <ProductDetailGallery product=product.clone()/>
                        <ProductDetailProperties product=product.clone()/>
                    </div>
                </div>
                <div class="row-start-3 row-end-4 dark:text-white">
                    <div class="flex flex-col md:grid grid-cols-3 grid-rows-1 gap-x-4 gap-y-4">
                        <ProductDetailDescription description=product
                            .description
                            .unwrap_or_default()/>
                        <ProductDetailMetaList product_id=product.id metadata=product.metadata/>
                    </div>
                </div>
            </div>
        </div>
    }
}
