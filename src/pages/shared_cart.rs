use crate::components::orderflow::shared_cart::shared_cart_totals::SharedCartTotals;
use crate::{
    codec::decode_from_id,
    components::orderflow::cart::site::cart_item::CartListItem,
    contexts::{cart_context::CartItem, configuration_context::ConfigurationContext},
    economy::format_timestamp_to_date,
    error::AppResult,
    fetch::api_get_call,
};

use futures::future::join_all;
use leptos::*;
use leptos_meta::Title;
use leptos_router::use_params_map;
use model::Product;
use wasm_bindgen::UnwrapThrowExt;

#[must_use]
#[component]
pub fn SharedCartPage() -> impl IntoView {
    let decode_resource = create_local_resource(use_params_map(), move |map| async move {
        if let Some(id) = map.get("id") {
            decode_from_id(id.to_string())
        } else {
            None
        }
    });

    view! {
        <Title text="Geteilter Warenkorb"/>
        <div class="dark:text-white">
            {move || match decode_resource.get() {
                Some(response) => {
                    match response {
                        Some(cart_format) => {
                            let items = cart_format
                                .items
                                .iter()
                                .map(|item| CartItem {
                                    product_id: item.1.clone(),
                                    quantity: item.0,
                                })
                                .collect::<Vec<CartItem>>();
                            view! {
                                <p class="font-bold text-4xl mb-4 mt-6 md:mb-10 md:mt-12 text-center">
                                    {format!(
                                        "Geteilter Warenkorb vom {}",
                                        format_timestamp_to_date(cart_format.timestamp),
                                    )}
                                </p>
                                <div class="my-2 flex flex-col md:flex-row justify-center">
                                    <CartContent cart_items=items/>
                                </div>
                            }
                                .into_view()
                        }
                        None => {
                            view! {
                                <p class="text-2xl mt-2 md:mt-12 text-center">
                                    "Warenkorb konnte nicht geladen werden"
                                </p>
                            }
                                .into_view()
                        }
                    }
                }
                None => {
                    view! {
                        <p class="text-2xl mt-2 md:mt-12 text-center">
                            "Warenkorb wird geladen..."
                        </p>
                    }
                        .into_view()
                }
            }}

        </div>
    }
}

#[component]
pub fn CartContent(cart_items: Vec<CartItem>) -> impl IntoView {
    let configuration_context = expect_context::<ConfigurationContext>();
    let product_resource = create_local_resource(
        move || cart_items.clone(),
        move |cart_items| async move {
            let product_result_tasks: Vec<_> = cart_items
                .iter()
                .map(|cart_item| async {
                    (
                        cart_item.clone(),
                        api_get_call::<Product>(
                            format!(
                                "{}/products/{}",
                                configuration_context.get_app_environment().backend_endoint,
                                cart_item.product_id.to_owned()
                            )
                            .as_str(),
                            None,
                        )
                        .await,
                    )
                })
                .collect();
            join_all(product_result_tasks)
                .await
                .into_iter()
                .collect::<Vec<(CartItem, AppResult<Product>)>>()
        },
    );

    move || {
        match product_resource.get() {
        Some(cart_content) => {
            view! {
                <ul class="mt-2 p-4 md:p-0">
                    {cart_content
                        .iter()
                        .filter(|product_result| product_result.1.is_ok())
                        .map(|item| {
                            view! {
                                <CartListItem
                                    product=item.1.clone().unwrap_throw()
                                    quantity=item.0.quantity
                                    allow_actions=false
                                />
                            }
                                .into_view()
                        })
                        .collect_view()}
                </ul>
                <SharedCartTotals cart_content=cart_content.clone()/>
            }.into_view()
        }
        None => view! { <p class="text-2xl mt-2 md:mt-12 text-center">"Produkte werden geladen..."</p> }
        .into_view(),
    }
    }
}
