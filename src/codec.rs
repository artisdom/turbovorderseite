use base64::{engine::general_purpose::URL_SAFE_NO_PAD, Engine as _};
use chrono::Utc;
use miniz_oxide::{deflate::compress_to_vec_zlib, inflate::decompress_to_vec_zlib_with_limit};
use serde::{Deserialize, Serialize};
use wasm_bindgen::UnwrapThrowExt;

use crate::contexts::cart_context::CartItem;

const COMPRESSION_LEVEL: u8 = 10;
const MAX_DECOMPRESSION_SIZE: usize = 50_000;

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct CartFormat {
    #[serde(rename(serialize = "T", deserialize = "T"))]
    pub timestamp: i64,
    #[serde(rename(serialize = "I", deserialize = "I"))]
    pub items: Vec<(i32, String)>,
}

pub fn encode_to_id(items: Vec<CartItem>) -> String {
    let timestamp = Utc::now().timestamp();
    let serialized = serde_json::to_string(&CartFormat {
        timestamp,
        items: items
            .iter()
            .map(|item| (item.quantity, item.product_id.clone()))
            .collect(),
    })
    .unwrap_throw();
    let compressed = compress_to_vec_zlib(serialized.as_bytes(), COMPRESSION_LEVEL);
    URL_SAFE_NO_PAD.encode(compressed)
}

pub fn decode_from_id(id: String) -> Option<CartFormat> {
    let decoded = URL_SAFE_NO_PAD.decode(id).ok()?;
    let decompressed =
        decompress_to_vec_zlib_with_limit(decoded.as_slice(), MAX_DECOMPRESSION_SIZE).ok()?;
    Some(serde_json::from_slice::<CartFormat>(decompressed.as_slice()).ok()?)
}
