use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct TokenClaims {
    exp: i64,
    iat: i64,
    auth_time: i64,
    jti: String,
    iss: String,
    aud: String,
    sub: String,
    typ: String,
    azp: String,
    session_state: String,
    acr: String,
    allowed_origins: Vec<String>,
    realm_access: RealmAccess,
    resource_access: ResourceAccess,
    scope: String,
    sid: String,
    pub email_verified: bool,
    pub name: String,
    pub preferred_username: String,
    pub given_name: String,
    pub family_name: String,
    pub email: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct RealmAccess {
    roles: Vec<String>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct ResourceAccess {
    account: AccountRoles,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct AccountRoles {
    roles: Vec<String>,
}
