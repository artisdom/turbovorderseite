use model::OrderStatus;

#[must_use]
pub fn format_status(status: &OrderStatus) -> &'static str {
    match status {
        OrderStatus::Pending => "Zahlung ausstehend",
        OrderStatus::Processing => "In Bearbeitung",
        OrderStatus::Shipped => "Unterwegs",
        OrderStatus::Delivered => "Ausgeliefert",
        OrderStatus::Canceled => "Storniert",
    }
}
