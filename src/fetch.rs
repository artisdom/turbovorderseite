use crate::error::{AppError, AppResult};

use chrono::Utc;
use once_cell::sync::Lazy;
use reqwest::{RequestBuilder, StatusCode};
use std::collections::HashMap;
use std::sync::Mutex;

#[derive(Debug)]
struct CacheEntry {
    value: serde_json::Value,
    timestamp: chrono::DateTime<Utc>,
}

static CACHE: Lazy<Mutex<HashMap<String, CacheEntry>>> = Lazy::new(|| Mutex::new(HashMap::new()));

/// # Panics
///
/// Will panic when hour 1 cannot be parsed
/// # Errors
///
/// Will return `Err` if API is not reachable, data cannot be parsed or mutex cache error
pub async fn cached_api_get_call<T>(
    path: &str,
    bearer_authorization: Option<String>,
) -> AppResult<T>
where
    T: serde::de::DeserializeOwned + serde::Serialize + std::fmt::Debug,
{
    let cache = &*CACHE;

    if let Some(entry) = cache.lock().map_err(|_| AppError::MutexError)?.get(path) {
        let elapsed = Utc::now() - entry.timestamp;
        if elapsed < chrono::Duration::try_hours(1).expect("Chrono duration invalid") {
            let cached_value: T = serde_json::from_value(entry.value.clone())?;
            return Ok(cached_value);
        }
    }

    let response = api_get_call::<T>(path, bearer_authorization).await?;

    let new_entry = CacheEntry {
        value: serde_json::to_value(&response)?,
        timestamp: Utc::now(),
    };

    cache
        .lock()
        .map_err(|_| AppError::MutexError)?
        .insert(path.to_string(), new_entry);

    Ok(response)
}

/// # Errors
///
/// Will return `Err` if API is not reachable or data cannot be parsed
pub async fn api_get_call<T>(path: &str, bearer_token: Option<String>) -> AppResult<T>
where
    T: serde::de::DeserializeOwned + std::fmt::Debug,
{
    let client = bearer_authorization(reqwest::Client::new().get(path), bearer_token);
    api_send::<T>(client).await
}

/// # Errors
///
/// Will return `Err` if API is not reachable or data cannot be parsed
pub async fn api_post_call<T, C>(
    path: &str,
    body: Option<T>,
    bearer_token: Option<String>,
) -> AppResult<C>
where
    T: serde::de::DeserializeOwned + serde::Serialize,
    C: serde::de::DeserializeOwned,
{
    let client = bearer_authorization(reqwest::Client::new().post(path).json(&body), bearer_token);
    api_send::<C>(client).await
}

/// # Errors
///
/// Will return `Err` if API is not reachable or data cannot be parsed
pub async fn api_put_call<T, C>(path: &str, body: T, bearer_token: Option<String>) -> AppResult<C>
where
    T: serde::de::DeserializeOwned + serde::Serialize,
    C: serde::de::DeserializeOwned,
{
    let client = bearer_authorization(reqwest::Client::new().put(path).json(&body), bearer_token);
    api_send::<C>(client).await
}

fn bearer_authorization(client: RequestBuilder, bearer_token: Option<String>) -> RequestBuilder {
    match bearer_token {
        Some(bearer_token) => client.header("Authorization", format!("Bearer {bearer_token}")),
        None => client,
    }
}

/// # Errors
///
/// Will return `Err` if API is not reachable or data cannot be parsed
async fn api_send<T>(client: RequestBuilder) -> AppResult<T>
where
    T: serde::de::DeserializeOwned,
{
    client
        .send()
        .await
        .map_err(|err| AppError::ApiError(err.to_string()))?
        .json()
        .await
        .map_err(|err| {
            // FIXME No handling funcationality for 404
            if let Some(status) = err.status() {
                if status == StatusCode::NOT_FOUND {
                    return AppError::NotFound;
                }
            }
            AppError::ApiError(err.to_string())
        })
}
