#![allow(clippy::wildcard_imports)]
#![allow(clippy::module_name_repetitions)]

use super::{
    cdn::ContentProvider,
    components::navigation::{footer::Footer, navbar::Navbar},
    contexts::{
        admin_context::AdminContext, cart_context::CartContext,
        configuration_context::ConfigurationContext, faq_context::FaqContext,
        filter_context::FilterContext, last_seen_context::LastSeenContext,
        order_context::OrderContext, profile_context::ProfileContext,
        search_context::SearchContext, wishlist_context::WishlistContext,
    },
    environment::AppEnvironment,
    error::{template::ErrorTemplate, AppError},
    pages::{
        admin_order_page::AdminOrderDetailPage, admin_page::AdminPage, battery::BatteryPage,
        cart_page::CartPage, catalog_page::CatalogPage, category_page::CategoryPage,
        checkout_forward::CheckoutForward, faq_page::FaqPage, home_page::HomePage,
        imprint::ImprintPage, order_handler::OrderHandler, payment::PaymentPage,
        privacy::PrivacyPage, product_page::ProductPage, products_page::ProductsPage,
        profile_page::ProfilePage, revocation::RevocationPage, shared_cart::SharedCartPage,
        source_page::SourcePage, terms::TermsPage, wishlist_page::WishlistPage,
    },
};

use leptos::*;
use leptos_darkmode::Darkmode;
use leptos_meilisearch::{
    AuthParameters as MeilisearchAuthParameters, MultiSearch, SearchQueryBuilder, SingleSearch,
};
use leptos_meta::*;
use leptos_oidc::{Auth, AuthParameters};
use leptos_router::*;

use model::MeilisearchProduct;

#[must_use]
#[component]
pub fn App() -> impl IntoView {
    provide_meta_context();
    let darkmode = Darkmode::init();

    view! {
        <Link
            rel="preload"
            href="/static/fonts/montserrat.woff2"
            as_="font"
            crossorigin="anonymous"
        />
        <Stylesheet id="leptos" href="/pkg/frontend.css"/>

        // define darkmode by class
        <Html lang="de" class=move || (if darkmode.is_dark() { "dark" } else { "" })/>

        // global class props
        <Body class="bg-white md:bg-gray-100 dark:bg-[var(--dark-background-color)] flex flex-col min-h-screen overflow-x-hidden"/>

        <Router fallback=|| {
            let mut outside_errors = Errors::default();
            outside_errors.insert_with_default_key(AppError::NotFound);
            view! { <ErrorTemplate outside_errors/> }.into_view()
        }>
            <Routing/>
        </Router>
    }
}
fn Routing() -> impl IntoView {
    let environment = AppEnvironment::init();

    let auth_parameters = AuthParameters {
        issuer: environment.auth_authorization_endpoint.clone(),
        client_id: environment.auth_client_id.clone(),
        redirect_uri: environment.login_redirect_endpoint.clone(),
        post_logout_redirect_uri: environment.logout_redirect_endpint.clone(),
        scope: Some(environment.auth_scope.clone()),
        audience: None,
        challenge: leptos_oidc::Challenge::S256,
    };
    let _ = Auth::init(auth_parameters);

    ConfigurationContext::init(environment.clone());

    provide_context(CartContext::new());
    provide_context(WishlistContext::new());
    provide_context(FilterContext::new());
    provide_context(FaqContext::new());
    provide_context(LastSeenContext::new());
    provide_context(AdminContext::new());
    provide_context(SearchContext::new());

    ProfileContext::init(environment.auth_audience);
    OrderContext::init(environment.backend_endoint);
    ContentProvider::init(
        environment.content_networks.clone(),
        environment.pull_defaults.clone(),
    );
    MultiSearch::<MeilisearchProduct>::init(MeilisearchAuthParameters {
        host: environment.meilisearch_endpoint.clone(),
        api_key: Some(environment.meilisearch_api_key.clone()),
    })
    .expect("unable to init meili multi-search");

    // As long we do not support filtering and optimize response times
    // in single search we focus the selector on »None«
    SingleSearch::<MeilisearchProduct>::init(
        MeilisearchAuthParameters {
            host: environment.meilisearch_endpoint.clone(),
            api_key: Some(environment.meilisearch_api_key.clone()),
        },
        "products",
        &SearchQueryBuilder::default().hits_per_page(30_usize),
    )
    .expect("unable to init meili single-search");

    view! {
        <main class="h-full flex flex-col">
            <Navbar/>
            <div class="h-full">
                <Routes>
                    <Route path="" view=HomePage/>
                    <Route path="product/:id" view=ProductPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="products" view=ProductsPage/>
                    <Route path="category/:lvl/:id" view=CategoryPage/>
                    <Route path="profile" view=ProfilePage/>
                    <Route path="wishlist" view=WishlistPage/>
                    <Route path="cart" view=CartPage/>
                    <Route path="shared-cart/:id" view=SharedCartPage/>
                    <Route path="checkout-forward" view=CheckoutForward/>
                    <Route path="order/:status" view=OrderHandler/>
                    <Route path="source" view=SourcePage/>
                    <Route path="support" view=FaqPage/>
                    <Route path="shipping" view=FaqPage/>
                    <Route path="retoure" view=FaqPage/>
                    <Route path="payment" view=PaymentPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="privacy" view=PrivacyPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="revocation" view=RevocationPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="battery" view=BatteryPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="terms" view=TermsPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="imprint" view=ImprintPage ssr=SsrMode::PartiallyBlocked/>
                    <Route path="design-catalog" view=CatalogPage/>
                    <Route path="admin" view=AdminPage/>
                    <Route path="admin/order/:id" view=AdminOrderDetailPage/>
                </Routes>
            </div>
        </main>
        <Footer/>
    }
}
