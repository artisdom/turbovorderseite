#![allow(non_snake_case)]

pub mod app;
pub mod environment;
pub mod error;

#[cfg(feature = "ssr")]
pub mod fileserv;

pub mod authentication;
pub mod cdn;
pub mod codec;
pub mod components;
pub mod contexts;
pub mod economy;
pub mod fetch;
pub mod legal;
pub mod order;
pub mod pages;
pub mod sitemap_generator;

#[cfg(feature = "hydrate")]
#[wasm_bindgen::prelude::wasm_bindgen]
pub fn hydrate() {
    use crate::app::App;

    // initializes logging using the `log` crate
    _ = console_log::init_with_level(log::Level::Debug);
    // console_error_panic_hook::set_once();

    leptos::mount_to_body(App);
}
