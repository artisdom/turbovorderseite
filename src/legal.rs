use leptos::{server, ServerFnError};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum LegalDocument {
    Imprint,
    Revocation,
    Terms,
    Payment,
    Privacy,
    Battery,
}

#[server(GetLegalDocument, "/api/legal")]
pub async fn get_legal_document(legal_document: LegalDocument) -> Result<String, ServerFnError> {
    Ok(match legal_document {
        LegalDocument::Imprint => include_str!(env!("LEGAL_IMPRINT_PATH")),
        LegalDocument::Revocation => include_str!(env!("LEGAL_REVOCATION_PATH")),
        LegalDocument::Terms => include_str!(env!("LEGAL_TERMS_PATH")),
        LegalDocument::Payment => include_str!(env!("LEGAL_PAYMENT_PATH")),
        LegalDocument::Privacy => include_str!(env!("LEGAL_PRIVACY_PATH")),
        LegalDocument::Battery => include_str!(env!("LEGAL_BATTERY_PATH")),
    }
    .to_string())
}
