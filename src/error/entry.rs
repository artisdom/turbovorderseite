use super::AppError;

#[allow(clippy::wildcard_imports)]
use leptos::*;
use leptos_router::A;

#[allow(clippy::needless_pass_by_value)]
#[allow(clippy::module_name_repetitions)]
#[must_use]
#[component]
pub fn ErrorEntry(error: AppError) -> impl IntoView {
    let app_error = error.clone().status_code().as_u16();

    match app_error {
        403 => view! {
            <ErrorView
                error_code=app_error
                error_title="Kein Zugang."
                error_description="Sorry, diese Seite ist geschützt. Melde dich an und probiere es erneut."
            />
        }.into_view(),
        404 => view! {
            <ErrorView
                error_code=app_error
                error_title="Wir haben überall gesucht."
                error_description="Sorry, leider können wir die gewünschte Seite aktuell nicht finden."
            />
        }.into_view(),
        500 => view! {
            <ErrorView
                error_code=app_error
                error_title="Internes Server Problem."
                error_description="Ein Problem ist aufgetreten, wir sind gerade daran das Problem zu beheben."
            />
        }.into_view(),
        _ => if (200..300).contains(&app_error) {
            view! {
                <ErrorView
                    error_code=app_error
                    error_title="Erfolgreich."
                    error_description="Die Serveranfrage war erfolgreich."
                />
            }.into_view()
        } else {
            view! {
                <ErrorView
                    error_code=app_error
                    error_title="Ein Problem ist aufgetreten."
                    error_description="Ein Problem ist aufgetreten, wir sind gerade daran das Problem zu beheben."
                />
            }.into_view()
        }
    }
}

#[must_use]
#[component]
pub fn ErrorView(
    error_code: u16,
    error_title: &'static str,
    error_description: &'static str,
) -> impl IntoView {
    view! {
        <section>
            <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
                <div class="mx-auto max-w-screen-sm text-center">
                    <h1 class="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-[var(--primary-color)]">
                        {error_code}
                    </h1>
                    <p class="mb-4 text-3xl tracking-tight font-bold text-gray-900 md:text-4xl dark:text-white">
                        {error_title}
                    </p>
                    <p class="mb-4 text-lg font-light text-gray-500 dark:text-gray-400">
                        {error_description}
                    </p>
                    <A
                        href="/"
                        class="inline-flex text-white bg-[var(--primary-color)] hover:opacity-80 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-primary-900 my-4"
                    >
                        "Zurück zum Shop"
                    </A>
                </div>
            </div>
        </section>
    }
}
