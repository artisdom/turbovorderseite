docker buildx build \
    --push \
    --platform linux/arm64 \
    -f CI/Dockerfile \
    -t registry.gitlab.com/turbomarktplatz/turbovorderseite:staging \
    $(grep -v '^\s*$' .env.production | awk -F= '{print "--build-arg", $1"="$2}' | paste -s -d " ") . \
    # --no-cache
