docker buildx build \
    --push \
    --platform linux/amd64,linux/arm64 \
    -f CI/Dockerfile \
    -t registry.gitlab.com/turbomarktplatz/turbovorderseite:production \
    $(cat .env.production | sed 's@^@--build-arg @g' | paste -s -d " ") . \
    --no-cache
