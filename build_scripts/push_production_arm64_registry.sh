docker buildx build \
    --push \
    --platform linux/arm64 \
    -f CI/Dockerfile \
    -t registry.gitlab.com/turbomarktplatz/turbovorderseite:production \
    $(grep -v '^\s*$' .env.production | awk -F= '{print "--build-arg", $1"="$2}' | paste -s -d " ") . \
    --no-cache
