/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: {
    relative: true,
    files: ["*.html", "./src/**/*.rs"],
  },
  theme: {
    fontFamiliy: {
      sans: ['Montserrat', 'sans-serif']
    },
    extend: {
      fontFamily: {
        // raleway: ['Raleway', 'sans-serif'],
        nunito: ['Nunito', 'sans-serif'],
        quicksand: ['Quicksand', 'sans-serif'],
      },
      fontSize: {
        xxs: '0.55rem'
      },
      animation: {
        fadeIn: 'fadeIn 0.25s ease-in-out',
        slideIn: '0.2s ease-in 1s reverse both running slidein',
        float: 'float 3s ease-in-out infinite',
        shake: 'shake 0.2s ease-in-out infinite',
        heart: 'pulse 0.8s ease-in-out infinite',
      },
      spacing: {
        '300': '75rem',
      },
      scale: {
        '102': '1.02',
      },
      keyframes: {
        fadeIn: {
          '0%': { right: '-200px' },
          '100%': { right: '0px' },
        },
        float: {
          '0%': {
            transform: 'translateY(0px)',
          },
          '50%': {
            transform: 'translateY(-3px)',
          },
          '100%': {
            transform: 'translateY(0px)',
          },
        },      
        shake: {
          '0%': {
            transform: 'rotate(0deg)',
          },
          '25%': {
            transform: 'rotate(5deg)',
          },
          '50%': {
            transform: 'rotate(0deg)',
          },
          '75%': {
            transform: 'rotate(-5deg)',
          },
          '100%': {
            transform: 'rotate(0deg)',
          },
        },      
        heart: {
          '0%': {
            transform: 'scale(1)',
          },
          '25%': {
            transform: 'scale(1.1)',
          },
          '50%': {
            transform: 'scale(1.2)',
          },
          '75%': {
            transform: 'scale(1.3)',
          },
          '100%': {
            transform: 'scale(1.2)',
          },
        },      
      },
    },
  },
  plugins: [],
}
