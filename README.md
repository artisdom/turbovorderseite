![Logo](assets/turbocharger_turbovorderseite.png)

# TurboVorderseite - A WebAssembly shop system

![GitLab Stars](https://img.shields.io/gitlab/stars/timfvb%2Fturbovorderseite)

A blazingly fast shop sytem like never before.

## About

Project TurboVorderseite is a web assembly shop frontend for our custom shop backend, written in the Rust language and made with [Leptos](https://github.com/leptos-rs/leptos) and [TailwindCSS](https://github.com/tailwindlabs/tailwindcss).

![Logo](assets/function_map.png)

## Features

* ✅ Authentication system
* ✅ Search with Meilisearch search engine
* ✅ Cart system
* ✅ Order system
* ✅ Admin area
* ✅ Category page

## Setup

Environment variables that needs to be set up:
```env
AUTH_AUTHORIZATION_ENDPOINT=<your auth endpoint>
AUTH_CLIENT_ID=<your auth cliend id>
AUTH_SCOPE=<your auth scopes>
AUTH_REDIRECT_URI=<your auth redirect url>
AUTH_AUDIENCE=<your auth audience>

LOGIN_REDIRECT_ENDPOINT=<your auth login redirect url>
LOGOUT_REDIRECT_ENDPOINT=<your auth logout redirect url>

APP_DEPLOYMENT=<your app deployment e.g.: https://example.com>
BACKEND_ENDPOINT=<your backend endpoint>

CDN_HOST=<your cdn host>
CDN_EXTERNAL=<your cdn external>
CDN_FALLBACK=<your cdn fallback>

MEILISEARCH_ENDPOINT=<your meilisearch endpoint>
MEILISEARCH_API_KEY=<your meilisearch public key>

FREE_SHIPPING=<free shipping amount>
SHIPPING_FEE=<your shipping fee>

COMPANY_PHONE=<your company phone>
COMPANY_EMAIL=<your company email>
COMPANY_REGISTRATION=<your company registration>
COMPANY_NAME=<your company name>
COMPANY_STREET=<your company stree>
COMPANY_POSTAL_CODE=<your company postal code>
COMPANY_CITY=<your company city>

LEGAL_IMPRINT_PATH=<your path to imprint document>
LEGAL_REVOCATION_PATH=<your path to imprint document>
LEGAL_TERMS_PATH=<your path to terms document>
LEGAL_PAYMENT_PATH=<your path to payment document>
LEGAL_PRIVACY_PATH=<your path to privacy document>
LEGAL_BATTERY_PATH=<your path to battery document>
```

## Related open source projects

![Projects](assets/projects_production.png)

| Project          | Description                                                              | Repository                                                    |
| ---------------- | ------------------------------------------------------------------------ | ------------------------------------------------------------- |
| TurboHinterseite | High performance shop backend written in rust                            | [GitLab](https://gitlab.com/turbomarktplatz/turbohinterseite) |
| TurboBilder      | Content delivery network (CDN)                                           | [GitLab](https://gitlab.com/turbomarktplatz/turbobilder)      |
| TurboSuche       | Service to maintain data consistency between surrealdb and meilisearchdb | [GitLab](https://gitlab.com/turbomarktplatz/turbosuche)       |
| Meilisearch      | Lightning-fast search API                                                | [GitHub](https://github.com/meilisearch/meilisearch)          |
| SurrealDB        | Scalable, distributed document-graph database                            | [GitHub](https://github.com/surrealdb/surrealdb)              |
| Rauthy           | OpenID Connect Single Sign-On Identity Provider                          | [GitHub](https://github.com/sebadob/rauthy)                   |


## Production example
![Logo](assets/primary_logo.png)

[Website](https://moturbo.com) | [Infrastructure](https://moturbo.com/source) | [Discord](https://discord.gg/ghUNEbN2Rx) | [Instagram](https://www.instagram.com/moturbo_shop)

The project was developed to realize our idea of founding a motorcycle and scooter onlineshop.


We are now online at [moturbo.com](https://moturbo.com).

## Mentions

Flowbite
* Icons: https://flowbite.com/icons
* Components: https://flowbite.com/#components

## Special thanks

* Daniél Kerkmann - https://gitlab.com/kerkmann
* Niklas Scheerhoorn - https://gitlab.com/koopa1338

And the Leptos community: https://leptos.dev
